

import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, AsyncStorage, View, Button, Alert, TouchableHighlight, Image, FlatList, ImageBackground, YellowBox } from 'react-native';
import { createStackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import a from './screen/home'
import repair from "./screen/repair";
import QandA from './screen/q&a';
import accountpage from './screen/Account';
import checkarea from './screen/checkarea'
import service from './screen/service';
import ebill from './screen/ebill'
import report from './screen/report';
import notice from './screen/notice';
import payment from './screen/payment';
import qrcode from './screen/qrcode';
import seeallnews from './screen/seeallnews'
import call from './screen/call';
import allpromotion from './screen/allpromotion'
import allrecommend from './screen/allrecommend'
import allweatherscreen from './screen/allweatherscreen'
import Account from './screen/Account';
import checkbill from './screen/checkbill'
import editProfile from './screen/editProfile'
import checkService from './screen/checkService';
import loginHome from './screen/loginHome'
import Setting from './screen/Setting'
import register from './screen/register'
import changePassword from './screen/changePassword'
import promotion1 from './screen/promotion1'
import promotion2 from './screen/promotion2'
import promotion3 from './screen/promotion3'
import promotion4 from './screen/promotion4'
import viewReport from './screen/viewReport'
import reportProblem from './screen/reportProblem'
import eservice from './screen/eservice'
import renderBA from './screen/renderBA'

console.disableYellowBox = true;





const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
});


const loginFirstPage = createStackNavigator({
  loginFirstPage: {
    screen: loginHome,
    navigationOptions: { header: null}
  },
  checkCustomerService: {
    screen: checkService,
    navigationOptions: {header: null}
  },
  registerPage: {
    screen: register,
    navigationOptions: {header: null}
  },
  
 
 
})


const RootTest = createStackNavigator({
  MainApp: {
    screen: a,
    navigationOptions: {
      header: null,
      headerTransitionPreset:'fade-in-place',
    },
  },
  noticePage: { screen: notice },
  checkareapage: { screen: checkarea },
  reportPage: { screen: report },
  serviceCenter: { screen: service },
  ebillPage: { screen: ebill },
  repairPage: { screen: repair },
  paymentPage: { screen: payment },
  qrcodePage: { screen: qrcode },
  phonebook: { screen: call },
  seeallnewspage: { screen: seeallnews },
  allpromotionpage: { screen: allpromotion },
  // allrecommendpage: { screen: allrecommend },
  allweather: { screen: allweatherscreen,
  navigationOptions: {header: null}
  },
  checkbillPage: { screen: checkbill },
  editProfilePage: { screen: editProfile },
  QandApage: { screen:QandA },
 
  account:{ screen:Account },
  settingPage:{screen:Setting},
  promotion1:{screen:promotion1},
  promotion2:{screen:promotion2},
  promotion3:{screen:promotion3},
  promotion4:{screen:promotion4},
  viewReportPage:{screen:viewReport},
  reportProblem:{screen:reportProblem},
  eservicePage:{screen:eservice},
  renderBA:{screen:renderBA},
  
  changePassword:{
    screen:changePassword,
    navigationOptions: {header: null}
  }
},
  {
     mode: 'modal',
      // headerMode:'float',
      // headerTransitionPreset:'fade-in-place'
  })

  RootTest.navigationOptions = ({ navigation }) => {

    let tabBarVisible = true;
    let routeName = navigation.state.routes[navigation.state.index].routeName;
  
    if (routeName == 'checkareapage') {
      tabBarVisible = false
    }
    if (routeName == 'reportPage') {
      tabBarVisible = false
    }
    if (routeName == 'serviceCenter') {
      tabBarVisible = false
    }
    if (routeName == 'ebillPage') {
      tabBarVisible = false
    }
    if (routeName == 'repairPage') {
      tabBarVisible = false
    }
    if (routeName == 'paymentPage') {
      tabBarVisible = false
    }
    if (routeName == 'qrcodePage') {
      tabBarVisible = false
    }
    if (routeName == 'phonebook') {
      tabBarVisible = false
    }
    if (routeName == 'seeallnewspage') {
      tabBarVisible = false
    }
    if (routeName == 'allweather') {
      tabBarVisible = false
    }
    if (routeName == 'checkbillPage') {
      tabBarVisible = false
    }
    if (routeName == 'editProfilePage') {
      tabBarVisible = false
    }
    if (routeName == 'QandApage') {
      tabBarVisible = false
    }
    if (routeName == 'account') {
      tabBarVisible = false
    }
    if (routeName == 'settingPage') {
      tabBarVisible = false
    }
    if (routeName == 'registerPage') {
      tabBarVisible = false
    }
    if (routeName == 'changePassword') {
      tabBarVisible = false
    }
    
    if (routeName == 'allpromotionpage') {
      tabBarVisible = false
    }
    if (routeName == 'promotion1') {
      tabBarVisible = false
    }
    if (routeName == 'noticePage') {
      tabBarVisible = false
    }

    if (routeName == 'viewReportPage') {
      tabBarVisible = false
    }
    
    if (routeName == 'reportProblem') {
      tabBarVisible = false
    }
    
    

    
    
  
  
    return { tabBarVisible }
  }
  

  let originalGetDefaultProps = Text.defaultProps;
  Text.defaultProps = function () {
    return { ...originalGetDefaultProps, allowFontScaling: false };
  };
  Text.defaultProps.allowFontScaling = false;

export default class App extends Component {
  state = {
    login: false
  }
  componentDidMount() {
    AsyncStorage.getItem('userid')
      .then(result => {
        // alert(JSON.stringify(result))
        if(result != null){
          this.setState({ login:true })
        }
        // alert(JSON.stringify(this.state.login))
      })
  }
  render() {
    const App = createAppContainer(loginFirstPage);
    const App2 = createAppContainer(RootTest); 
    return this.state.login ? <App2/> : <App />
  }
};

// const App1 = createAppContainer(RootTest);

// export default App1;