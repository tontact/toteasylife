


import React from 'react';
import { Platform, FlatList, View, Text, Button, Alert, TouchableHighlight, TouchableOpacity, StyleSheet, TextInput, ActivityIndicator, Image, ImageBackground, Picker } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';
import {
  widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';

import { Icon } from 'react-native-elements'

import { SearchBar } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
// import Swiper from 'react-native-swiper';
import SwiperFlatList from 'react-native-swiper-flatlist';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import { WebView } from 'react-native-webview';

export default class reportProblem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pickerSelection: 'Search categories',
      pickerDisplayed: false,
      text: '',
    
      serviceNumber:this.props.navigation.getParam('serviceNumber'),

     
    }
  }



  static navigationOptions = ({ navigation }) => {
     const { params = {} } = navigation.state;

    // const params = navigation.getParam('checklogin')
    // this.state.user
    // alert(params)
    return {
      headerTransitionPreset: 'fade-in-place',
      // title: 'ชำระค่าบริการ',
      headerTitle: (<Text style={{  fontFamily: "Prompt-Light", color: "#000", fontSize: 20, fontWeight: '500' , textAlign: "center", flex: 1,}}>{'แจ้งเหตุเสีย'}</Text>),


      headerLeft: (
        <TouchableOpacity style={{ marginLeft: wp(3), }} >
          <Ionicons name='ios-arrow-back' size={30} onPress={() => navigation.goBack()} />
        </TouchableOpacity>
      ),
      // headerRight: (
      //   <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>

      //   </HeaderButtons>
      // ),
      // //  headerTitle: <Logo/>,
      headerRight: (
        <TouchableOpacity style={{ marginRight: wp(2.5) }} >

        </TouchableOpacity>
      ),

      headerStyle: {
        backgroundColor: '#F9F9F9'
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        // fontFamily: "Prompt-Light",
        textAlign: 'center',
        flex: 1,

      },
    }
  };

 
//   componentDidMount(){
//       alert(this.state.serviceNumber)
//   }



  render() {
   
    return (
      
      <WebView source={{ uri: 'http://tel1177.tot.co.th/new_sr.php?frmmb=totez&num=' + this.state.serviceNumber}} />

     


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingTop: 20,
    flexDirection: 'column'

  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  linearGradient: {
    flexDirection: 'column',
    flex: 1,
    alignSelf: 'center',
    borderRadius: 5,
    height: 220,
    width: wp('100')
  },

  buttonSearch: {
    alignItems: 'center',
    backgroundColor: '#00BFFF',
    padding: 10,
    marginTop: 60,
    marginLeft: 140,
    borderRadius: 5,
    justifyContent: 'center',
    flexDirection: 'row'


  },
});


