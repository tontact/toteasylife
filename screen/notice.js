import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, AsyncStorage, FlatList, Image ,RefreshControl} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen';
import axios from 'axios';
import CardView from 'react-native-cardview'
import Loading from '../loading'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { ScrollView } from 'react-native-gesture-handler';

export default class notiba extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      id: "",
      notibano: [],
      baNumber: [],
      refreshing: false,
    };
  }
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    // const params = navigation.getParam('checklogin')
    // this.state.user
    // alert(params)
    return {
      headerTransitionPreset: 'fade-in-place',
      // title: 'ชำระค่าบริการ',
      headerTitle: (<Text style={{ fontFamily: "Prompt-Light", color: "#000", fontSize: 20, fontWeight: '500', textAlign: "center", flex: 1, }}>{'แจ้งเตือน'}</Text>),


      headerLeft: (
        <TouchableOpacity style={{ marginLeft: wp(3), }} >
          <Ionicons name='ios-arrow-back' size={30} onPress={() => navigation.goBack()} />
        </TouchableOpacity>
      ),
      // headerRight: (
      //   <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>

      //   </HeaderButtons>
      // ),
      // //  headerTitle: <Logo/>,
      headerRight: (
        <TouchableOpacity style={{ marginRight: wp(2.5) }} >

        </TouchableOpacity>
      ),

      headerStyle: {
        backgroundColor: '#F9F9F9'
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        // fontFamily: "Prompt-Light",
        textAlign: 'center',
        flex: 1,

      },
    }
  };


  _onRefresh = () => {
    this.setState({ refreshing: true, });
    this.getBaUser();
    // this.getActivityASA()
  };
  componentDidMount = async () => {
    var id = await AsyncStorage.getItem('userid');
    //  var baNumber = await AsyncStorage.getItem('baNumber');

    await this.setState({ id: id })
    // await this.setState({ baNumber: baNumber })
    // //console.log(baNumber)
    await this.getBaUser();
  }



  getBaUser() {
    const url = "http://203.113.11.167/api/getBaUser/" + this.state.id;
    // //console.log(url);
    // const headers = {
    //   'Content-Type': 'application/json',
    //   'Authorization': 'JWT fefege...'
    // }
    axios.get(url)
      .then(result => {
        // var userprofile = JSON.stringify(result);
        // //console.log(userprofile);
        //  alert(JSON.stringify(result.data));
        // AsyncStorage.setItem('baNumber', (result.data));
        // //console.log(result.data, 'getBA');

        this.setState({

          baNumber: result.data

        })
        this.getNotiba();
        // //console.log(result.data, 'getBA1');

      })
      .catch(err => {
        // alert(JSON.stringify(error));
      })
      this.setState({ refreshing: false });
  }

  async getNotiba() {
    // alert(this.state.baNumber.length);
    var pushNoti = []
    for (var i = 0; i < this.state.baNumber.length; i++) {
      //console.log(this.state.baNumber[i].baNumber);
      const url = "http://app1.toteservice.com/index.php?r=api/Easylife/chkNotiBibil010_2020&username=easylife&password=e@sylifeP@ss&bano=" + this.state.baNumber[i].baNumber;

      // //console.log(url);

      await axios.get(url)
        .then(result => {

          // console.log(result.data.Debt_bano, "result");
          // //console.log(result.data.Debt_bano, "Debt_bano")

          // let baNumber = this.state.baNumber;
          // baNumber[i].invo1 = parseInt(result.data.outstandingDebtMny) > 0 ? Object.values(result.data.inv01) : []
          // pushNoti = pushNoti.push( result.data.Debt_bano != null ? Object.values(result.data.Debt_bano) : [])

          if (result.data.CntDebt_bano != 0) {
            pushNoti=pushNoti.concat(result.data.Debt_bano[0])
            //console.log(pushNoti[0], 'push')
            // this.state.notibano.push(pushNoti[0])
            // this.setState({
            //   notibano: this.state.notibano.push(pushNoti[0]),
            //   loading: false,
            // })
          }

          console.log(pushNoti,i, "notification");


        })

        .catch(err => {
          // alert(JSON.stringify(err));

        })
    } 
    this.setState({
      loading: false,
      notibano:pushNoti
    })
  }



  renderItem(item) {
    return (
      <View style={{ alignSelf: 'center' }}>
        <View
          style={{
            width: wp(90),
            height: wp(20),
            flexDirection: "column",
            backgroundColor: "white",
            marginBottom: 10,
            borderRadius: 10,
            marginLeft: 20,
            marginRight: 20,
            paddingRight: 60,
            shadowColor: "#000",
            shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14,


          }}
        >
          {/* { this.state.notibano.length != 0 ? */}

          <View style={{}}>
            <View style={{}}>
              <View style={{ flexDirection: "row" }}>

                <Image
                  source={require('../alliconESL/Iconnewsetapp/PAYTOTeasylifeAW1copy15.png')}
                  style={{ width: 50, height: 50, borderRadius: 50 / 2, marginTop: wp(3.7), marginLeft: wp(3), marginRight: wp(3) }}
                />

                <View>


                  <Text style={{ fontFamily: 'Prompt-SemiBold', fontSize: wp(4), color: 'black', marginTop: wp(2) }}>เลขที่ใบแจ้งบริการ: {item.invoiceNumber}</Text>

                  <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(3) }}>ครบกำหนดจ่ายวันที่: {item.paymentDueDate}</Text>

                  {item.CutLine == "yes" ?
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(3), color: "red" }}>ยอดรวมชำระ: {item.outstandingDebtMny / 10000000}</Text>
                    :
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(3), }}>ยอดค้างรวมชำระ: {item.outstandingDebtMny / 10000000} บาท</Text>
                  }

                </View>
              </View>


            </View>
          </View>
          {/* {}: null} */}

        </View>
      </View>
    );
  }

  render() {
    return (
      this.state.loading ? (<Loading />) : (

        <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            tintColor='#117ffe'
            colors={['#117ffe']}
            style={{ backgroundColor: 'white' }}
          />
        }
        
        >
        <View style={{ flexDirection: "column", alignItems: "center", marginTop: wp(5) }}>

          {this.state.notibano.length == 0 ? <View>

            <Text style={{ fontSize: wp(4), color: '#000', fontFamily: 'Prompt-Light', fontWeight: '300', marginTop: wp(5), alignSelf: 'center' }}>ไม่มียอดค้างชำระ</Text>
            {/* <Text style={{ fontSize: wp(3.5), color: '#000', fontFamily: 'Prompt-Light', fontWeight: '300', marginTop: wp(2), alignSelf: 'center' }}>à¸à¸£à¸¸à¸“à¸²à¹€à¸žà¸´à¹ˆà¸¡à¸£à¸²à¸¢à¸à¸²à¸£</Text> */}
          </View> :

            <FlatList
              data={this.state.notibano}
              contentContainerStyle={{ marginTop: wp(2) }}
              renderItem={({ item }) => this.renderItem(item)}
            // extraData={this.state}
            >
            </FlatList>

          }

        </View>
        </ScrollView>
        )
    );
  }
}

;
