import React, { Component } from 'react';
import {
    Image, Alert, StyleSheet, Text, Animated, TouchableHighlight,WebView,
    View, AsyncStorage, TextInput, NativeModules, ActivityIndicator, FlatList, TouchableOpacity, ImageBackground
} from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as loc,
    removeOrientationListener as rol
} from 'react-native-responsive-screen';

import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';

import axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons';

import Ionicons from 'react-native-vector-icons/Ionicons';



console.disableYellowBox = true;

var ImagePicker = NativeModules.ImageCropPicker;

const width = '100%';
const IoniconsHeaderButton = passMeFurther => (
    <HeaderButton {...passMeFurther} IconComponent={Icon} iconSize={30} color="white" />

);

export default class checkService extends Component {
  constructor(props) {
    super(props);
    this.state = {
       
    };
  }
 

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

   // const params = navigation.getParam('checklogin')
   // this.state.user
   // alert(params)
   return {
     headerTransitionPreset: 'fade-in-place',
     // title: 'ชำระค่าบริการ',
     headerTitle: (<Text style={{  fontFamily: "Prompt-Light", color: "#000", fontSize: 20, fontWeight: '500' , textAlign: "center", flex: 1,}}>{'สมัคร eBill'}</Text>),


     headerLeft: (
       <TouchableOpacity style={{ marginLeft: wp(3), }} >
         <Ionicons name='ios-arrow-back' size={30} onPress={() => navigation.goBack()} />
       </TouchableOpacity>
     ),
     // headerRight: (
     //   <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>

     //   </HeaderButtons>
     // ),
     // //  headerTitle: <Logo/>,
     headerRight: (
       <TouchableOpacity style={{ marginRight: wp(2.5) }} >

       </TouchableOpacity>
     ),

     headerStyle: {
       backgroundColor: '#F9F9F9'
     },
     headerTintColor: '#000',
     headerTitleStyle: {
       // fontFamily: "Prompt-Light",
       textAlign: 'center',
       flex: 1,

     },
   }
 };


    render() {
    
    return (
        <View style={{ flex: 1 }}>      
        
        {/* <WebView source={{ uri: 'https://ebill.nteservice.com/ebillonline' }} /> */}
        <WebView source={{ uri: 'https://quickebill.nteservice.com' }} />

        </View>
    );
  }
}


const styles = StyleSheet.create({
    bnimage: {
        //borderRadius: 10,
        height: hp(63),
        width: wp(100),
        alignSelf: 'center'
    },
    fontStyle: {
        fontFamily: 'Prompt-Light'
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 20,
        width: wp(85),
        marginTop: wp(20)
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    buttonSearch: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        padding: 10,
        backgroundColor: '#79cff7',
        width: wp(60),
        marginTop: 40,
        marginBottom: 10,
        //marginLeft:14,
        borderRadius: 30,
        justifyContent: 'center',
        flexDirection: 'column'
    },
    loginE: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        padding: 10,
        backgroundColor: '#fff',
        width: wp(60),
        marginTop: 40,
        marginBottom: 10,
        //marginLeft:14,
        borderRadius: 30,
        justifyContent: 'center',
        flexDirection: 'column',
        borderWidth: 3,
        borderColor: '#25bef8'
    },
    login: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        //padding: 10,
        alignSelf: 'center',
        // backgroundColor: '#8A23FC',
        height: wp(12),
        width: wp(65),
        marginTop: wp(5),
        marginBottom: 10,
        //marginLeft:14,
        justifyContent: 'center',
        flexDirection: 'column',
        // borderWidth: 1,
        borderRadius: 30,
        // borderColor: '#8A23FC'
    },
    loginModal: {
        //alignSelf: 'flex-start',
        // backgroundColor: '#03a9f4',
        padding: 10,
        backgroundColor: '#79cff7',
        width: wp(60),
        marginTop: 70,
        marginBottom: 10,
        //marginLeft:14,
        borderRadius: 30,
        justifyContent: 'center',
        flexDirection: 'column'
    },
    otpAgain: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        //padding: 10,
        backgroundColor: '#8A23FC',
        height: wp(12),
        width: wp(65),
        marginTop: wp(2.5),
        marginBottom: 10,
        //marginLeft:14,
        justifyContent: 'center',
        flexDirection: 'column',
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#8A23FC'
    },
    lineStylew3: {
        borderWidth: 1,
        borderColor: '#009ACD',
        //backgroundColor: '#8A23FC',
        marginTop: 10,
        marginBottom: wp(7.5),
        alignSelf: 'center',
        width: wp('20'),
    
      },
      customerService: {
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: '#fff',
        height: wp(8),
        width: wp(20),
        marginTop: wp(2.5),
        flexDirection:'row',
        justifyContent: 'center', 
        borderWidth: 2,
        borderRadius: 30,
       
    } 
})