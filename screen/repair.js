import React from 'react';
import { Platform, FlatList, View, Text, Button, Alert, TouchableHighlight, TouchableOpacity, StyleSheet, TextInput, ActivityIndicator, Image, ImageBackground, Picker, } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';
import {
  widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';

import { Icon } from 'react-native-elements'

import { SearchBar } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
// import Swiper from 'react-native-swiper';
import SwiperFlatList from 'react-native-swiper-flatlist';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';




export default class repair extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      pickerSelection: 'Please select service',
      pickerSelection1: 'Specify type of problem',
      pickerDisplayed: false,
      pickerDisplayed1: false,
      
      text: ''


    }
  }

  setPickerValue(newValue) {
    this.setState({
      pickerSelection: newValue,
    
      
    })

    this.togglePicker();
  }

  setPickerValue1(newValue) {
    this.setState({
      pickerSelection1: newValue,
    
      
    })

    this.togglePicker();
  }

  togglePicker(n) {

    if(n==1){
    this.setState({
      pickerDisplayed: !this.state.pickerDisplayed,
      
    })}
    else if (n==2){
      this.setState({
        pickerDisplayed1: !this.state.pickerDisplayed1,
        
      }) 
    }
  }


  // togglePicker1() {
  //   this.setState({
  //     pickerDisplayed1: !this.state.pickerDisplayed1,
      
  //   })
  // }

  static navigationOptions = ({ navigation }) => {
    // const { params = {} } = navigation.state;

    // const params = navigation.getParam('checklogin')
    // this.state.user
    // alert(params)
    return {
      title: 'Repair Service',

      // headerLeft: (
      //   <TouchableHighlight style={styles.headerHome} >
      //     <Ionicons name='md-person' size={30} color='#FFDEAD' />
      //   </TouchableHighlight>
      // ),
      // headerRight: (
      //   <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>

      //   </HeaderButtons>
      // ),
      // //  headerTitle: <Logo/>,

      headerStyle: {
        backgroundColor: '#3399FF'
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        // fontFamily: "Prompt-Light",
        textAlign: 'center',
        flex: 1,

      },
    }
  };

  onPress = () => {
    const data = this.state.pickerSelection;
    const text = this.state.text

    if (data == "") {
      Alert.alert('Plese select and fill the information')
    }


    // else 
    //   Alert.alert('Plese wait')




  }


  render() {
    const pickerValues = [
      {
        title: 'Please select service',
        value: 'Please select service'
      },
      {
        title: 'TOT 5G',
        value: 'TOT 5G'
      },
      {
        title: 'TOT WIFI',
        value: 'TOT WIfI'
      },
      {
        title: 'TOT Internet',
        value: 'TOT Internet'
      },
      {
        title: 'High speed internet service',
        value: 'High speed internet service'
      },
        

    ]

    const pickerValues1 = [
      {
        title: 'Specify type of problem',
        value: 'Specify type of problem'
      }, 

      {
        title: 'Problem1',
        value: 'Problem1'
      },
      {
        title: 'Problem2',
        value: 'Problem2'
      },
      {
        title: 'Problem3',
        value: 'Problem3'
      },
      {
        title: 'Problem4',
        value: 'Problem4'
      },
     

    ]


    return (


      <LinearGradient colors={['#FFFAFA', '#FFFAF0']} style={styles.linearGradient}>
        <ScrollView>
          
          <View style={styles.container}>

          

            <View style={{ flexDirection: 'row', marginLeft: wp('4.3'), alignItems: 'baseline' }}>
              {/* <Text style={{ marginTop: wp('3'), fontSize: 30, fontWeight: 'bold' }} >Type of search</Text> */}


              <Button onPress={() => this.togglePicker('1')} title={"Repair Categories ▼ "} color="#000" />


            </View>
            <Text style={{ marginLeft: wp('6'), marginTop: 20, fontWeight: 'bold' }}> {this.state.pickerSelection} :</Text>
            {
              this.state.pickerSelection == 'Please select service' ? null : this.state.pickerSelection == 'TOT 5G' ?
                <View style={{ padding: 10 }}>

                  <TextInput
                    style={{
                      height: hp(4.3), width: wp(72), marginLeft: 10, marginLeft: wp('4.3'), margin: 5, backgroundColor: '#fff', paddingLeft: 10,
                      color: '#424242', paddingLeft: 10,
                    }}
                    borderWidth={0.3}
                    borderColor='#696969'
                    borderRadius={5}
                    keyboardType={'numeric'}
                    placeholder="Fill the phone number"
                    onChangeText={(text) => this.setState({ text })}
                  />
                </View> : this.state.pickerSelection == 'TOT WIfI' ?

                  <View style={{ padding: 10 }}>


                    <TextInput
                      style={{
                        height: hp(4.3), width: wp(80), marginLeft: wp('4.3'), margin: 5, backgroundColor: '#fff',
                        color: '#424242', paddingLeft: 10,
                      }}
                      borderWidth={0.3}
                      borderRadius={5}
                      borderColor='#696969'
                      placeholder="Fill the name"
                      onChangeText={(text) => this.setState({ text })}
                    />

                    <TextInput
                      style={{
                        height: hp(4.3), width: wp(80), marginLeft: 10, marginLeft: wp('4.3'), margin: 5, backgroundColor: '#fff',
                        color: '#424242', paddingLeft: 10,
                      }}
                      borderWidth={0.3}
                      borderRadius={5}
                      borderColor='#696969'
                      placeholder="Fill the surname"
                      onChangeText={(text) => this.setState({ text })}
                    />
                  </View> : this.state.pickerSelection == 'TOT Internet' ?

                    <View style={{ padding: 10 }}>


                      <TextInput
                        style={{
                          height: hp(4.3), width: wp(80), marginLeft: wp('4.3'), margin: 5, backgroundColor: '#fff',
                          color: '#424242', paddingLeft: 10,
                        }}
                        borderWidth={0.3}
                        borderRadius={5}
                        borderColor='#696969'
                        placeholder="Fill the name of government place"
                        onChangeText={(text) => this.setState({ text })}
                      />
                    </View> : this.state.pickerSelection == 'High speed internet service' ?

                      <View style={{ padding: 10 }}>


                        <TextInput
                          style={{
                            height: hp(4.3), width: wp(80), marginLeft: wp('4.3'), margin: 5, backgroundColor: '#fff',
                            color: '#424242', paddingLeft: 10,
                          }}
                          borderWidth={0.3}
                          borderRadius={5}
                          borderColor='#696969'
                          keyboardType={'numeric'}
                          placeholder="Fill the special number"
                          onChangeText={(text) => this.setState({ text })}
                        />


                      </View> :

                      <View style={{ padding: 10 }}>


                        <TextInput
                          style={{
                            height: hp(4.3), width: wp(80), marginLeft: wp('4.3'), margin: 5, backgroundColor: '#fff',
                            color: '#424242', paddingLeft: 10,
                          }}
                          borderWidth={0.3}
                          borderRadius={5}
                          borderColor='#696969'
                          placeholder="Fill the information"
                          onChangeText={(text) => this.setState({ text })}
                        />

                        <TextInput
                          style={{
                            height: hp(4.3), width: wp(80), marginLeft: 10, marginLeft: wp('4.3'), margin: 5, backgroundColor: '#fff',
                            color: '#424242', paddingLeft: 10,
                          }}
                          borderWidth={0.3}
                          borderRadius={5}
                          borderColor='#696969'
                          placeholder="Fill the information"
                          onChangeText={(text) => this.setState({ text })}
                        />

                      </View>


            }

          

            <Modal
              animationType={"slide"}
              isVisible={this.state.pickerDisplayed}
              onSwipeComplete={() => this.setState({ pickerDisplayed: false })}
              swipeDirection={['up', 'left', 'right', 'down']}
              style={styles.bottomModal}
            >
              <TouchableOpacity underlayColor={false} style={{ flex: 1, justifyContent: 'flex-end' }} onPress={() => this.setState({ pickerDisplayed: false })}>
                <View>
                  <Picker
                    style={{ backgroundColor: '#fff', position: 'absolute', bottom: 0, left: 0, right: 0 }}
                    selectedValue={this.state.pickerSelection}
                    onValueChange={(itemValue, itemIndex) => this.setState({ pickerSelection: itemValue })}>
                    {/* <Picker.Item label="Chicken" value="chicken" /> */}
                    {
                      pickerValues.map((item, index) =>

                        <Picker.Item label={item.title} value={item.title} key={index} />


                      )



                    }
                  </Picker>

                  <TouchableOpacity onPress={() => this.togglePicker('1')} style={{ marginBottom: wp('33'), alignSelf: 'flex-end', paddingRight: 15 }}>
                    <Text style={{ color: '#000', fontWeight: 'bold' }} >Done</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.togglePicker('1')} style={{ margin: 25, alignSelf: 'center' }}>
                    <Text style={{ color: '#999' }}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </TouchableOpacity>
            </Modal>



            <View style={{ flexDirection: 'row', marginLeft: wp('4.3'), alignItems: 'baseline', marginTop:20 }}>
              {/* <Text style={{ marginTop: wp('3'), fontSize: 30, fontWeight: 'bold' }} >Type of search</Text> */}
              <Button onPress={() => this.togglePicker('2')} title={"Specify Problem ▼ "} color="#000" />
            </View>
            <Text style={{ marginLeft: wp('6'), marginTop: 20, fontWeight: 'bold' }}> {this.state.pickerSelection1} :</Text>

            {
              this.state.pickerSelection1 == 'Specify type of problem' ? null : this.state.pickerSelection1 == 'Problem1' ?
                <View style={{ padding: 10 }}>

                  <TextInput
                    style={{
                      height: hp(4.3), width: wp(72), marginLeft: 10, marginLeft: wp('4.3'), margin: 5, backgroundColor: '#fff', paddingLeft: 10,
                      color: '#424242', paddingLeft: 10,
                    }}
                    borderWidth={0.3}
                    borderColor='#696969'
                    borderRadius={5}
                    keyboardType={'numeric'}
                    placeholder="Fill the phone number"
                    onChangeText={(text) => this.setState({ text })}
                  />
                </View> : this.state.pickerSelection1 == 'Problem2' ?

                  <View style={{ padding: 10 }}>


                    <TextInput
                      style={{
                        height: hp(4.3), width: wp(80), marginLeft: wp('4.3'), margin: 5, backgroundColor: '#fff',
                        color: '#424242', paddingLeft: 10,
                      }}
                      borderWidth={0.3}
                      borderRadius={5}
                      borderColor='#696969'
                      placeholder="Fill the name"
                      onChangeText={(text) => this.setState({ text })}
                    />

                    <TextInput
                      style={{
                        height: hp(4.3), width: wp(80), marginLeft: 10, marginLeft: wp('4.3'), margin: 5, backgroundColor: '#fff',
                        color: '#424242', paddingLeft: 10,
                      }}
                      borderWidth={0.3}
                      borderRadius={5}
                      borderColor='#696969'
                      placeholder="Fill the surname"
                      onChangeText={(text) => this.setState({ text })}
                    />
                  </View> : this.state.pickerSelection1 == 'Problem3' ?

                    <View style={{ padding: 10 }}>


                      <TextInput
                        style={{
                          height: hp(4.3), width: wp(80), marginLeft: wp('4.3'), margin: 5, backgroundColor: '#fff',
                          color: '#424242', paddingLeft: 10,
                        }}
                        borderWidth={0.3}
                        borderRadius={5}
                        borderColor='#696969'
                        placeholder="Fill the name of government place"
                        onChangeText={(text) => this.setState({ text })}
                      />
                    </View> : this.state.pickerSelection1 == 'Problem4' ?

                      <View style={{ padding: 10 }}>


                        <TextInput
                          style={{
                            height: hp(4.3), width: wp(80), marginLeft: wp('4.3'), margin: 5, backgroundColor: '#fff',
                            color: '#424242', paddingLeft: 10,
                          }}
                          borderWidth={0.3}
                          borderRadius={5}
                          borderColor='#696969'
                          keyboardType={'numeric'}
                          placeholder="Fill the special number"
                          onChangeText={(text) => this.setState({ text })}
                        />


                      </View> :

                      <View style={{ padding: 10 }}>


                        <TextInput
                          style={{
                            height: hp(4.3), width: wp(80), marginLeft: wp('4.3'), margin: 5, backgroundColor: '#fff',
                            color: '#424242', paddingLeft: 10,
                          }}
                          borderWidth={0.3}
                          borderRadius={5}
                          borderColor='#696969'
                          placeholder="Fill the information"
                          onChangeText={(text) => this.setState({ text })}
                        />

                        <TextInput
                          style={{
                            height: hp(4.3), width: wp(80), marginLeft: 10, marginLeft: wp('4.3'), margin: 5, backgroundColor: '#fff',
                            color: '#424242', paddingLeft: 10,
                          }}
                          borderWidth={0.3}
                          borderRadius={5}
                          borderColor='#696969'
                          placeholder="Fill the information"
                          onChangeText={(text) => this.setState({ text })}
                        />

                      </View>


            }




            <Modal
              animationType={"slide"}
              isVisible={this.state.pickerDisplayed1}
              onSwipeComplete={() => this.setState({ pickerDisplayed1: false })}
              swipeDirection={[ 'down']}
              style={styles.bottomModal}
              onBackdropPress={() => this.setState({ pickerDisplayed: false })}
            >
              <TouchableOpacity underlayColor={false} style={{ flex: 1, justifyContent: 'flex-end' }} onPress={() => this.setState({ pickerDisplayed1: false })}>
                <View>
                  <Picker
                    style={{ backgroundColor: '#fff', position: 'absolute', bottom: 0, left: 0, right: 0 }}
                    selectedValue={this.state.pickerSelection1}
                    onValueChange={(itemValue, itemIndex) => this.setState({ pickerSelection1: itemValue })}>
                    {/* <Picker.Item label="Chicken" value="chicken" /> */}

                    {
                      pickerValues1.map((item, index) =>

                        <Picker.Item label={item.title} value={item.title} key={index} />

                      )
                    }



                  </Picker>

                  <TouchableOpacity onPress={() => this.togglePicker('2')} style={{ marginBottom: wp('33'), alignSelf: 'flex-end', paddingRight: 15 }}>
                    <Text style={{ color: '#000', fontWeight: 'bold' }} >Done</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.togglePicker('2')} style={{ margin: 25, alignSelf: 'center' }}>
                    <Text style={{ color: '#999' }}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </TouchableOpacity>
            </Modal>


            



            <View>

              <TouchableOpacity
                style={styles.buttonSearch}
                onPress={this.onPress}

              >
                <Text style={{ fontWeight: 'bold', color:'#fff' }}> Report </Text>
              </TouchableOpacity>


            </View>

          </View>

        </ScrollView>
      </LinearGradient>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingTop: 20,
    flexDirection: 'column',
    marginTop: 10

  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
   // marginBottom:20
  },
  linearGradient: {
    flexDirection: 'column',
    flex: 1,
    alignSelf: 'center',
    borderRadius: 5,
    height: 220,
    width: wp('100')
  },

  buttonSearch: {
    alignItems: 'center',
    backgroundColor: '#00BFFF',
    padding: 10,
    marginTop: 60,
    marginLeft: 140,
    borderRadius: 5,
    justifyContent: 'center',
    flexDirection: 'row'


  },
});


