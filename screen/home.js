import React, { Component } from 'react';
import { SafeAreaView, AsyncStorage, Animated, Platform, FlatList, Dimensions, View, Text, Button, StyleSheet, RefreshControl } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';
import {
  widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';


import Axios from 'axios';
// import { SearchBar } from 'react-native-elements';
import { ScrollView, TouchableWithoutFeedback } from 'react-native-gesture-handler';

import WeatherComponent from './header';

import ImageSelecter from 'react-native-image-picker';
import RNRestart from "react-native-restart";
import IconHome from '../components/icon-home/index';
import Profile from '../components/profile/index';
import News from '../components/news/index';
import Promotion from '../components/promotion/index';
import axios from 'axios';

const IoniconsHeaderButton = passMeFurther => (
  <HeaderButton {...passMeFurther} IconComponent={Ionicons} iconSize={28} color="white" />
);
const HEADER_MAX_HEIGHT = wp(60);
const HEADER_MIN_HEIGHT = wp(25);
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

const { height, width } = Dimensions.get('window')


// const {
//   height: SCREEN_HEIGHT,
// } = Dimensions.get('window');

// const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
// const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
// const NAV_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 64;

// const SCROLL_EVENT_THROTTLE = 16;
// const DEFAULT_HEADER_MAX_HEIGHT = 170;
// const DEFAULT_HEADER_MIN_HEIGHT = NAV_BAR_HEIGHT;
// const DEFAULT_EXTRA_SCROLL_HEIGHT = 30;
// const DEFAULT_BACKGROUND_IMAGE_SCALE = 1.5;




export default class a extends Component {
  constructor(props) {
    super(props);
    this.index = 0;
    this.index1 = 0;
    this.index2 = 0;

  }



  state = {
    search: '',
    imageSource: null,

    data: [],
    userid: 0,
    checkbill: [],
    m1: [],
    m2: [],
    bano: [],
    svno: [],
    sum: 0,

    pickerDisplayed: false,
    test: null,
    scrollY: new Animated.Value(0),
    pincode: null,

    news_data: [],
    refreshing: false,
    ListPromotion: [],
    ListNews: []
  };

  updateSearch = search => {
    this.setState({ search });
  };




  static navigationOptions = ({ navigation }) => {
    // const { params = {} } = navigation.state;

    // const params = navigation.getParam('checklogin')
    // this.state.user
    // alert(params)

    return {
      title: 'TOT Easy Life',

      // headerLeft: (
      //   <TouchableHighlight style={styles.headerHome} >
      //     <Ionicons name='md-person' size={30} color='#FFDEAD' />
      //   </TouchableHighlight>
      // ),
      // headerRight: (
      //   <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>

      //   </HeaderButtons>
      // ),
      // //  headerTitle: <Logo/>,

      headerStyle: {
        backgroundColor: '#3399FF'
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        // fontFamily: "Prompt-Light",
        textAlign: 'center',
        flex: 1,

      },
    }
  };
  //   test(){
  //       Alert.alert('in')
  //   }



  // renderItem(item) {
  //   return (

  //   );
  // }




  _getUser() {

    AsyncStorage.getItem('user').then(userid => {
      Axios.get('http://203.113.11.167/api/myuser/' + userid)
        .then(result => {
          this.setState({
            data: result.data.data,

          })
          this._checkbill()
          // alert(this.state.data.bano)

        }).catch(err => {
          this.getData()
        })


    })
  }



  SelectCameraRoll = () => {
    //   ImagePicker.openPicker({
    //     width: 300,
    //     height: 300,
    //     cropping: true,
    //     includeBase64: true,
    //     includeExif: true,
    // }).then(image => {
    //     console.log('received base64 image');
    //     this.setState({
    //         imageSource: {uri: 'data:image/jpeg;base64,'+image.data}
    //     });
    // })
    const options = {
      // mediaType:'photo',
      quality: 0.5,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };
    ImageSelecter.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        // alert('response image ....')
        // const source = { uri: data:${response.data.mime};base64, + response.data };
        // let source = { uri: response.uri };
        this.setState({
          imageSource: {
            uri: 'data:image/jpeg;base64,' + response.data,
          }
        });
        //alert(this.state.imageSource.uri);
        //this.uploadPhoto(this.state.citizen);
      }
    });
  };


  getPromotion() {
    const url = "http://203.113.11.167/api/promotion";
    // console.log(url);
    axios.get(url)
      .then(result => {
        this.setState({ ListPromotion: result.data })
        let data = []

        this.state.ListPromotion.forEach(item => {
          if (item.type_id == 1) {
            data.push(item)
          }
        })
        this.setState({ ListPromotion: data })
        // alert(JSON.stringify(data))

        // console.log(this.state.ListPromotion);
      })

      .catch(err => {
        alert(JSON.stringify(err));
      })
    this.setState({ refreshing: false });

  }




  getNew() {
    const url = "http://203.113.11.167/api/news";
    // console.log(url);
    axios.get(url)
      .then(result => {

        var data = result.data.slice(0, 5)
        this.setState({ ListNews: data })
        // console.log(this.state.ListNews, 'Listnews')
      })

      .catch(err => {
        alert(JSON.stringify(err));
      })
    this.setState({ refreshing: false });
  }


  _onRefresh = () => {
    this.setState({ refreshing: true, });
    this.getPromotion()
    this.getNew()

    // this.getActivityASA()
  };
  componentDidMount() {

    this.getPromotion()
    this.getNew()
  }

  componentWillMount() {
    // alert(JSON.stringify(this.props))
    this.scrollY = new Animated.Value(0)

    this.startHeaderHeight = 60
    this.endHeaderHeight = 0
    if (Platform.OS == 'android') {
      this.startHeaderHeight = 100 + StatusBar.currentHeight
      this.endHeaderHeight = 70 + StatusBar.currentHeight
    }

    this.animatedHeaderHeight = this.scrollY.interpolate({
      inputRange: [0, 50],
      outputRange: [this.startHeaderHeight, this.endHeaderHeight],
      extrapolate: 'clamp'
    })

    this.animatedOpacity = this.animatedHeaderHeight.interpolate({
      inputRange: [this.endHeaderHeight, this.startHeaderHeight],
      outputRange: [0, 1],
      extrapolate: 'clamp'
    })
    this.animatedTagTop = this.animatedHeaderHeight.interpolate({
      inputRange: [this.endHeaderHeight, this.startHeaderHeight],
      outputRange: [0, 0],
      extrapolate: 'clamp'
    })
    this.animatedMarginTop = this.animatedHeaderHeight.interpolate({
      inputRange: [this.endHeaderHeight, this.startHeaderHeight],
      outputRange: [50, 30],
      extrapolate: 'extend'
    })

    const imageOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: 'extend',
    });
    const imageTranslate = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -50],
      extrapolate: 'clamp',
    });
    //let data = [1, 2, 3]
    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: 'clamp',
    });

  }

  Login() {
    // alert('in')
    let a = 'login'
    AsyncStorage.getItem('login')
      .then(data => {
        if (data == 'login') {
          AsyncStorage.clear()
            .then(async data => {
              await this.setState({ isModalVisible: false })
            });
          setTimeout(() => {
            RNRestart.Restart();
          }, 700);

        } else {
          AsyncStorage.setItem('login', a)
            .then(async result => {
              // alert(JSON.stringify(result))
              await this.setState({ isModalVisible: false })
            })
          setTimeout(() => {
            RNRestart.Restart();
          }, 700);
        }

      })
  }



  onComplete = (inputtedPin, clear) => {
    if (inputtedPin == this.state.pincode) {
      this.setState({ visibleModal: null })
    } else {
      alert("รหัสผ่านไม่ถูกต้อง")
      clear();
    }
  }

  onPress(inputtedPin, clear, pressed) {
    console.log("Pressed: " + pressed);
    console.log("inputtedPin: " + inputtedPin)
  }


  //  <Text  style={{marginTop:wp(5),alignItems: 'stretch', marginLeft:wp(3), marginRight:wp(3) , width:wp(95), }}>




  getDataNews() {
    axios.get('http://203.113.11.167/api/news')
      .then(result => {

        this.setState({
          news_data: result.data,

          loading: false
        })
        // console.log(this.state.news_data)
        alert(JSON.stringify(result.data))
        // alert(JSON.stringify(this.state.RegionsForecast.length))
      })
  }



  render() {
    // let data = [{
    //   name: 'ทดสอบ1',
    //   detail: 'ทดสอบ1'
    // },
    // {
    //   name: 'ทดสอบ2',
    //   detail: 'ทดสอบ2'
    // }]



    const HEADER_MAX_HEIGHT = wp('50%');
    const HEADER_MIN_HEIGHT = wp('20%');
    const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

    const imageOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: 'extend',
    });
    const imageTranslate = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -50],
      extrapolate: 'clamp',
    });
    //let data = [1, 2, 3]
    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: 'clamp',
    });
    return (

      // <LinearGradient colors={['#FFF', '#DCDCDC']} style={styles.linearGradient}>

      <View style={styles.linearGradient}>

        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View >


            <Animated.View style={[{ backgroundColor: '#000', height: headerHeight, }]}>


              <View style={styles.bar}>
                <Text style={styles.title}  >TOT Easy Life</Text>


                {/* <WeatherComponent route={this.props.navigation} /> */}

              </View>
            </Animated.View>

            <Animated.View
              style={[styles.backgroundImage, { opacity: imageOpacity, transform: [{ translateY: imageTranslate }] },]}>
              <WeatherComponent route={this.props.navigation} />

            </Animated.View>



          </View>


          {/* <View style={{width:'90%', height:'10%', backgroundColor:'black', borderRadius:10, alignSelf:'center', marginTop:hp(-5)}}/> */}

          <ScrollView style={{ flex: 1, }}

            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
                tintColor='#117ffe'
                colors={['#117ffe']}
                style={{ backgroundColor: 'white' }}
              />
            }

            style={styles.fill}
            showsVerticalScrollIndicator={false}
            scrollEventThrottle={16}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }]
            )}
            style={{ flex: 1, backgroundColor: '#FFF' }}>



            <View >

              <Profile route={this.props.navigation} />

              {/* </Animated.View> */}

              <IconHome route={this.props.navigation} />
            </View>

            {/* <View style={styles.lineStylew6} /> */}





            <View style={{ flex: 1, marginBottom: wp(5) }}>
              {/* <View style={styles.lineStylew3} /> */}

              <View>
                {/* <TouchableOpacity
                  onPress={() => this.Login()}
                  style={styles.login}
                >
                  <Text style={{ fontSize: wp(4), fontWeight: 'bold', color: '#25bef8' }}> ออกจากระบบ </Text>
                </TouchableOpacity> */}
                {/* <Button
                  onPress={() => this.getDataNews()}
                  titleStyle={{ fontFamily: 'Prompt-Light', fontSize: wp(6) }}
                  title="ออกจากระบบ"
                  // type="clear"
                  buttonStyle={{ width: wp(90), alignSelf: 'center', marginBottom: wp(10), borderRadius: 10, backgroundColor: 'rgb(0,156,150)', marginBottom: wp(2) }}
                /> */}


              </View>
              <Promotion route={this.props.navigation} />
              {/* <Promotion Route={this.props.navigation} /> */}



              {/* <View style={styles.lineStylew3} /> */}

              <News route={this.props.navigation} />



              {/* <View style={styles.lineStylew3} /> */}

              <View>
                {/* <TouchableOpacity
                  onPress={() => this.Login()}
                  style={styles.login}
                >
                  <Text style={{ fontSize: wp(4), fontWeight: 'bold', color: '#25bef8' }}> ออกจากระบบ </Text>
                </TouchableOpacity> */}



              </View>



            </View>




          </ScrollView>

        </View>
      </View >
      // </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  headerHome: {
    borderBottomColor: '#FFDEAD',
    borderBottomWidth: wp('1.5'),
    borderColor: '#FFDEAD',
    justifyContent: 'center',
    marginLeft: wp('2'),
    alignItems: 'center',
    borderWidth: 2,
    borderRadius: 100,
    width: wp('8.6'),
    height: wp('8.6'),
    backgroundColor: '#0099FF'
  },
  content: {
    backgroundColor: 'white',
    height: wp(36),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  contentTitle1: {
    //marginTop:wp(2),
    fontSize: 16,
    marginBottom: wp(2),
    fontFamily: 'Prompt-Light'
  },
  contentTitle2: {
    marginTop: wp(2),
    fontSize: 16,
    marginBottom: wp(2),
    fontFamily: 'Prompt-Light'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  iconstyle4: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconstyle1: {
    justifyContent: 'center', alignSelf: 'center', margin: 1,
  },
  iconstyle2: {
    justifyContent: 'center', alignSelf: 'center', margin: 5, flexDirection: 'row'
  },
  lineStylew3: {
    borderWidth: 0.5,
    borderColor: '#DCDCDC',
    backgroundColor: '#9B9B9B',
    marginTop: wp(28.5),
    alignSelf: 'center',
    width: wp('95'),
  },
  lineStylew4: {
    borderWidth: 0.2,
    borderColor: '#DCDCDC',
    alignSelf: 'center',
    width: wp(100),
  },
  lineStylew5: {
    borderWidth: 0.5,
    borderColor: '#DCDCDC',
    backgroundColor: '#9B9B9B',
    marginTop: 10,
    alignSelf: 'center',
    width: wp('68'),
    marginRight: wp(3),

  },
  lineStylew6: {
    borderWidth: 0.25,
    borderColor: '#DCDCDC',
    alignSelf: 'center',
    width: wp(100),
  },

  iconView: {
    flexDirection: 'column',
    flex: 1,
    // backgroundColor: '#87CEFA', 
    height: hp('220'), width: wp('95'),
    borderRadius: 5,
    alignSelf: 'center',
    paddingTop: 15
  },

  linearGradient: {
    flexDirection: 'column',
    flex: 1,
    alignSelf: 'center',
    borderRadius: 5,
    height: 220,
    width: wp('100')
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },

  buttonSeeall: {
    alignItems: 'flex-end',
    // marginTop: 150,
    backgroundColor: '#00BFFF',
    color: '#fff',
    padding: 5,
    marginTop: 20,
    marginRight: 10,
    borderRadius: 5,
    justifyContent: 'flex-end',
    flexDirection: 'row'

  },

  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
    flex: 1,

  },

  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },

  fill: {
    flex: 1,
  },
  bar: {
    marginTop: wp(8),
    height: wp(8),
    alignItems: 'center',
    justifyContent: 'center',

  },
  title: {
    backgroundColor: 'transparent',
    color: 'white',
    fontSize: wp('4.5%'), fontWeight: '500',
    fontFamily: 'prompt-light'

  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#FFF',
    overflow: 'hidden',
  },
  bar1: {
    marginTop: wp(20),
    height: wp('8%'),
    alignItems: 'center',
    justifyContent: 'center',

  },

  BlockPromo: {
    flex: 1,
    backgroundColor: '#FFFAFA',
    marginLeft: wp(3),
    borderRadius: 10,
    height: 180,
    shadowColor: "#000",
    marginBottom: wp(3), marginTop: wp(3),
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.23, shadowRadius: 2.62, elevation: 4,
  },
  BlockNews: {
    flex: 1,
    backgroundColor: '#FFFAFA',
    marginLeft: wp(3),
    borderRadius: 10,
    marginBottom: wp(3),
    marginTop: wp(3),
    height: wp(68),
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.3, shadowRadius: 4.65, elevation: 15,
  }


})












