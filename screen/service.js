import React, { Component } from 'react';
import { View, Text, Linking,TouchableOpacity } from 'react-native';
import MapView, { Marker, Callout } from 'react-native-maps';

import axios from 'axios';
import MapViewDirections from 'react-native-maps-directions';
const GOOGLE_MAPS_APIKEY = 'AIzaSyCFSa14AFU5QZXkFiXLXwDvi9Ue-r_Yx1c';
import Loading from '../loading'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default  class service extends Component {
  constructor(props) {
    super(props);
    this.state = {
      myLocation: null,
      Locationlat: null,
      Locationlong: null,
      exchange_name: null,
      office_name: null,
      loading: true,
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0,
      longitudeDelta: 0,

      region: {
        latitude: 13.8864328,
       longitude: 100.5752203,
        // latitudeDelta: 0.0922,
        // longitudeDelta: 0.0421,
      },
    };
  }
  static navigationOptions = ({ navigation }) => {
    // const { params = {} } = navigation.state;

    // const params = navigation.getParam('checklogin')
    // this.state.user
    // alert(params)
    return {
      headerTransitionPreset: 'fade-in-place',
      // title: 'ชำระค่าบริการ',
      headerTitle: (<Text style={{  fontFamily: "Prompt-Light", color: "#000", fontSize: 20, fontWeight: '500' , textAlign: "center", flex: 1,  }}>ศูนย์บริการใกล้คุณ</Text>),

      headerRight: (
        <TouchableOpacity style={{ marginRight: wp(2.5) }} >
      
        </TouchableOpacity>
      ),
      headerLeft: (
        <TouchableOpacity style={{ marginLeft: wp(3), }} >
          <Ionicons name='ios-arrow-back' size={30} onPress={() => navigation.goBack()} />
        </TouchableOpacity>
      ),
      // headerRight: (
      //   <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>

      //   </HeaderButtons>
      // ),
      // //  headerTitle: <Logo/>,

      headerStyle: {
        backgroundColor: '#F9F9F9'
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        fontFamily: "Prompt-Light",
        textAlign: 'center',
        flex: 1,
        fontSize: 20,
      },
    }
  };


  componentDidMount() {
    this.getLocation();
  }
  // { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
  // alert('in')
  // Geolocation.getCurrentPosition(
  //   (position) => {
  //     console.log(position);
  //     this.setState({
  //       userLocation: {
  //         latitude: position.coords.latitude,
  //         longitude: position.coords.longitude,
  //         latitudeDelta: 0.0922,
  //         longitudeDelta: 0.0421,
  //       }
  //     })
  //     // alert(JSON.stringify(this.state.userLocation.latitude+","+this.state.userLocation.longitude))
  //     console.log("USERLOCATION: " + this.state.userLocation.latitude);
  //     this.getPlace();
  //   },
  //   (error) => {
  //     // See error code charts below.
  //     console.log(error.code, error.message);
  //   },
  //   { enableHighAccuracy: true, timeout: 60000, maximumAge: 10000 }
  // );



  getLocation() {

    // Get the current position of the user
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log(position)
        this.setState(
          (prevState) => ({
            myLocation: {
              // latitude: 13.8864328,
              // longitude: 100.5752203,
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            },
          
          } 
          ), 
          
          () => {
            this.getPlace(); 
          }
        );
      },
      (error) => this.setState({ forecast: error.message }),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }

  // getLocation() {
  //   // Get the current position of the user
  //   Geolocation.getCurrentPosition(
  //     position => {
  //       console.log(position);
  //       this.setState(
  //         prevState => ({
  //           userLocation: {
  //             latitude: position.coords.latitude,
  //             longitude: position.coords.longitude,
              // latitudeDelta: 0.0922,
              // longitudeDelta: 0.0421,
  //           }
  //         }),
  //         () => {
  //           this.getPlace();
  //         }
  //       );
  //     },
  //     error => this.getLocation()
  //     // { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
  //   );
  // }

  getPlace() {
    //  alert(JSON.stringify(this.state.myLocation.latitude+","+this.state.myLocation.longitude))
    // const url = "http://app1.toteservice.com/index.php?r=api/Nontest/ESearchPlace&lat="+this.state.myLocation.latitude+"&lng="+this.state.myLocation.longitude;
    const url = "http://203.113.11.192:3000/esearchplace/" + this.state.myLocation.latitude + "," + this.state.myLocation.longitude
    // const url = "http://203.113.11.192:3000/esearchplace/13.8864328,100.5752203"
    // const url = "http://203.113.11.192:3000/esearchplace/13.888466901200898,100.56657891602174"
    // const url = "http://203.113.11.192:3000/esearchplace/" + this.state.userLocation.latitude + "," + this.state.userLocation.longitude
    // axios({
    //   method: "get",
    //   url: url,
    //   timeout: 60 * 0.1 * 1000
    // })
    // console.log("url: " + url);
    axios.get(url)
      .then(response => {
        this.setState({ 
          Locationlat: response.data.office_lat_lon.lat, 
          Locationlong: response.data.office_lat_lon.long,
          exchange_name: response.data.data.exchange_name,
          office_name: response.data.data.office_name,
          loading: false
        
        })
        // Alert.alert(JSON.stringify(response.data.ws_status))
        console.log("Locationlat: " + this.state. Locationlat);
        console.log("Locationlong: " + this.state. Locationlong);
       
      })
      .catch(err => {
        alert(JSON.stringify(error))
      })
  }

  
  render() {
    // const origin = {latitude: 37.3318456, longitude: -122.0296002};
  //   console.log("555555: " + this.state.Locationlong);
  //  alert(this.state.myLocation)
    return (
      this.state.loading ?
        <Loading /> :
        this.state.Locationlat != null ?
          <View>
            <MapView
             
              mapType="standard"
              region={this.state.myLocation}
              showsUserLocation={true}
              showsMyLocationButton={true}
              style={{ height: "100%", width: "100%" }}
              ref={(ref) => { this.mapRef = ref }}>
              {/* {userLocationMarker} */}
              

              <Marker coordinate={{ latitude: Number(this.state.Locationlat), longitude: Number(this.state.Locationlong) }}>
                <Callout onPress={() => { Linking.openURL("https://www.google.com/maps/dir//" + this.state.Locationlat + "," + this.state.Locationlong) }}>
                  <Text style={{ color: "black", fontWeight: "500", textAlign: "center",fontFamily:'prompt-light' }}>{this.state.exchange_name}</Text>
                  <Text style={{ color: "black", textAlign: "center", fontFamily:'prompt-light' }}>{this.state.office_name}</Text>
                </Callout>
              </Marker>
              
              <MapViewDirections
                origin={this.state.myLocation}
                destination={{ latitude: Number(this.state.Locationlat), longitude: Number(this.state.Locationlong) }}
                // destination={this.state.myLocation}
                apikey={GOOGLE_MAPS_APIKEY}
                strokeWidth={3}
                strokeColor="#3d90ff"
              />
            </MapView>
          </View>
          : this.getPlace
    );
  }
}





