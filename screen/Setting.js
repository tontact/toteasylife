import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  AsyncStorage,
  TouchableHighlight,
  TouchableOpacity,
  Button,
  SafeAreaView,
  Alert
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen';
import SwitchToggle from 'react-native-switch-toggle';
import Modal from "react-native-modal";
import PinView from 'react-native-pin-view';
import Icon from "react-native-vector-icons/Ionicons";
import Icon2 from 'react-native-vector-icons/dist/FontAwesome';
import Loading from '../loading'
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class Setting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      switchOn1: true,
      switchOn2: true,
      switchOn3: false,
      switchOn4: false,
      textpassword: "แก้ไข",
      visibleModalId: null,
      pincode: null
    };
  }
  static navigationOptions = ({ navigation }) => {
     const { params = {} } = navigation.state;

    // const params = navigation.getParam('checklogin')
    // this.state.user
    // alert(params)
  
   
    return {
      
      headerTransitionPreset: 'fade-in-place',
      // title: 'ชำระค่าบริการ',
      headerTitle: (<Text style={{  fontFamily: "Prompt-Light", color: "#000", fontSize: 20, fontWeight: '500' , textAlign: "center", flex: 1, }}>ตั้งค่า</Text>),

      headerRight: (
        <TouchableOpacity style={{ marginRight: wp(2.5) }} >
          <Icon name='ios-menu' size={30} color='#000' />
        </TouchableOpacity>
      ),
      headerLeft: (
        <TouchableOpacity style={{ marginLeft: wp(3), }} >
        <Ionicons name='ios-close' size={40}   onPress={() => navigation.goBack()}/>
    </TouchableOpacity>
      ),
      // //  headerTitle: <Logo/>,

      headerStyle: {
        backgroundColor: '#F9F9F9'
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        fontFamily: "Prompt-Light",
        textAlign: 'center',
        flex: 1,
        fontSize: 20,
      },
    }
  };
  componentDidMount = async() => {
    // setTimeout(() => {
    // }, 100);
   await this.Data1();
   await this.Data2();
   await this.Data3();
   await this.Data4();
  }
  getRightText() {
    return this.state.switchOn1 ? '' : 'Eng';
  }
  getLeftText() {
    return this.state.switchOn1 ? 'ไทย' : '';
  }
  getRightText2() {
    return this.state.switchOn2 ? '' : 'ปิด';
  }
  getLeftText2() {
    return this.state.switchOn2 ? 'เปิด' : '';
  }
  getRightText3() {
    return this.state.switchOn3 ? '' : 'ปิด';
  }
  getLeftText3() {
    return this.state.switchOn3 ? 'เปิด' : '';
  }
  getRightText4() {
    return this.state.switchOn4 ? '' : 'ปิด';
  }
  getLeftText4() {
    return this.state.switchOn4 ? 'เปิด' : '';
  }
  onPress1 = () => {
    this.setState({ switchOn1: !this.state.switchOn1 });
    this.Register1();
  }
  onPress2 = () => {
    this.setState({ switchOn2: !this.state.switchOn2 });
    this.Register2();
  }
  onPress3 = () => {
    this.setState({ switchOn3: !this.state.switchOn3 });
    this.Register3();
    console.log("switchOn3", this.state.switchOn3)
    console.log("5555:" + this.state.pincode);

    if (this.state.switchOn3 !== true) {
      if (this.state.pincode === null) {
        this.setState({ visibleModal: 'bottom' })
      }
    }
  }
  onPress4 = () => {
    this.setState({ switchOn4: !this.state.switchOn4 });
    this.Register4();
  }
  Registerpincode = async () => {
    try {
      // let switchOn4 = false
      const { pincode } = this.state
      await AsyncStorage.setItem('pincode', pincode)
      // console.log(user)
    } catch (e) {

    }
  }
  Register1 = async () => {
    try {
      // let switchOn4 = false
      const { switchOn1 } = this.state
      await AsyncStorage.setItem('switchOn1', JSON.stringify(!switchOn1))
      // console.log(user)
    } catch (e) {

    }
  }
  Register2 = async () => {
    try {
      // let switchOn4 = false
      const { switchOn2 } = this.state
      await AsyncStorage.setItem('switchOn2', JSON.stringify(!switchOn2))
      // console.log(user)
    } catch (e) {

    }
  }
  Register3 = async () => {
    try {
      // let switchOn4 = false
      const { switchOn3, pincode } = this.state
      await AsyncStorage.setItem('switchOn3', JSON.stringify(!switchOn3))
      console.log("log3", !switchOn3)
      // console.log(user)
    } catch (e) {

    }
  }
  Register4 = async () => {
    try {
      // let switchOn4 = false
      const { switchOn4 } = this.state
      await AsyncStorage.setItem('switchOn4', JSON.stringify(!switchOn4))
      // console.log(user)
    } catch (e) {

    }
  }

  Data1 = () => {
    try {
      AsyncStorage.getItem('switchOn1')
        .then((value1) => {
          if (value1 !== null) {
            this.setState({ switchOn1: JSON.parse(value1) });
          } else {
            this.setState({ switchOn1: true })
          }
          // alert(this.state.switchOn3)
        });
    }
    catch (error) {
      alert(error)
    }
  }
  Data2 = () => {
    try {
      AsyncStorage.getItem('switchOn2')
        .then((value2) => {
          if (value2 !== null) {
            this.setState({ switchOn2: JSON.parse(value2) });
          } else {
            this.setState({ switchOn2: true })
          }
          // alert(this.state.switchOn3)
        });
    }
    catch (error) {
      alert(error)
    }
  }
  Data3 = async() => {
    var pincode = await AsyncStorage.getItem('pincode');

    try {
      AsyncStorage.getItem('switchOn3')
        .then((value3) => {
          if (value3 !== null) {
            if(pincode!=null){ 
              this.setState({ switchOn3: JSON.parse(value3) });
            }
           
          } else {
            this.setState({ switchOn3: false })
          }
          AsyncStorage.getItem('pincode')
            .then((valuepincode) => {
              this.setState({ pincode: valuepincode })
              // alert(this.state.pincode)
            });
          // alert(this.state.switchOn3)
        });
    }
    catch (error) {
      alert(error)
    }
  }
  Data4 = () => {
    try {
      AsyncStorage.getItem('switchOn4')
        .then((value4) => {
          if (value4 !== null) {
            this.setState({ switchOn4: JSON.parse(value4) });
            this.setState({ loading: false })
          } else {
            this.setState({ switchOn4: false })
            this.setState({ loading: false })
          }
          // alert(this.state.switchOn4)
        });
    }
    catch (error) {
      alert(error)
    }
  }
 
  Closepincode = async () => {
    try {
      // let switchOn4 = false
      const { switchOn3, pincode } = this.state
      await AsyncStorage.setItem('switchOn3', JSON.stringify(!switchOn3))
      this.setState({ visibleModal: null, switchOn3: false })
      console.log("log3", !switchOn3)
      // console.log(user)
    } catch (e) {

    }
  }
 
  onComplete = (inputtedPin, clear) => {
    this.setState({ pincode: inputtedPin })
    this.Registerpincode();
    Alert.alert(
      "บันทึกเรียบร้อย",
      "",
      [
        { text: 'ตกลง', onPress: () => this.setState({ visibleModal: null }) },
      ],
      { cancelable: false },
    );
    // alert("บันทึกเรียบร้อย")
    // this.setState({ visibleModal: null })
    clear();
  }
  onPress(inputtedPin, clear, pressed) {
    console.log("Pressed: " + pressed);
    console.log("inputtedPin: " + inputtedPin)
  }
  renderModalContent = () => (

    <View style={{ flex: 1, backgroundColor: 'white', flexDirection: "column", alignItems: "center", }}>
      <SafeAreaView style={{ alignItems: "center", flexDirection: "column" }}>
        <View
          style={{
            alignSelf: "flex-start",
            // justifyContent: "flex-start",
         
          }}
        >
           <TouchableOpacity style={{ marginLeft: wp(-5), marginTop:wp(1)}} onPress={() => this.Closepincode()} >
                    <Ionicons name='ios-arrow-back' size={30} />
                </TouchableOpacity>
        
        </View>
        <Text style={{ color: "#008DFF", fontSize: wp(8), fontFamily: "Prompt-Light", fontWeight:'500',marginTop:wp(10)}}>Enter the Code</Text>
        <Text style={{ color: "#008DFF", fontSize: wp(5), fontFamily: "Prompt-Light",fontWeight:'400' }}>กรุณาใส่รหัสผ่าน</Text>
        <PinView
          onPress={this.onPress}
          onComplete={this.onComplete}
          pinLength={6}
          buttonTextColor="#008DFF"
          buttonBgColor="white"
          inputActiveBgColor="#008DFF"
          inputBgOpacity={0.3}
          keyboardViewStyle={{ borderWidth: 1, borderColor: '#008DFF', }}
          inputViewStyle={{ width: wp(4), height: wp(4), marginTop:wp(2)}}
          keyboardViewTextStyle={{ fontSize: wp(8), fontWeight:'300' }}
          buttonDeletePosition="right"
          deleteText={<Icon name="ios-backspace" size={32} color="black" />}
          buttonDeleteStyle={{ borderWidth: 0 }}
        />
      </SafeAreaView>
    </View>

  );
  render() {
    const { rowStyle, imageItem } = styles
    return (
      this.state.loading ?
        <Loading /> :
        <ScrollView style={{ backgroundColor: "white" }}>
          {/* <View style={{ margin: wp(5) }}>
            <Text style={{ color: "black", fontSize: 20, fontWeight: "bold" }}>Language</Text>
            <View style={rowStyle}>
              <Image source={require("../alliconESL/HomeIcons/HomeIconsAW1-21.png")} style={imageItem}></Image>
              <Text style={{ color: "black", fontSize: 18, fontFamily: "Prompt-Regular", marginLeft: wp(2) }}>ภาษา</Text>
              <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row', marginBottom: wp(1) }}>
                <SwitchToggle
                  backTextRight={this.getRightText()}
                  backTextLeft={this.getLeftText()}

                  type={1}
                  buttonStyle={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    position: 'absolute'
                  }}

                  rightContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                  leftContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}

                  textRightStyle={{ color: "white", fontFamily: "Prompt-Regular" }}
                  textLeftStyle={{ color: "white", fontFamily: "Prompt-Regular" }}

                  containerStyle={{
                    width: wp(22),
                    height: wp(10),
                    borderRadius: 25,
                    padding: wp(0.75),
                  }}
                  backgroundColorOn='#008DFF'
                  backgroundColorOff='#008DFF'
                  circleStyle={{
                    width: wp(7.2),
                    height: wp(7.2),
                    borderRadius: 25,
                    backgroundColor: 'blue', // rgb(102,134,205)
                  }}
                  switchOn={this.state.switchOn1}
                  onPress={this.onPress1}
                  circleColorOff='white'
                  circleColorOn='white'
                />
              </View>
            </View>
          </View> */}

          
          <View style={{ marginLeft: wp(5), marginRight: wp(5) , marginTop:wp(5)}}>
            <Text style={{ color: "black", fontSize: wp(6), fontFamily: "Prompt-Light", fontWeight:'400' ,}}>การตั้งค่า</Text>
            {/* <View style={rowStyle}>
              <Image source={require("../alliconESL/HomeIcons/HomeIconsAW1-22.png")} style={imageItem}></Image>
              <Text style={{ color: "black", fontSize: 18, fontFamily: "Prompt-Regular", marginLeft: wp(2) }}>แจ้งเตือนชำระค่าบริการ</Text>
              <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row', marginBottom: wp(1) }}>
                <SwitchToggle
                  backTextRight={this.getRightText2()}
                  backTextLeft={this.getLeftText2()}

                  type={1}
                  buttonStyle={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    position: 'absolute'
                  }}

                  rightContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                  leftContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}

                  textRightStyle={{ color: "white", fontFamily: "Prompt-Regular" }}
                  textLeftStyle={{ color: "white", fontFamily: "Prompt-Regular" }}

                  containerStyle={{
                    width: wp(22),
                    height: wp(10),
                    borderRadius: 25,
                    padding: wp(0.75),
                  }}
                  backgroundColorOn='#008DFF'
                  backgroundColorOff='gray'
                  circleStyle={{
                    width: wp(7.2),
                    height: wp(7.2),
                    borderRadius: 25,
                    backgroundColor: 'blue', // rgb(102,134,205)
                  }}
                  switchOn={this.state.switchOn2}
                  onPress={this.onPress2}
                  circleColorOff='white'
                  circleColorOn='white'
                />
              </View>
            </View> */}

            <View style={rowStyle}>
              <Image source={require("../alliconESL/HomeIcons/HomeIconsAW1-23.png")} style={imageItem}></Image>
              <Text style={{ color: "black", fontSize: 18, fontFamily: "Prompt-Regular", marginLeft: wp(2) }}>การล็อกรหัสผ่าน</Text>
              <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row', marginBottom: wp(1) }}>
                <SwitchToggle
                  backTextRight={this.getRightText3()}
                  backTextLeft={this.getLeftText3()}

                  type={1}
                  buttonStyle={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    position: 'absolute'
                  }}

                  rightContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                  leftContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}

                  textRightStyle={{ color: "white", fontFamily: "Prompt-Regular" }}
                  textLeftStyle={{ color: "white", fontFamily: "Prompt-Regular" }}

                  containerStyle={{
                    width: wp(22),
                    height: wp(10),
                    borderRadius: 25,
                    padding: wp(0.75),
                  }}
                  backgroundColorOn='#008DFF'
                  backgroundColorOff='gray'
                  circleStyle={{
                    width: wp(7.2),
                    height: wp(7.2),
                    borderRadius: 25,
                    backgroundColor: 'blue', // rgb(102,134,205)
                  }}
                  switchOn={this.state.switchOn3}
                  onPress={this.onPress3}
                  circleColorOff='white'
                  circleColorOn='white'
                />
              </View>
            </View>
            <View style={rowStyle}>
              <Image source={require("../alliconESL/HomeIcons/HomeIconsAW1-24.png")} style={imageItem}></Image>
              <Text style={{ color: "black", fontSize: 18, fontFamily: "Prompt-Regular", marginLeft: wp(2) }}>เปลี่ยนรหัสผ่าน</Text>
             
              {this.state.pincode != null ?
                <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row', marginBottom: wp(1), marginRight: wp(6.5), }}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('changePassword')}>
                    <Text style={{ color: "#008DFF", fontSize: 18, fontFamily: "Prompt-Regular", }}>{this.state.textpassword}</Text>
                  </TouchableOpacity>
                </View>
                : null}
            </View>

            {/* <View style={rowStyle}>
              <Image source={require("../alliconESL/HomeIcons/HomeIconsAW1-25.png")} style={imageItem}></Image>
              <Text style={{ color: "black", fontSize: 18, fontFamily: "Prompt-Regular", marginLeft: wp(2) }}>Toch ID</Text>
              <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row', marginBottom: wp(1) }}>
                <SwitchToggle
                  backTextRight={this.getRightText4()}
                  backTextLeft={this.getLeftText4()}

                  type={1}
                  buttonStyle={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    position: 'absolute'
                  }}

                  rightContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                  leftContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}

                  textRightStyle={{ color: "white", fontFamily: "Prompt-Regular" }}
                  textLeftStyle={{ color: "white", fontFamily: "Prompt-Regular" }}

                  containerStyle={{
                    width: wp(22),
                    height: wp(10),
                    borderRadius: 25,
                    padding: wp(0.75),
                  }}
                  backgroundColorOn='#008DFF'
                  backgroundColorOff='gray'
                  circleStyle={{
                    width: wp(7.2),
                    height: wp(7.2),
                    borderRadius: 25,
                    backgroundColor: 'blue', // rgb(102,134,205)
                  }}
                  switchOn={this.state.switchOn4}
                  onPress={this.onPress4}
                  circleColorOff='white'
                  circleColorOn='white'
                />
              </View>
            </View> */}
          </View>
          <View>
          </View>
          <View style={{ flexDirection: "column", alignItems: "center", marginTop: wp(3) }}>
            <Image source={require("../alliconESL/HomeIcons/HomeIconsAW1-26.png")} style={{ width: wp(28), height: wp(15), marginTop: wp(12) }}></Image>
            <Text style={{ color: "black", fontSize: 14, fontFamily: "Prompt-Regular", marginTop: wp(3) }}>แอพพลิเคชั่นที่ช่วยให้ชีวิตคุณง่ายขึ้น</Text>
            <Text style={{ color: "black", fontSize: 10, fontFamily: "Prompt-Regular", marginTop: wp(5) }}>TOT PUBLIC COMPANY LIMITED</Text>
          </View>
          <Modal
            isVisible={this.state.visibleModal === 'bottom'}
            onSwipeComplete={() => this.setState({ visibleModal: null })}
            // swipeDirection={['up', 'left', 'right', 'down']}
            style={styles.bottomModal}
          >
            {this.renderModalContent()}
          </Modal>
        </ScrollView>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  rowStyle: {
    height: wp(14),
    flex: 1,
    flexDirection: "row",
    backgroundColor: "white",
    alignItems: "center",
    marginTop: wp(1.5),
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  },
  imageItem: {
    width: wp(6),
    height: wp(6),
    resizeMode: 'stretch',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  content: {
    backgroundColor: 'red',
    padding: wp(20),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
});
