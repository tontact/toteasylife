import React, { Component } from 'react';
import { View, Text, Linking, Alert, StyleSheet, TextInput, Image, TouchableOpacity, AsyncStorage, Animated, FlatList, SafeAreaView, Button, } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';
import {
  widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';

import { Icon, ThemeConsumer, CheckBox } from 'react-native-elements'


import { ScrollView, TouchableWithoutFeedback } from 'react-native-gesture-handler';
// import Swiper from 'react-native-swiper';

import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';


import axios from 'axios';
import base64 from 'react-native-base64';
import DeviceInfo from 'react-native-device-info';
import ImageView from 'react-native-image-view';
import Loading from '../loading'
import Spinner from 'react-native-loading-spinner-overlay';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import QRCode from 'react-native-qrcode-svg';

const images = [
  {
    source: require('../img/example.jpg'),
    title: 'TOT Easy life',
    width: wp(200),
    height: wp(60),

  },
];

export default class Account extends Component {



  state = {
    loading: false,
    data: [],
    userid: 0,



    bano: [],
    svno: [],
    payment: [],

    Ipaddress: '111.111.111.111',
    // mbrid: '',
    // usrtoken: '',
    baid: '',
    // balist: [],

    opencheckboxdel: false,
    baiddelete: '',
    serviceiddelete: '',

    servicenumber: '',


    // urlpayment: '',
    checked: [],
    baidpayment: [],
    opencheckbox: false,
    checkall: false,
    spinner: false,
    checkbill: false,



    email: '',
    total: '',
    totallist: [{}],

    IDcard: '',

    userID: '',
    baNumber: [],
    BaID: '',
    id: '',
    visibleModal: false,
    test: [{ id: 1, name: "ton" }, { id: 2, name: "benz" }],


    modalVisible: true,
    isImageViewVisible: false,
    qrcodeScan: ''
  }


  constructor(props) {
    super(props, {
      scrollOffset: null,
    });

    this.scrollViewRef = React.createRef();
  }

  handleOnScroll = event => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y,
    });
  };
  handleScrollTo = p => {
    if (this.scrollViewRef.current) {
      this.scrollViewRef.current.scrollTo(p);
    }
  };



  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    // const params = navigation.getParam('checklogin')
    // this.state.user
    // alert(params)
    return {
      headerTitle: (<Text style={{ fontFamily: "Prompt-Light", color: "#000", textAlign: "center", flex: 1, fontSize: 20, fontWeight: '500' }}>ค่าบริการ</Text>),


      headerRight: (
        params.checkboxdel == true ?
          <TouchableOpacity style={{ marginRight: wp(3) }} onPress={() => params.opencheckboxdel()} >
            <Text style={{ fontFamily: 'Prompt-SemiBold', marginRight: wp(2), fontSize: wp(4) }}>{'เสร็จสิ้น'}</Text>
          </TouchableOpacity>
          :
          <TouchableOpacity style={{ marginRight: wp(3) }} onPress={() => params.opencheckboxdel()} >
            <Image style={{ width: wp(6), height: wp(7) }} source={require('../alliconESL/Iconnewsetapp/PAYTOTeasylifeAW1copy12.png')} />
          </TouchableOpacity>
      ),

      headerLeft: (
        <TouchableOpacity style={{ marginLeft: wp(3), }} >
          <Ionicons name='ios-arrow-back' size={30} onPress={() => navigation.goBack()} />
        </TouchableOpacity>
      ),
      // //  headerTitle: <Logo/>,

      headerStyle: {
        backgroundColor: '#F9F9F9'
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        fontFamily: "Prompt-Light",
        textAlign: 'center',
        flex: 1,
        fontSize: 20,
        fontWeight: 500,
        zoomImage: false,


      },
    }
  };



  componentDidMount = async () => {
    this.setState({ spinner: true });
    this.props.navigation.setParams({
      opencheckboxdel: this.opencheckboxdel,
      checkboxdel: false
    })
    DeviceInfo.getIpAddress().then(ip => {
      // console.log("IpAddress:" + ip);
      this.setState({ Ipaddress: ip })
    });
    var IDcard = await AsyncStorage.getItem('citizenID');
    var id = await AsyncStorage.getItem('userid');
    // console.log(IDcard);

    await this.setState({ id: id })
    // await this.setState({ IDcard: IDcard })
    // await this.GetBA();
    await this.getProfile();
    await this.getBa();

    // let intialCheck = this.state.balist.map(x => false);
    // await this.setState({ checked: intialCheck })

  }


  getProfile() {
    const url = "http://203.113.11.167/api/myuser/" + this.state.id;
    // console.log(url);
    // const headers = {
    //   'Content-Type': 'application/json',
    //   'Authorization': 'JWT fefege...'
    // }
    axios.get(url)
      .then(result => {
        // var userprofile = JSON.stringify(result);
        // console.log(userprofile);
        // console.log(result);
        // console.log(JSON.stringify(result.data.data.id) + 'checkbill');
        this.setState({
          data1: result.data.data,
          Firstname: result.data.data.name,
          Firstname1: result.data.data.name,
          Lastname: result.data.data.lastname,
          Lastname1: result.data.data.lastname,
          Mobileno: result.data.data.tel,
          Mobileno1: result.data.data.tel,
          Email: result.data.data.email,
          Email1: result.data.data.email,
          Personalid: result.data.data.IDcard,
          Address: result.data.data.address,
          Province: result.data.data.province,
          District: result.data.data.district,
          Subdistrict: result.data.data.subdistrict,
          Postcode: result.data.data.postcode

        })
        // console.log(this.state.Firstname1)
        // console.log(this.state.Lastname1)

        // console.log(this.state.data1, 'Myprofile')

      })
      .catch(err => {
        alert(JSON.stringify(error));
      })
  }



  // setBA() {

  //   this.setState({ spinner: !this.state.spinner });
  //   console.log(this.state.id);
  //   const url = "http://203.113.11.167/api/addBA";



  //   if (this.state.BaID.length == 0) {
  //     Alert.alert(
  //       "กรุณากรอกหมายเลข BA",
  //       "",
  //       [
  //         { text: 'ตกลง' },
  //       ],
  //       { cancelable: false },
  //     );
  //   } else if (this.state.BaID.length < 12) {
  //     Alert.alert(
  //       "กรุณากรอกหมายเลข BA ให้ครบ 12 หลัก",
  //       "",
  //       [
  //         { text: 'ตกลง' },
  //       ],
  //       { cancelable: false },
  //     );
  //   }

  //   else {

  //     axios.post(url, {

  //       userID: this.state.id,
  //       baNumber: this.state.BaID,
  //     },

  //     ).then(result => {
  //       // console.log(this.state.userID);
  //       console.log(this.state.baNumber);
  //       // console.log(this.state.servicenumber);
  //       // console.log(result);


  //       if (result.data.message == 'หมายเลข ฺBA นี้มีอยู่ในระบบแล้ว') {
  //         Alert.alert(
  //           "เพิ่ม BA numberไม่สำเร็จ",
  //           "หมายเลข BA นี้มีอยู่ในระบบแล้ว",
  //           [
  //             { text: 'ตกลง', },
  //           ],
  //           { cancelable: false },
  //         );
  //         //console.log(result.data.data.IDcard);
  //       } else {
  //         Alert.alert(
  //           "เพิ่ม BA number สำเร็จ",
  //           "",
  //           [
  //             { text: 'ตกลง', onPress: () => this.success() },
  //           ],
  //           { cancelable: false },
  //         );
  //       }

  //     }).catch(e => {
  //       console.log(e);

  //     });
  //   }
  // }


  addNotiba() {
    const url = "http://app1.toteservice.com/index.php?r=api/Easylife/addBibil010_2020&username=easylife&password=e@sylifeP@ss&udid=" + this.state.id + "&svno=test00&bano=" + this.state.BaID;
    const url1 = "http://203.113.11.167/api/addBA";
    // console.log(url);
    axios.post(url)
      .then(result => {
        // this.setState({
        //   notibano: result.data.Debt_bano,
        //   loading: false,
        // })

        if (result.data.status == 'error01') {
          Alert.alert(
            "ขออภัย!",
            "หมายเลขบริการนี้ได้ลงทะเบียนไว้แล้ว",
            [
              { text: 'ตกลง', },
            ],
            { cancelable: false },
          );
        }
        else if (result.data.status == 'error06') {
          Alert.alert(
            "ระบบตรวจสอบพบว่ารหัสลูกค้าที่ท่านกรอกไม่ตรงกับฐานข้อมูลในระบบ",
            "กรุณาตรวจสอบข้อมูลให้ตรงกับใบแจ้งค่าใช้บริการและกรอกใหม่อีกครั้งค่ะ",
            [
              { text: 'ตกลง', },
            ],
            { cancelable: false },
          );
        }
        else if (result.data.status == 'error07') {
          Alert.alert(
            "กรณาระบุหมายเลข BA 12 หลัก",
            "",
            [
              { text: 'ตกลง', },
            ],
            { cancelable: false },
          );
        }

        else if (result.data.status == 'error08') {
          Alert.alert(
            "รหัสลูกค้าไม่ถูกต้อง",
            "กรุณากรอกข้อมูลให้ถูกต้องตามที่แสดงไว้บนหน้าบิล",
            [
              { text: 'ตกลง', },
            ],
            { cancelable: false },
          );
        }


        else if (result.data.status == 'success') {
          axios.post(url1, {

            userID: this.state.id,
            baNumber: this.state.BaID,
          },

          ).then(result => {
            // console.log(this.state.userID);
            // console.log(this.state.baNumber);
            // console.log(this.state.servicenumber);
            // console.log(result);


            if (result.data.message == 'หมายเลข BA นี้มีอยู่ในระบบแล้ว') {
              Alert.alert(
                "เพิ่ม BA numberไม่สำเร็จ",
                "หมายเลข BA นี้มีอยู่ในระบบแล้ว",
                [
                  { text: 'ตกลง', },
                ],
                { cancelable: false },
              );
              //console.log(result.data.data.IDcard);
            } else {
              Alert.alert(
                "เพิ่ม BA number สำเร็จ",
                "",
                [
                  { text: 'ตกลง', onPress: () => this.success() },
                ],
                { cancelable: false },
              );
            }

          }).catch(e => {
            console.log(e);

          });
        }

      })
      .catch(err => {
        alert(JSON.stringify(err));
      })
  }



  getBa() {
    const url = "http://203.113.11.167/api/getBaUser/" + this.state.id;
    // console.log(url);
    // const headers = {
    //   'Content-Type': 'application/json',
    //   'Authorization': 'JWT fefege...'
    // }
    axios.get(url)
      .then(result => {
        // var userprofile = JSON.stringify(result);
        // console.log(userprofile);


        // alert(JSON.stringify(result.data));
        //  AsyncStorage.setItem('baNumber', (result.data));


        this.setState({

          baNumber: result.data.map((item) => {
            return { ...item, status: false }
          })


        })


        // console.log(this.state.Firstname1)
        // console.log(this.state.Lastname1)
        // console.log(this.state.baNumber, 'getBA');
        // console.log(result.data[2].baNumber, "notice")


      }).finally((result) => {
        this.opencheckbox()
      })
      .catch(err => {
        // alert(JSON.stringify(error));
      })
  }


  getDetailBa = async (item, QRCode) => {
    // alert(item)
    this.setState({ spinner: !this.state.spinner });
    // await this.setState({
    //   baNumber: item.baNumber,
    //   serviceid: item.serviceid,

    // })
    const url = "http://app1.toteservice.com/index.php?r=api/Easylife/viewbibil010&username=easylife&password=e@sylifeP@ss&biid=202457&udid=00000&bano=" + item
    axios.get(url)
      .then(result => {
        console.log(result, 'modalBA');
        if (result.data.outstandingDebtMny > 0) {
          this.setState({ spinner: !this.state.spinner });
          var total = result.data.outstandingDebtMny / 10000000
          // console.log(total)

          this.setState({
            totallist: Object.values(result.data.inv01),
            total: parseFloat(total),
            qrcodeScan: QRCode,
            visibleModal: true

          })
          // console.log(result);

          // console.log(this.state.totallist);

          // console.log(this.state.total);

        } else {
          // this.setState({ spinner: !this.state.spinner });
          // alert("ไม่มียอดค้างชำระ")
          Alert.alert(
            "ไม่มียอดค้างชำระ",
            "",
            [
              { text: 'OK', onPress: () => this.setState({ spinner: !this.state.spinner }) },
            ],
            { cancelable: false },
          );
        }

        // setTimeout(() => {
        //   this.setState({ visibleModal: 'bottom' })
        // }, 5000) 
        // alert(JSON.stringify(this.state.totallist))
      })
      .catch(err => {
        // alert(JSON.stringify(error));
      })
  }


  getPayment() {
    // if(this.state.baidpayment == null){
    //   var ba_list = this.state.baid
    // } else{
    //   var ba_list = this.state.baidpayment
    // }
    // var baidpayment = this.state.baidpayment

    this.setState({ spinner: !this.state.spinner });
    const url = "https://www.toteservice.com/esapi/AipEasyPay/setpymbill";

    const headers = {
      'Content-Type': 'application/json',
      'Authorization': 'JWT fefege...'
    }

    axios.post(url, {
      ipaddress: this.state.Ipaddress,
      auth_contracts: "easy1ife2ew@",
      auth_code: "s3f1@6d54b",
      lang: "TH",
      easylife_member_id: JSON.stringify(this.state.id),
      firstname: this.state.data1.name,
      lastname: this.state.data1.lastname,
      ba_list: this.state.baidpayment,
      mobile_contact: this.state.data1.tel,
      email: this.state.data1.email,
      payment_qrcode_type: false
    },

      {
        headers: headers
      }

    ).then(result => {

      console.log(this.state.mobile_contact, 'mobile');

      if (result.data.statuscode == 200) {
        // var getbaid = (result.data.result.balist.fixed[0].baid)
        // console.log(result.data.rurl);

        this.setState({ spinner: !this.state.spinner });
        this.props.navigation.navigate('eservicePage', { url: result.data.rurl.toString() })
        // Linking.openURL()


        this.setState({ urlpayment: result.data.rurl })
        // this.setState({ loading: false })
        // alert(this.state.urlpayment)
      } else {

        Alert.alert(
          "กรุณาเลือกชำระค่าบริการ",
          "",
          [
            { text: 'ตกลง', onPress: () => this.setState({ spinner: !this.state.spinner }) },
          ],
          { cancelable: false },
        );
        // console.log(result.data.statuscode);
        // alert(JSON.stringify(result.data.statuscode))
      }

    }).catch(e => {
      console.log(e);
    });
  }



  success() {

    this.getProfile()
    this.getBa()
    // this.addNotiba()
    this.setState({ spinner: !this.state.spinner });
    this.setState(this.state.baNumber = null)
    this.setState(this.state.servicenumber = null)
    this.setState({ visiblePicker: false })

  }









  opencheckboxdel = () => {
    this.setState({ opencheckboxdel: !this.state.opencheckboxdel, checkbill: false })
    this.setState({ opencheckbox: false })
    this.props.navigation.setParams({
      checkboxdel: !this.props.navigation.getParam('checkboxdel')
    })
  }




  checkeddel = async (index) => {
    let baNumber = this.state.baNumber;
    var baiddelete = baNumber[index].baNumber;


    //var serviceiddelete = balist[index].serviceid;
    // var serviceiddelete1 = serviceiddelete.toString();

    const url = "http://203.113.11.167/api/delBA";

    axios.post(url, {

      userID: this.state.id,
      baNumber: baiddelete,

    },

    ).then(result => {
      //  console.log(this.state.baiddelete);
      //   console.log(this.state.serviceiddelete);
      // console.log(result);

      if (result.data.Message == 'ลบข้อมูลเรียบร้อยแล้ว') {
        // alert('ddd')

        Alert.alert(
          "ลบรหัสลูกค้าสำเร็จ",
          "",
          [
            // { text: 'cancel',style: 'cancel' ,onPress: () => console.log('Cancel Pressed')},
            { text: 'ตกลง', onPress: () => this.success1() },
          ],
          { cancelable: false },
        );

      } else {
        Alert.alert(
          "ลบรหัสลูกค้าไม่สำเร็จ",

          [
            { text: 'ตกลง', onPress: () => this.setState({ spinner: false }) },
          ],
          { cancelable: false },
        );
      }

    })

  }



  delNotiba = async (index) => {
    // Alert.alert(index)
    let baNumber = this.state.baNumber;
    var baiddelete = baNumber[index].baNumber;
    // Alert.alert(baiddelete)
    const url = "http://app1.toteservice.com/index.php?r=api/Easylife/delBibil010&username=easylife&password=e@sylifeP@ss&udid=" + this.state.id + "&svno=test00&bano=" + baiddelete
    const url1 = "http://203.113.11.167/api/delBA";

    // console.log(url);

    axios.post(url)
      .then(result => {

        //  console.log("delNotiba:" + result);

      })
      .catch(err => {
        alert(JSON.stringify(err));
      })


    axios.post(url1, {

      userID: this.state.id,
      baNumber: baiddelete,

    },

    ).then(result => {
      //  console.log(this.state.baiddelete);
      //   console.log(this.state.serviceiddelete);
      //  console.log(result);

      if (result.data.Message == 'ลบข้อมูลเรียบร้อยแล้ว') {
        // alert('ddd')

        Alert.alert(
          "ลบรหัสลูกค้าสำเร็จ",
          "",
          [
            // { text: 'cancel',style: 'cancel' ,onPress: () => console.log('Cancel Pressed')},
            { text: 'ตกลง', onPress: () => this.success1() },
          ],
          { cancelable: false },
        );

      } else {
        Alert.alert(
          "ลบรหัสลูกค้าไม่สำเร็จ",

          [
            { text: 'ตกลง', onPress: () => this.setState({ spinner: false }) },
          ],
          { cancelable: false },
        );
      }

    })

  }





  async opencheckbox() {

    // console.log("baNumber1", this.state.baNumber[0].baNumber);

    for (let i = 0; i < this.state.baNumber.length; i++) {
      var url = "http://app1.toteservice.com/index.php?r=api/Easylife/viewbibil010&username=easylife&password=e@sylifeP@ss&biid=202457&udid=00000&bano=" + this.state.baNumber[i].baNumber
      await axios.get(url)
        .then(result => {
          console.log(result, 'result',);
          //  console.log(this.state.baNumber.length)
          // var fullname = result.data.ba.LegalName

          var res = result.data.ba.lname.split("|").join(" ");
          // baNumber[i].fullname = res

          let baNumber = this.state.baNumber;
          baNumber[i].invo1 = parseInt(result.data.outstandingDebtMny) > 0 ? Object.values(result.data.inv01) : []
          baNumber[i].fullname = res
          baNumber[i].sumBalance = result.data.outstandingDebtMny / (10000000)
          // baNumber[i].statusinvo = parseInt(result.data.outstandingDebtMny) > 0 ? true : false
          baNumber[i].statusinvo = true

          baNumber[i].QRCode = result.data.textQR == undefined ? "ไม่มียอดค้างชำระ" : result.data.textQR
          // baNumber[i].QRCode = result.data.textQR
          baNumber[i].serviceno = result.data.ba.primaryServiceNo
          // baNumber[i] = !baNumber[i];

          this.setState({ baNumber })
        })


      // console.log("baNumber1", this.state.baNumber);

    }
    this.setState({ spinner: false })
    // console.log('baNumber', this.state.baNumber);



  }



  // deleteornot() {
  //   Alert.alert(
  //     "ต้องการลบหรือไม่",
  //     JSON.stringify(result.data.statuscode),
  //     [
  //       { text: 'OK', onPress: () => this.checked1() },
  //     ],
  //     { cancelable: false },
  //   );
  // }
  async payAll() {

    // for (let i = 0; i < this.state.baNumber.length; i++) {
    //   if (this.state.baNumber[i].statusinvo) {
    //     await this.checked2(this.state.baNumber[i], i)
    //   }
    // }
    this.setState({
      // checkall: !this.state.checkall , 
      checkbill: true,
      opencheckboxdel: false
    })
  }

  checked2 = (item, index) => {
    let baNumber = this.state.baNumber;
    baNumber[index].status = !baNumber[index].status;
    this.setState({ baNumber })
    this.submit2()

  }
  isBigEnough = (element) => {
    if (element.status != false) {
      // console.log("element",element)
      return element.baNumber;
    }

  }


  success1() {
    this.getProfile()
    this.getBa()
    this.delNotiba()
    this.setState(this.state.baiddelete = null)
    // this.setState(this.state.serviceiddelete = null)
  }



  submit2 = () => {
    // console.log('baidpayment',this.state.baidpayment)
    var filtered = this.state.baNumber.filter(this.isBigEnough)
      .map((item, index) => {
        // console.log('baNumber', item.baNumber)
        return item.baNumber
      });
    // console.log('filtered', filtered);
    this.setState({ baidpayment: filtered })


  }




  // success() {
  //   this.getProfile()
  //   this.getBa()
  //   this.addNotiba()
  //   this.setState(this.state.baNumber = null)

  //   this.setState(this.state.servicenumber = null)
  //   this.setState({ visiblePicker: false })
  //   this.setState({ spinner: !this.state.spinner });
  // }


  renderItemtotal(item) {


    return (

      <View>

        <View>
          <View style={{ alignItems: "center", width: wp(90), }}>

            <View style={{ width: wp(90), height: wp(40), }}>
              <View >
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), color: '#1b7af7', marginTop: wp(2), fontWeight: '400', alignSelf: 'center' }}>เลขที่ใบแจ้งบริการ</Text>
                <Text style={{ fontFamily: 'Prompt-SemiBold', fontSize: wp(5), color: '#1b7af7', letterSpacing: 1.0, alignSelf: 'center' }}> {item.invoiceNumber}</Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: wp(2), alignSelf: 'center', }}>
                {/* <Image
                  source={require('../ICON_Png/กรอบเท่ากัน/icondesign-13.png')}
                  style={{ width: 22, height: 22, borderRadius: 22 / 2 }}
                /> */}
                <View style={{ flexDirection: 'column', marginTop: wp(1), alignSelf: 'center' }}>
                  <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4) }}>ครบกำหนดจ่าย</Text>
                  <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), alignSelf: 'center' }}>{item.paymentDueDate}</Text>
                </View>

                <View style={styles.lineStylew5} />

                <View style={{ flexDirection: 'column', marginTop: wp(1), alignSelf: 'center' }}>
                  <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), }}>ยอดค้างชำระ</Text>
                  <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), alignSelf: 'center' }}>{item.outstandingDebtMny} บาท</Text>
                </View>
              </View>


            </View>


          </View>


        </View>

      </View>
    );
  }





  renderModalDetailBa() {

    return (


      <View style={{ height: wp(150), backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 20, borderTopRightRadius: 20, borderColor: 'rgba(0, 0, 0, 0.1)', }}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          ref={this.scrollViewRef}
          onScroll={this.handleOnScroll}
          scrollEventThrottle={16}>

          <FlatList
            style={{ marginTop: wp(2) }}
            data={this.state.totallist}
            renderItem={({ item }) => this.renderItemtotal(item)}
          // extraData={this.state}
          >
          </FlatList>
          <View style={{ alignItems: 'center', width: wp(90), marginTop: wp(10) }}>

            <View style={{ flexDirection: 'row', }}>
              <View style={{
                width: wp(10), height: wp(10), marginRight: wp(30),
                borderWidth: wp(2),
                borderLeftColor: '#3d90ff',
                borderBottomColor: 'transparent',
                borderRightColor: 'transparent',
                borderTopColor: '#3d90ff',

              }} />

              <View style={{
                width: wp(10), height: wp(10), marginRight: wp(0),
                borderWidth: wp(2),
                borderLeftColor: 'transparent',
                borderBottomColor: 'transparent',
                borderRightColor: '#3d90ff',
                borderTopColor: '#3d90ff'
              }} />
            </View>

            <View style={{ marginTop: wp(-5) }}>
              <QRCode

                value={this.state.qrcodeScan}
                logo={this.state.qrcodeScan}
                logoSize={20}
                size={150}
                logoBackgroundColor='transparent'
              />
            </View>
            <View style={{ flexDirection: 'row', marginTop: wp(-5) }}>
              <View style={{
                width: wp(10), height: wp(10), marginRight: wp(30),
                borderWidth: wp(2),
                borderLeftColor: '#3d90ff',
                borderBottomColor: '#3d90ff',
                borderRightColor: 'transparent',
                borderTopColor: 'transparent',
              }} />

              <View style={{
                width: wp(10), height: wp(10), marginRight: wp(0),
                borderWidth: wp(2),
                borderLeftColor: 'transparent',
                borderBottomColor: '#3d90ff',
                borderRightColor: '#3d90ff',
                borderTopColor: 'transparent',
              }} />
            </View>
          </View>


        </ScrollView>


        <Text style={{ fontFamily: 'Prompt-SemiBold', fontSize: wp(5), color: '#1b7af7', marginTop: wp(2) }}>ยอดรวมค้างชำระ : {this.state.total} บาท</Text>

        <View style={{ marginBottom: wp('3%') }}>
          <TouchableOpacity
            onPress={() => this.setState({ visibleModal: false })}

          >
            <Text style={{ fontWeight: '500', fontFamily: 'prompt-light', fontSize: wp(6), color: '#1b7af7', marginBottom: wp(2) }}>ปิด</Text>
          </TouchableOpacity>
        </View>
      </View>

    );
  }

  renderItem(item, index,) {
    // const { } = style;

    // console.log(item.QRCode,'qrcode')

    return (
      this.state.loading ?
        <Loading /> :
        <View>
          {/* <View>
          <View>
          <Text>{item.id}</Text>
        </View>

        </View> */}

          <View style={{ flex: 1 }}>
            {this.state.opencheckboxdel == true ?

              <TouchableOpacity onPress={() => this.delNotiba(index)}>
                <Image style={{ marginTop: wp(0), marginLeft: wp(0), width: wp(6), height: wp(6), position: 'absolute' }} source={require('../alliconESL/Iconnewsetapp/PAYTOTeasylifeAW1copy14.png')} />
              </TouchableOpacity>

              : null
            }
            <View style={{ alignSelf: 'center', flex: 1, }} >

              <View style={{ marginTop: wp(1), marginBottom: wp(2) }}>

                <TouchableOpacity
                  onPress={() => this.getDetailBa(item.baNumber, item.QRCode)}
                >

                  <View style={{
                    width: wp(90), height: wp(40), backgroundColor: '#FFF', borderRadius: 10, justifyContent: 'center', alignSelf: 'center', flexDirection: 'column'
                    , shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14,
                  }}>

                    <View style={{ flexDirection: 'column' }}>
                      <View style={{ flexDirection: 'row', }}>

                        <View style={{ alignSelf: 'center', marginLeft: wp(4), }}>

                          {/* <View style={{ flexDirection: 'row' }}>
                          <View style={{
                            width: wp(5), height: wp(5), marginRight: wp(15),
                            borderWidth: wp(1.3),
                            borderLeftColor: '#3d90ff',
                            borderBottomColor: 'transparent',
                            borderRightColor: 'transparent',
                            borderTopColor: '#3d90ff',

                          }} />

                          <View style={{
                           width: wp(5), height: wp(5), marginRight: wp(0),
                            borderWidth: wp(1.3),
                            borderLeftColor: 'transparent',
                            borderBottomColor: 'transparent',
                            borderRightColor: '#3d90ff',
                            borderTopColor: '#3d90ff'
                          }} />
                        </View> */}


                          {/* <View style={{marginLeft:wp(2), marginTop: wp(-3)}}> */}
                          <View style={{}}>
                            {
                              item.sumBalance == 0 ?
                                <Image
                                  source={require('../ICON_Png/กรอบเท่ากัน/PAYIconsAW1copy_06.png')}
                                  style={{ width: wp(27), height: wp(27), borderRadius: wp(27) / 2, shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, }}
                                /> :

                                <QRCode

                                  value={item.QRCode}
                                  logo={item.QRCode}
                                  logoSize={20}
                                  size={110}
                                  logoBackgroundColor='transparent'
                                />


                            }



                          </View>

                          {/* <View style={{ flexDirection: 'row', marginTop: wp(-6) }}>
                          <View style={{
                            width: wp(5), height: wp(5), marginRight: wp(15),
                            borderWidth: wp(1.3),
                            borderLeftColor: '#3d90ff',
                            borderBottomColor: '#3d90ff',
                            borderRightColor: 'transparent',
                            borderTopColor: 'transparent',
                          }} />

                          <View style={{
                       width: wp(5), height: wp(5), marginRight: wp(0),
                            borderWidth: wp(1.3),
                            borderLeftColor: 'transparent',
                            borderBottomColor: '#3d90ff',
                            borderRightColor: '#3d90ff',
                            borderTopColor: 'transparent',
                          }} />
                        </View> */}

                        </View>

                        {/* <Image
                        source={require('../ICON_Png/กรอบเท่ากัน/PAYIconsAW1copy_06.png')}
                        style={{ width: wp(27), height: wp(27), borderRadius: wp(27) / 2, shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, }}
                      /> */}

                        <View style={{ flexDirection: 'column', marginLeft: wp(2) }}>
                          <View style={{ flexDirection: 'row' }}>



                            <Text style={{ fontFamily: 'Prompt-Light', fontWeight: '500', fontSize: wp(4), width: wp(55), color: '#3d90ff', marginLeft: wp(2.5) }}>{item.fullname}</Text>




                          </View>

                          <View style={{ flexDirection: 'row', marginHorizontal: wp(1), }}>

                            {this.state.baNumber == "" ?
                              <View style={{ flexDirection: 'row', }}>
                                <Image
                                  source={require('../ICON_Png/กรอบเท่ากัน/icondesign-13.png')}
                                  style={{ width: wp(10), height: wp(10), borderRadius: wp(10) / 2 }}
                                />
                                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(3.5), alignSelf: 'center', marginLeft: wp(0.5) }}>รหัสลูกค้า: ไม่มีข้อมูล</Text>
                              </View>
                              :
                              <View style={{ flexDirection: 'row', }}>
                                <Image
                                  source={require('../ICON_Png/กรอบเท่ากัน/icondesign-13.png')}
                                  style={{ width: wp(10), height: wp(10), borderRadius: wp(10) / 2 }}
                                />
                                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(3.5), alignSelf: 'center', marginLeft: wp(0.5) }}>รหัสลูกค้า: {item.baNumber}</Text>

                              </View>}

                          </View>
                          <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(3.5), marginLeft: wp(2.5) }}>หมายเลขบริการ: {item.serviceno}</Text>

                          <View style={{ flexDirection: 'row', marginLeft: wp(1), marginBottom: wp(0.5) }}>

                            <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(3.5), alignSelf: 'center', marginLeft: wp(1.3), marginTop: wp(1) }}>ยอดรวมค้างชำระ: {item.sumBalance} บาท</Text>
                          </View>


                          {this.state.baNumber == null ?

                            <View>

                              <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(3), color: 'black', marginLeft: wp(2), marginTop: wp(1), }}> กรุณาเพิ่มหมายเลขรหัสลูกค้า เพื่อแสดงยอดค้างชำระ </Text>
                            </View>
                            :
                            <View style={{ flexDirection: 'row', }}>
                              {/* <Image
                              source={require('../alliconESL/PayIcons/PAYIconsAW1copy06.png')}
                              style={{ width: 25, height: 25, borderRadius: 25 / 2, marginLeft: wp(1.3), }}
                            /> */}
                              {/* <Text style={{ fontFamily: 'Prompt-Light', fontSize: 11, alignSelf: 'center' }}>ยอดค้างชำระ:</Text>
                          <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4.5), marginTop: wp(-0.5), fontWeight: '400' }}> 2200.25 </Text>
                          <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4.5), marginTop: wp(-0.5), fontWeight: '400' }}>บาท</Text> */}
                              <TouchableOpacity onPress={() => this.getDetailBa(item.baNumber, item.QRCode)}>
                                <View style={{ flexDirection: 'row', marginTop: wp(0.5) }}>
                                  <Text style={{ color: 'black', fontFamily: 'Prompt-Light', fontWeight: '500', fontSize: wp(3.5), color: '#3d90ff', marginLeft: wp(2.5) }}>ดูรายละเอียดยอดค้างชำระ {">"}</Text>

                                </View>
                              </TouchableOpacity>


                            </View>
                          }



                        </View>


                        {/* {this.state.opencheckbox == true ? */}





                        {/* <View style={{ backgroundColor: 'red', width:wp(10),  marginLeft:wp(-10),height:wp(12), alignItems:'center',}}> */}

                        {this.state.checkbill == true ?
                          <View style={{ width: wp(10), height: wp(12), alignItems: 'center', marginTop: wp(21), marginLeft: wp(-12), }}>
                            {item.statusinvo ?

                              <CheckBox
                                center
                                containerStyle={{ alignSelf: 'center', width: wp(10), height: wp(12) }}
                                // title={item.baid}
                                onPress={() => this.checked2(item, index)}
                                checked={item.status}
                              />

                              :
                              null
                            }
                          </View> : null

                        }




                      </View>



                    </View>


                  </View>

                </TouchableOpacity>


              </View>


            </View>

          </View>
        </View>
    );
  }






  render() {
    return (
      this.state.loading ?

        <Loading /> :

        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>

          {this.state.ba != "" ?
            <View style={{ alignSelf: 'center', width: wp(50), height: wp(10), borderRadius: 20, marginTop: wp(5), }} >
              <TouchableOpacity onPress={() => this.payAll()}>
                <LinearGradient style={styles.pay2} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#3ad1ff', '#117ffe']}>
                  <Text style={{ fontSize: wp(4), fontWeight: '500', color: '#fff', fontFamily: 'Prompt-Light' }}>{this.state.checkall ? "เลือกรายการที่ต้องการ" : "เลือกรายการที่ต้องการ"} </Text>
                </LinearGradient>
              </TouchableOpacity>

            </View> : null
          }

          <FlatList
            style={{ width: wp(100), marginTop: wp(5), }}

            data={this.state.baNumber}
            //data={this.state.data1}
            ref={ref => (this.flatlist = ref)}
            // extraData={this.state}
            renderItem={({ item, index, }) => this.renderItem(item, index)}>


          </FlatList>


          {
            this.state.visiblePicker ?

              <View style={{ marginTop: wp(8), alignItems: 'center', }}>

                <View style={{
                  width: wp(90), height: wp(36.5), backgroundColor: '#FFF', borderRadius: 10, alignItems: 'center', flexDirection: 'column'
                  , shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, marginBottom: wp(12)
                }}>


                  <View style={{ marginTop: wp(2) }}>

                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), color: '#3d90ff', marginTop: wp(1), alignSelf: 'center', fontWeight: '500' }}>กรุณาเพิ่มหมายเลขรหัสลูกค้า</Text>


                    <TextInput
                      maxLength={12}
                      placeholder={'รหัสลูกค้า (Account No.)'}
                      placeholderTextColor='#4F4F4F'
                      keyboardType={'numeric'}

                      value={this.state.BaID}
                      style={{
                        fontSize: wp(3.5), width: wp(75), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.5),
                        height: wp(10), backgroundColor: '#EEEEEE', borderRadius: 30, borderColor: "white", borderWidth: 0.5, marginTop: wp(1), marginBottom: wp(2),
                      }}
                      onChangeText={(text) => this.setState({ BaID: text })}
                    />

                  </View>

                  <View style={{ flexDirection: 'row', marginRight: wp(4), }} >
                    <TouchableOpacity
                      onPress={() => this.setState({
                        visiblePicker: false
                      })}
                      style={{ width: 35, height: 35, borderRadius: 35 / 2, marginLeft: wp(5), marginRight: wp(5) }}>
                      <Image
                        source={require('../../toteasylife/alliconESL/PayIcons/PAYIconsAW1copy-10.png')}
                        style={{ width: 48, height: 48, borderRadius: 48 / 2, }}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => this.addNotiba()}
                      style={{ width: 35, height: 35, borderRadius: 35 / 2, marginLeft: wp(5), marginRight: wp(5) }}>
                      <Image
                        source={require('../../toteasylife/alliconESL/PayIcons/PAYIconsAW1copy-11.png')}
                        style={{ width: 48, height: 48, borderRadius: 48 / 2, }}
                      />
                    </TouchableOpacity>
                  </View>


                </View>

              </View>
              :

              <TouchableOpacity onPress={() => this.setState({
                visiblePicker: true
              })} style={{ alignSelf: 'center', marginTop: wp(5), width: 35, height: 35, borderRadius: 35 / 2, marginBottom: wp(8) }}>
                <Image
                  source={require('../alliconESL/PayIcons/PAYIconsAW1copy_07.png')}
                  style={{ width: 35, height: 35, borderRadius: 35 / 2, }}
                />
              </TouchableOpacity>
          }




          {this.state.baidpayment.length != 0 ?
            <TouchableOpacity
              style={{ alignSelf: 'center', width: wp(50), height: wp(10), marginBottom: wp(12) }} onPress={() => this.getPayment()}>
              {/* <Image  source={require('./img/icon-design-09.png')} style={{height:wp(12), width: wp(12)}} /> */}
              <LinearGradient style={styles.login} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#3ad1ff', '#117ffe']}>
                <Text style={{ fontSize: wp(4), color: '#fff', fontFamily: 'Prompt-Light', fontWeight: '500' }}> ชำระค่าบริการ </Text>
              </LinearGradient>
            </TouchableOpacity>
            :
            null
          }

          {this.state.baNumber != "" ?
            null :
            <View>
              {this.state.visiblePicker ?
                null
                : <View>


                  <Text style={{ fontSize: wp(3.5), color: '#000', fontFamily: 'Prompt-Light', fontWeight: '300', marginTop: wp(5), alignSelf: 'center' }}>ไม่มีรหัสลูกค้า (Account No.)</Text>
                  <Text style={{ fontSize: wp(3.5), color: '#000', fontFamily: 'Prompt-Light', fontWeight: '300', marginTop: wp(2), alignSelf: 'center' }}>กรุณาเพิ่มรายการ</Text>
                  <TouchableOpacity onPress={() =>
                    this.setState({
                      isImageViewVisible: true
                    }

                    )}

                  >
                    <Image source={require('../img/example.jpg')}
                      style={{
                        height: wp(50), width: wp(90), resizeMode: 'contain', alignSelf: 'center',
                        shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14,
                      }} />



                    <ImageView
                      glideAlways
                      images={images}
                      imageIndex={0}
                      animationType="fade"
                      isVisible={this.state.isImageViewVisible}
                      renderFooter={this.renderFooter}
                      onClose={() => this.setState({ isImageViewVisible: false })}
                      onImageChange={index => {
                        console.log(index);
                      }}
                    />

                  </TouchableOpacity>




                </View>

              }
            </View>
          }




          {/* <Modal
            isVisible={this.state.visibleModal === 'bottom'}
             onSwipeComplete={() => this.setState({ visibleModal: null })}
            // swipeDirection={['left', 'right']}
            onSwipeComplete={this.close}
            swipeDirection={['down']}
            scrollTo={this.handleScrollTo}
            scrollOffset={this.state.scrollOffset}
            scrollOffsetMax={400 - 300}
            style={styles.bottomModal}
          >
            {this.renderModalDetailBa()}

          </Modal> */}

          <Modal
            isVisible={this.state.visibleModal}
            // onSwipeComplete={() => this.setState({ visibleModal: null })}
            // swipeDirection={['left', 'right']}
            onSwipeComplete={this.close}
            // swipeDirection={['down']}
            scrollTo={this.handleScrollTo}
            scrollOffset={this.state.scrollOffset}
            scrollOffsetMax={400 - 300}
            style={styles.bottomModal}
          >

            {this.renderModalDetailBa()}
          </Modal>



          {/* <Spinner

            visible={this.state.spinner}

            textContent={'กำลังโหลดข้อมูล...'}
            textStyle={styles.spinnerTextStyle}
          /> */}

          <Spinner
            color='#117ffe'
            visible={this.state.spinner}

            textContent={'กำลังโหลดข้อมูล...'}
            textStyle={styles.spinnerTextStyle}
          />
        </KeyboardAwareScrollView>






    )
  }
}



const styles = StyleSheet.create({
  headerHome: {
    borderBottomColor: '#FFDEAD',
    borderBottomWidth: wp('1.5'),
    borderColor: '#FFDEAD',
    justifyContent: 'center',
    marginLeft: wp('2'),
    alignItems: 'center',
    borderWidth: 2,
    borderRadius: 100,
    width: wp('8.6'),
    height: wp('8.6'),
    backgroundColor: '#0099FF'
  },

  lineStylew3: {
    borderWidth: 0.5,
    borderColor: '#DCDCDC',
    backgroundColor: '#9B9B9B',
    marginTop: wp(28.5),
    alignSelf: 'center',
    width: wp('95'),
  },
  lineStylew4: {
    borderWidth: 0.5,
    borderColor: '#DCDCDC',
    backgroundColor: '#9B9B9B',
    marginTop: 10,
    alignSelf: 'center',
    width: wp('40'),
    marginBottom: 10,
  },
  lineStylew5: {
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'black',
    alignSelf: 'center',
    width: wp('0.5'),
    marginRight: wp(8),
    marginLeft: wp(8),
    height: wp(15),
    marginTop: wp(1)
  },

  lineStylew6: {
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor: 'black',
    alignSelf: 'center',
    width: wp('95'),
  },


  linearGradient: {
    flexDirection: 'column',
    flex: 1,
    alignSelf: 'center',
    borderRadius: 5,
    height: 220,
    width: wp('100')
  },

  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#FFF',
    overflow: 'hidden',
  },

  pay1: {
    alignItems: 'center',
    // backgroundColor: '#03a9f4',
    //padding: 10,
    // backgroundColor: '#8A23FC',//add8e6 ,79b6d2
    height: wp(10),
    width: wp(50),
    // marginBottom: wp(2),
    //marginLeft:14,
    justifyContent: 'center',
    flexDirection: 'column',
    // borderWidth: 1,
    borderRadius: 20,
    // borderColor: '#fff'
  },

  pay2: {
    alignItems: 'center',
    // backgroundColor: '#03a9f4',
    //padding: 10,
    // backgroundColor: '#8A23FC',//add8e6 ,79b6d2
    height: wp(10),
    width: wp(50),
    // marginBottom: wp(2),
    //marginLeft:14,
    justifyContent: 'center',
    flexDirection: 'column',
    // borderWidth: 1,
    borderRadius: 20,
    // borderColor: '#fff'
  },
  login: {
    alignItems: 'center',
    // backgroundColor: '#03a9f4',
    //padding: 10,
    alignSelf: 'center',
    // backgroundColor: '#8A23FC',
    height: wp(10),
    width: wp(50),
    marginTop: wp(5),
    marginBottom: wp(5),
    //marginLeft:14,
    justifyContent: 'center',
    flexDirection: 'column',
    // borderWidth: 1,
    borderRadius: 30,
    // borderColor: '#8A23FC'
  },

  spinnerTextStyle: {
    color: '#000',
    fontFamily: 'Prompt-Light',
    fontWeight: '500'
  },
  tab: {
    backgroundColor: '#fff',
    width: '100%',
    height: wp(20),
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
    flex: 1,
    width: wp(100), height: wp(120),
  }

})
