


import React, { Component } from 'react';
import { Animated, Platform, FlatList, View, Text, Button, Alert, TouchableHighlight, StyleSheet, TextInput, ActivityIndicator, Image, ImageBackground, TouchableOpacity, WebView } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';
import {
    widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';

import { Icon, ThemeConsumer } from 'react-native-elements'

import { SearchBar } from 'react-native-elements';
import { ScrollView, TouchableWithoutFeedback } from 'react-native-gesture-handler';
// import Swiper from 'react-native-swiper';
import SwiperFlatList from 'react-native-swiper-flatlist';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import WeatherComponent from './header';
import ImageViewer from 'react-native-image-zoom-viewer';
import Share from 'react-native-share'
import ImageSelecter from 'react-native-image-picker';
import ReactNativeParallaxHeader from 'react-native-parallax-header';

export default class editProfile extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        imageSource: null,
    };


    static navigationOptions = ({ navigation }) => {
        // const { params = {} } = navigation.state;

        // const params = navigation.getParam('checklogin')
        // this.state.user
        // alert(params)
        return {
            title: 'Edit Profile',

            // headerLeft: (
            //   <TouchableHighlight style={styles.headerHome} >
            //     <Ionicons name='md-person' size={30} color='#FFDEAD' />
            //   </TouchableHighlight>
            // ),
            // headerRight: (
            //   <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>

            //   </HeaderButtons>
            // ),
            // //  headerTitle: <Logo/>,

            headerStyle: {
                backgroundColor: '#FFFAFA'
            },
            headerTintColor: '#000',
            headerTitleStyle: {
                // fontFamily: "Prompt-Light",
                textAlign: 'center',
                flex: 1,

            },
        }
    };


    SelectCameraRoll = () => {
        //   ImagePicker.openPicker({
        //     width: 300,
        //     height: 300,
        //     cropping: true,
        //     includeBase64: true,
        //     includeExif: true,
        // }).then(image => {
        //     console.log('received base64 image');
        //     this.setState({
        //         imageSource: {uri: 'data:image/jpeg;base64,'+image.data}
        //     });
        // })
        const options = {
            // mediaType:'photo',
            quality: 0.5,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };
        ImageSelecter.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                // alert('response image ....')
                // const source = { uri: data:${response.data.mime};base64, + response.data };
                // let source = { uri: response.uri };
                this.setState({
                    imageSource: {
                        uri: 'data:image/jpeg;base64,' + response.data,
                    }
                });
                //alert(this.state.imageSource.uri);
                //this.uploadPhoto(this.state.citizen);
            }
        });
    };


    render() {
        return (
            <ScrollView>
            <View>
                <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{
                        width: wp(90), height: wp(45), backgroundColor: '#FFFAFA', borderRadius: 10, justifyContent: 'center', alignSelf: 'center', flexDirection: 'column'
                        , shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, marginTop: wp(30)
                    }}>
                        <Text style={{ fontFamily: 'Prompt-Light', fontWeight: '500', fontSize: 20, alignSelf: 'center', marginTop: wp(10), color: '#1E90FF' }}>คุณ ธนพนธ์</Text>

                        <View style={{ flexDirection: 'row', alignSelf: 'center', marginTop: wp(-3) }}>
                            <View style={{ margin: wp(3), alignItems: 'center' }}>
                                <Image
                                    source={require('../ICON_Png/กรอบเท่ากัน/icondesign-13.png')}
                                    style={{ width: 40, height: 40, borderRadius: 40 / 2, marginRight: wp(2) }}
                                />
                                <Text style={{ fontFamily: 'Prompt-Light', fontSize: 11 }}>รหัสลูกค้า</Text>
                                <Text style={{ fontFamily: 'Prompt-Light', fontSize: 11 }}>59070503488</Text>
                            </View>
                            <View style={styles.lineStylew3} />
                            <View style={{ margin: wp(3), alignItems: 'center' }}>
                                <Image
                                    source={require('../ICON_Png/กรอบเท่ากัน/icondesign-12.png')}
                                    style={{ width: 40, height: 40, borderRadius: 40 / 2, marginRight: wp(2) }}
                                />
                                <Text style={{ fontFamily: 'Prompt-Light', fontSize: 11 }}>หมายเลชบริการ</Text>
                                <Text style={{ fontFamily: 'Prompt-Light', fontSize: 11 }}>123456789012</Text>
                            </View>
                            <View style={styles.lineStylew3} />
                            <View style={{ margin: wp(3), alignItems: 'center' }}>
                                <Image
                                    source={require('../ICON_Png/กรอบเท่ากัน/icondesign-13.png')}
                                    style={{ width: 40, height: 40, borderRadius: 40 / 2, marginRight: wp(2) }}
                                />
                                <Text style={{ fontFamily: 'Prompt-Light', fontSize: 11 }}>ยอดค้างชำระ</Text>
                                <Text style={{ fontFamily: 'Prompt-Light', fontSize: 11 }}>2200.25 บาท</Text>
                            </View>

                        </View>




                    </View>
                    <View style={{ marginTop: wp(-75) }} >
                        {
                            this.state.imageSource == null ?
                                <Image
                                    source={require('../pic/pexelsphoto-1845534.jpeg')}
                                    style={{
                                        width: 130, height: 130, borderRadius: 130 / 2, marginTop: wp(5),
                                        shadowOffset: { width: 5, height: 5, }, shadowOpacity: 0.1, shadowRadius: 5, elevation: 14, marginBottom: hp(2),
                                    }}
                                /> : <Image
                                    source={{ uri: this.state.imageSource.uri }}
                                    style={{
                                        width: 130, height: 130, borderRadius: 130 / 2, marginTop: wp(5),
                                        shadowOffset: { width: 5, height: 5, }, shadowOpacity: 0.1, shadowRadius: 5, elevation: 14, marginBottom: hp(2),
                                    }}
                                />
                        }


                        {/* <TouchableOpacity
                            onPress={() => this.SelectCameraRoll()}>
                            <Image
                                source={require('../ICON_Png/กรอบเท่ากัน/icondesign-11.png')}
                                style={{ width: 40, height: 40, borderRadius: 40 / 2, marginTop: wp(-13), marginLeft: wp(25) }}
                            />
                        </TouchableOpacity> */}



                    </View>
                </View>

                {/* <View style={{
                width: wp(90), height: wp(10), backgroundColor: '#F5F5F5', borderTopLeftRadius: 10, borderTopRightRadius: 10, justifyContent: 'center', alignSelf: 'center', flexDirection: 'column'
                , shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, borderTopColor: '#DCDCDC', marginTop: wp(38)
            }}>

                <Text style={{ fontFamily: 'Prompt-Light', fontSize: 16 ,fontWeight:400, marginTop: wp(1.7), alignSelf:'center' }}>ข้อมูลส่วนตัว</Text>
            </View> */}
                <View style={{ shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, }}>
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#00BFFF', '#1E90FF', '#1C86EE']} style={styles.linearGradient}>
                        <Text style={styles.buttonText}>
                            ข้อมูลส่วนตัว
                        </Text>
                    </LinearGradient>
                </View>



            </View>


            <View style={{
                width: wp(90), height: wp(50), backgroundColor: '#FFFAFA', borderBottomRightRadius: 10, borderBottomLeftRadius: 10, justifyContent: 'center', alignSelf: 'center', flexDirection: 'column'
                , shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, marginTop: wp(0)
            }}>
                <View style={{ marginTop: wp(-1), marginLeft: wp(8), flexDirection: 'row' }}>
                    <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#1C86EE', marginTop: wp(1.5), marginRight: wp(2) }} />
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, color: '#1C86EE', fontWeight: '500' }}>ชื่อ-นามสกุล:</Text>
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, marginLeft: wp(1) }} > คุณธนพนธ์</Text>

                </View>
                <View style={styles.lineStylew5} />

                <View style={{ marginLeft: wp(8), flexDirection: 'row', marginTop: wp(3) }}>
                    <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#1C86EE', marginTop: wp(1.5), marginRight: wp(2) }} />
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, color: '#1C86EE', fontWeight: '500' }}>โทรศัพท์:</Text>
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, marginLeft: wp(6) }} > 0870733380</Text>

                </View>
                <View style={styles.lineStylew5} />

                <View style={{ marginLeft: wp(8), flexDirection: 'row', marginTop: wp(3) }}>
                    <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#1C86EE', marginTop: wp(1.5), marginRight: wp(2) }} />
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, color: '#1C86EE', fontWeight: '500' }}>อีเมล:</Text>
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, marginLeft: wp(8) }} > tontact.21@hotmail.com</Text>

                </View>
                <View style={styles.lineStylew5} />

                <View style={{ marginLeft: wp(8), flexDirection: 'row', marginTop: wp(3) }}>
                    <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#1C86EE', marginTop: wp(1.5), marginRight: wp(2) }} />
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, color: '#1C86EE', fontWeight: '500' }}>ที่อยู่:</Text>
                    {/* <Text numberOfLines={3} style={{ fontFamily: 'Prompt-Light', width:wp(68), fontSize:12}} > 22 ประชาอุทิศ 75 แยก 5 ถนนประชาอุทิศ แขวงทุ่งครุ เขตทุ่งครุ กรุงเทพมหานคร 10140</Text> */}
                    <Text numberOfLines={2} style={{ fontFamily: 'Prompt-Light', width: wp(60), fontSize: 12, marginLeft: wp(12), }}>4/135 ถ.เจริญประดิษฐ์ ต.รูสมิแล อ.เมือง จ.ปัตตานี 94000</Text>
                </View>




            </View>

            <View style={{ shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, }}>
                <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#00BFFF', '#1E90FF', '#1C86EE']} style={styles.linearGradient1}>
                    <Text style={styles.buttonText}>
                        รอบบิลของคุณ / แพ็คเก็จ
                        </Text>
                </LinearGradient>
            </View>


            <View style={{
                width: wp(90), height: wp(30), backgroundColor: '#FFFAFA', borderBottomRightRadius: 10, borderBottomLeftRadius: 10, justifyContent: 'center', alignSelf: 'center', flexDirection: 'column'
                , shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, marginTop: wp(0)
            }}>
                <View style={{ marginTop: wp(0), marginLeft: wp(8), flexDirection: 'row' }}>
                    <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#1C86EE', marginTop: wp(1.5), marginRight: wp(2) }} />
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, color: '#1C86EE', fontWeight: '500' }}>รอบบิลของคุณตั้งแต่:</Text>
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, marginLeft: wp(1) }}> 11/08/2019 - 11/09/2019</Text>

                </View>


                <View style={{ marginLeft: wp(8), flexDirection: 'column', marginTop: wp(2) }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#1C86EE', marginTop: wp(1.5), marginRight: wp(2) }} />
                        <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, color: '#1C86EE', fontWeight: '500' }}>แพ็คเก็จปัจจุบัน:</Text>
                    </View>
                    <Text numberOfLines={3} style={{ fontFamily: 'Prompt-Light', fontSize: 12, width: wp(80) }} >
                        TOT 3G แพ็คเก็จ 899 บาทต่อเดือน โทรทุกเครือข่าย 500 นาที่ เน็ต 3G ความเร็วสูงสุดต่อเนื่อง 300 / 300 Mbps ได้ไม่จำกัด
                     </Text>

                </View>





            </View>




        </ScrollView>





        );
    }
}
const styles = StyleSheet.create({
    lineStylew3: {
        borderWidth: 1,
        borderColor: '#1E90FF',
        //backgroundColor: '#8A23FC',
        marginTop: 8,
        alignSelf: 'center',
        height: wp(18),
        marginLeft: wp(3),
        marginRight: wp(3)

    },
    linearGradient: {
        flex: 1,
        width: wp(90),
        height: wp(12),
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        marginTop: wp(38),
        alignSelf: 'center',

    },
    linearGradient1: {
        flex: 1,
        width: wp(90),
        height: wp(12),
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        marginTop: wp(7),
        alignSelf: 'center',

    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Prompt-Light',
        textAlign: 'center',
        margin: 10,
        fontWeight: '500',
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    lineStylew5: {
        borderWidth: 0.5,
        borderColor: '#DCDCDC',
        backgroundColor: '#9B9B9B',
        marginTop: 10,
        alignSelf: 'center',
        width: wp(75),
        
    
      },
})
