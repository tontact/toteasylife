import React, { Component } from 'react';
import { Animated, Platform, FlatList, View, Text, Button, Alert, TouchableHighlight, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';
import {
  widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';

import { ThemeConsumer } from 'react-native-elements'

import { SearchBar } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
// import Swiper from 'react-native-swiper';
import SwiperFlatList from 'react-native-swiper-flatlist';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import { FluidNavigator, Transition } from 'react-navigation-fluid-transitions';
import { Header } from 'react-navigation';
import Icon from "react-native-vector-icons/Ionicons";

const IoniconsHeaderButton = passMeFurther => (
  <HeaderButton {...passMeFurther} IconComponent={Ionicons} iconSize={28} color="white" />
);


const HEADER_MAX_HEIGHT = 300;
const HEADER_MIN_HEIGHT = wp(25);
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;


let screenWidth = Dimensions.get('window').width;

// const {
//   height: SCREEN_HEIGHT,
// } = Dimensions.get('window');

// const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
// const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
// const NAV_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 64;

// const SCROLL_EVENT_THROTTLE = 16;
// const DEFAULT_HEADER_MAX_HEIGHT = 170;
// const DEFAULT_HEADER_MIN_HEIGHT = NAV_BAR_HEIGHT;
// const DEFAULT_EXTRA_SCROLL_HEIGHT = 30;
// const DEFAULT_BACKGROUND_IMAGE_SCALE = 1.5;




export default class allpromotion extends Component {
  constructor(props) {
    super(props);
    this.index = 0;
    this.index1 = 0;
    this.index2 = 0;
  }

  state = {
    search: '',
    pickerDisplayed: false,
    test: null,
    scrollY: new Animated.Value(0),


    data4: [
      {
        name: 'TOT fiber 2U ',
        detail: 'อินเทอร์เน็ตไฟเบอร์ออพติกแท้ 100% โปรโมชั่น Net Revo(ทุกระดับความเร็ว 50,100,150 และ 200 Mbps) ฟรี! ค่าติดตั้ง ค่าธรรมเนียมแรกเข้าหรือเปลี่ยนแพ็คเกจ และค่าบำรุงรักษารายเดือน ทั้งยังได้รับสิทธิ์ซื้ออุปกรณ์เสริมราคาพิเศษ *เงื่อนไขเป็นไปตามที่บริษัทกำหนด',
        path: 'https://www.tot.co.th/images/default-source/default-album/promotion/2018/05/tot-fiber2u_net-revo/aw_net-revo_final_website-01.png?sfvrsn=9f53f72d_2'
      },


      {
        name: 'Voice Topping" พูดไม่มียั้ง',
        detail: 'โปรโมชั่น Voice Topping พูดไม่มียั้ง สำหรับผู้ใช้บริการโทรศัพท์เคลื่อนที่ TOT Mobile ระบบเติมเงิน สามารถสมัครรายการส่งเสริมการขายนี้ได้ผ่าน บริการด่วนพิเศษ กด USSD บริการผ่านเว็บไซต์ และบริการผ่าน Application TOT Mobile Easy Mobile',
        path: 'https://www.tot.co.th/images/default-source/default-album/promotion/2018/05/tot3g_voicetopping/terser.jpg?sfvrsn=6305b456_2'
      },
      {
        name: 'TOT 008',
        detail: 'วันนี้ลูกค้าสามารถโทรศัพท์ทางไกลระหว่างประเทศ ประหยัดได้มากกว่า เมื่อกด 008 (คมชัดประหยัดจริง) พิเศษ เริ่มต้นที่ 2 บาท/นาที ผ่านเครือข่ายโทรศัพท์ ทีโอที ได้ตั้งเเต่วันที่ 1 กันยายน 2561 – 31 สิงหาคม 2562',
        path: 'https://www.tot.co.th/images/default-source/default-album/promotion/2018/05/tot-008/008-share.png?sfvrsn=34a16243_2'
      },
    ],


  };

  updateSearch = search => {
    this.setState({ search });
  };


  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    // const params = navigation.getParam('checklogin')
    // this.state.user
    // alert(params)


    return {

      headerTransitionPreset: 'fade-in-place',
      // title: 'ชำระค่าบริการ',
      headerTitle: (<Text style={{ fontFamily: "Prompt-SemiBold", color: "#000", textAlign: "center", flex: 1, fontSize: 20 }}>โปรโมชั่น</Text>),

      headerRight: (
        <TouchableOpacity style={{ marginRight: wp(2.5) }} >
          <Icon name='ios-menu' size={30} color='#000' />
        </TouchableOpacity>
      ),
      headerLeft: (
        <TouchableOpacity style={{ marginLeft: wp(3), }} >
          <Ionicons name='ios-arrow-back' size={30} onPress={() => navigation.goBack()} />
        </TouchableOpacity>
      ),
      // //  headerTitle: <Logo/>,

      headerStyle: {
        backgroundColor: '#FFFAFA'
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        fontFamily: "Prompt-Light",
        textAlign: 'center',
        flex: 1,
        fontSize: 20,
      },
    }
  };
  //   test(){
  //       Alert.alert('in')
  //   }



  // renderItem(item) {
  //   return (

  //   );
  // }


  isLegitIndex(index, length) {
    if (index < 0 || index >= length) return false;
    return true;
  }

  isLegitIndex1(index, length) {
    if (index < 0 || index >= length) return false;
    return true;
  }

  isLegitIndex2(index, length) {
    if (index < 0 || index >= length) return false;
    return true;
  }


  pagination = (velocity) => {
    let nextIndex;
    if (Platform.OS == "ios")
      nextIndex = velocity > 0 ? this.index + 1 : this.index - 1;
    else
      nextIndex = velocity < 0 ? this.index + 1 : this.index - 1;
    if (this.isLegitIndex(nextIndex, this.state.data2.length)) {
      this.index = nextIndex;
    }
    this.flatlist.scrollToIndex({ index: this.index, animated: true });
  }

  pagination1 = (velocity) => {
    let nextIndex;
    if (Platform.OS == "ios")
      nextIndex = velocity > 0 ? this.index1 + 1 : this.index1 - 1;
    else
      nextIndex = velocity < 0 ? this.index1 + 1 : this.index1 - 1;
    if (this.isLegitIndex1(nextIndex, this.state.data3.length)) {
      this.index1 = nextIndex;
    }
    this.flatlist1.scrollToIndex({ index: this.index1, animated: true });
  }

  pagination2 = (velocity) => {
    let nextIndex;
    if (Platform.OS == "ios")
      nextIndex = velocity > 0 ? this.index2 + 1 : this.index2 - 1;
    else
      nextIndex = velocity < 0 ? this.index2 + 1 : this.index2 - 1;
    if (this.isLegitIndex1(nextIndex, this.state.data4.length)) {
      this.index2 = nextIndex;
    }
    this.flatlist2.scrollToIndex({ index: this.index2, animated: true });
  }



  modaldetail = () => {
    const imageOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: 'extend',
    });
    const imageTranslate = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -50],
      extrapolate: 'clamp',
    });
    //let data = [1, 2, 3]
    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: 'clamp',
    });

    return (
      <LinearGradient colors={['#FFFAFA', '#FFFAF0']} style={styles.linearGradient}>
        <View style={{ flex: 1, flexDirection: 'column', }}>
          <ScrollView style={{ flex: 1, }}
            style={styles.fill}
            scrollEventThrottle={16}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }]
            )}>


            <Text style={{ marginTop: wp(80), alignItems: 'stretch', marginLeft: wp(3), fontSize: 25, fontFamily: 'Prompt-Regular', fontWeight: '500' }}>
              {this.state.test.name}
            </Text>
            <View style={{ alignItems: 'center' }}>
              <Text style={{ marginTop: wp(5), marginLeft: wp(3), marginRight: wp(3), width: wp(95), textAlign: 'justify', fontFamily: 'Prompt-Regular' }}>

                {'    ' + this.state.test.detail}
              </Text>
            </View>
          </ScrollView>




          <Animated.View style={[styles.header, { height: headerHeight }]}>
            <Animated.View>
              <View style={styles.bar}>
                <Text style={styles.title}  ></Text>
              </View>
            </Animated.View>

            <Animated.Image
              style={[styles.backgroundImage, { opacity: imageOpacity, transform: [{ translateY: imageTranslate }] },]}
              source={{ uri: this.state.test.path }} />
            {/* <Text style={{ justifyContent: 'flex-start', alignItems: 'baseline', marginLeft: wp(3), fontWeight: '900', fontSize: 20 }} onPress={() => { this.setState({ pickerDisplayed: false, test: null }) }} >X</Text> */}

            <Icon
              containerStyle={{ justifyContent: 'flex-start', alignItems: 'baseline', marginLeft: wp(3) }}
              name='close'
              type='font-awesome'


              onPress={() => { this.setState({ pickerDisplayed: false, test: null }) }} />

          </Animated.View>



        </View>
      </LinearGradient>
    )
  }












  //  <Text  style={{marginTop:wp(5),alignItems: 'stretch', marginLeft:wp(3), marginRight:wp(3) , width:wp(95), }}>

  render() {



    const sizeIcon = wp('5%');
    return (
      // <LinearGradient colors={['#FFFAFA', '#FFFAF0']} style={styles.linearGradient}>
      <ScrollView style={{ flex: 1 }}>

        <View style={{ flex: 1 }}>





          {/* <View style={styles.lineStylew3} /> */}

          <LinearGradient start={{ x: 0, y: 1.2 }} end={{ x: 1, y: 0 }} colors={['#FF33CC', '#FFCC44',]} style={styles.linearGradient}>



            <View style={styles.detailTopBottomSubContainer}>
              {/* <Text style={{
                color: 'white',
                fontWeight: '500',
                fontFamily: 'Prompt-Light',
                fontSize: scaleToDimension(35)
              }}>ALL - {"\n"}PROMOTION</Text> */}

              <Text style={{ color: 'white', fontSize: scaleToDimension(35), fontWeight: '500', fontFamily: 'Prompt-Light', }}>ALL -</Text>
              <Text style={{ color: 'white', fontSize: scaleToDimension(35), fontWeight: '500', fontFamily: 'Prompt-Light',marginTop:wp(-5) }}>PROMOTION</Text>
              <Text style={{ color: 'white', fontSize: scaleToDimension(15), fontWeight: '300', fontFamily: 'Prompt-Light',marginTop:wp(-3) }}>Get Great Deals !</Text>
              <Text style={{ color: 'white', fontSize: scaleToDimension(18), fontWeight: '500', fontFamily: 'Prompt-Light',marginTop:wp(0) }}>โปรโมชั่นทั้งหมด</Text>
            </View>



          </LinearGradient>




          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            {/* <Text style={{ marginLeft: wp(2), flexDirection: 'row', alignSelf: 'flex-end', fontWeight: 'bold', fontSize: 25 }} >Daily News</Text> */}

            {/* <View style={{ flexDirection: 'row', marginRight: wp(1), marginTop: 10 }} >
                <Text style={{ fontWeight: 'bold', color: 'black', marginTop: 3 }} onPress={() => this.props.navigation.navigate('seeallnewspage')} > See all </Text>

                <Icon
                  containerStyle={{ justifyContent: 'flex-start', alignItems: 'baseline', }}
                  name='navigate-next'
                  type='MaterialIcons'
                  onPress={() => console.log('1st')}
                />


             
              </View> */}
          </View>



          <View style={{ paddingTop: hp(1), paddingBottom: hp(2), flexDirection: 'column', justifyContent: 'center', alignItems: 'center', flex: 1, }}>
            <FlatList
              horizontal
              data={this.state.data4}
              ref={ref => (this.flatlist2 = ref)}
              onScrollEndDrag={e => {
                this.pagination2(e.nativeEvent.velocity.x);
              }}
              renderItem={({ item }) => {
                return (

                  <TouchableHighlight
                    underlayColor={false}
                    onPress={() => {
                      this.setState({
                        test: { name: item.name, detail: item.detail, path: item.path },
                        pickerDisplayed: true
                      })
                    }} style={{
                      flex: 1, backgroundColor: '#FFFAFA', margin: 9, borderRadius: 15, height: hp(43.5), shadowColor: "#000",
                      shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.3, shadowRadius: 4.65, elevation: 15,
                    }}>
                    <View >
                      <View style={{ width: wp(75), alignSelf: 'center', borderTopRightRadius: 15, borderTopLeftRadius: 15, overflow: 'hidden', }}>
                        <Image source={{ uri: item.path }} style={{ width: wp(75), height: hp(30), }} />
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        {/* <Image source={require('../img/2548.png')} style={{ width: 40, height: 40, borderRadius: (40 / 2), margin: 5 }} /> */}

                        <View style={{ flex: 1, flexDirection: 'column', margin: 8, marginTop: 10 }}>
                          <Text numberOfLines={1} style={{ color: "#000000EE", width: wp('60'), fontWeight: 'bold' }}>{item.name}</Text>
                          <View style={{ flexDirection: 'column', marginTop: 10 }}>
                            <Text numberOfLines={1} style={{ color: "#000000AA", width: wp('60'), }}>{item.detail}</Text>
                          </View>
                          <View style={styles.lineStylew4} />

                          <Text style={{ justifyContent: 'center', alignSelf: 'center', fontWeight: 'bold', color: '#1E90FF' }}>View detail</Text>
                        </View>
                      </View>

                    </View>
                  </TouchableHighlight>
                )
              }}
            />
          </View>

          <View style={styles.lineStylew3} />
          {this.state.test != null ?
            <Modal

              animationType={"slide"}
              isVisible={this.state.pickerDisplayed}
              onSwipeComplete={() => this.setState({ pickerDisplayed: false })}
              // swipeDirection={['down']}
              style={styles.bottomModal}
              onBackdropPress={() => this.setState({ pickerDisplayed: false })}

            >

              {
                this.modaldetail()
              }

            </Modal> : null
          }



        </View>




      </ScrollView>
      // </LinearGradient>
    );
  }
}

const scaleToDimension = (size) => {
  return screenWidth * size / 375
};

const styles = StyleSheet.create({

  iconstyle4: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconstyle1: {
    justifyContent: 'center', alignItems: 'center', margin: 1
  },
  lineStylew3: {
    borderWidth: 3,
    borderColor: '#DCDCDC',
    backgroundColor: '#9B9B9B',
    marginTop: 5,
    alignSelf: 'center',
    width: wp('100'),
  },
  lineStylew4: {
    borderWidth: 0.5,
    borderColor: '#DCDCDC',
    backgroundColor: '#9B9B9B',
    marginTop: 10,
    alignSelf: 'center',
    width: wp('70'),
    marginBottom: 10,
  },
  lineStylew5: {
    borderWidth: 0.5,
    borderColor: '#DCDCDC',
    backgroundColor: '#9B9B9B',
    marginTop: 20,
    alignSelf: 'center',
    width: wp('85'),
    marginBottom: 20,
  },
  iconView: {
    flexDirection: 'column',
    flex: 1,
    // backgroundColor: '#87CEFA', 
    height: hp('220'), width: wp('95'),
    borderRadius: 5,
    alignSelf: 'center',
    paddingTop: 15
  },

  linearGradient: {
    flexDirection: 'column',
    flex: 1,
    alignSelf: 'center',

    height: 220,
    width: wp('100')
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },

  buttonSeeall: {
    alignItems: 'flex-end',
    // marginTop: 150,
    backgroundColor: '#00BFFF',
    color: '#fff',
    padding: 5,
    marginTop: 20,
    marginRight: 10,
    borderRadius: 5,
    justifyContent: 'flex-end',
    flexDirection: 'row'

  },

  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },

  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },
  fill: {
    flex: 1,
  },
  bar: {
    marginTop: 20,
    height: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    backgroundColor: 'transparent',
    color: 'red',
    fontSize: 18,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#3399FF',
    overflow: 'hidden',
  },
  detailTopContainer: {
    height: scaleToDimension(250),
    width: screenWidth,
    backgroundColor: 'transparent',

  },
  navigationHeaderContainer: {
    height: Header.HEIGHT,
    width: screenWidth,
    color: "blue",
    justifyContent: 'center'
  },

  detailTopBottomSubContainer: {
    width: screenWidth - 30,
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: wp(20),
    left: wp(7),
    right: wp(5),

  },

})










