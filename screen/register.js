import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Button, ScrollView, Alert, AsyncStorage, SafeAreaView, ImageBackground } from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as loc,
    removeOrientationListener as rol
} from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import base64 from 'react-native-base64';
import DeviceInfo from 'react-native-device-info';
import DatePicker from 'react-native-datepicker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Modal from "react-native-modal";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import axios from 'axios';
import RNRestart from "react-native-restart";
import Ionicons from 'react-native-vector-icons/Ionicons';

// import { SafeAreaView } from 'react-navigation';
class register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Firstname: '',
            Lastname: '',
            Nationalid: '',
            Mobileno: '',
            Email: '',
            Password: '',
            Faceid: '',
            Birthdate: '',
            IpAddress: '',
            mCode: '',
            mConfirmAppCode: '',
            Visible: true,
            isModalVisible: false,
            Randomotp: "",
            OTPno: '',

            data: [
                {
                    name: 'นาย',
                    select: false
                },
                {
                    name: 'นาง',
                    select: false
                },
                {
                    name: 'นางสาว',
                    select: false
                },
            ],
            prefix : '',

        };
    }


    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };
    componentDidMount() {
        DeviceInfo.getIpAddress().then(ip => {
            console.log("IpAddress:" + ip);
            this.setState({ Ipaddress: ip })
        });
    }

    _Register = () => {

        if (this.state.prefix.length == '') {
            Alert.alert('กรุณาเลือกคำนำหน้าชื่อ',
                "",
                [
                    { text: 'ตกลง', cancelable: false },
                ])
        }
        else if (this.state.Firstname.length == '') {
            Alert.alert('กรุณากรอกชื่อจริงของท่าน',
                "",
                [
                    { text: 'ตกลง', cancelable: false },
                ])
        } else if (this.state.Lastname.length == '') {
            Alert.alert('กรุณากรอกนามสกุลจริงของท่าน',
                "",
                [
                    { text: 'ตกลง', cancelable: false },
                ])
        } else if (this.state.Nationalid.length == '') {
            Alert.alert('กรุณากรอกบัตรประชาชน',
                "",
                [
                    { text: 'ตกลง', cancelable: false },
                ])
        } else if (this.state.Nationalid.length < 13) {
            Alert.alert('กรุณากรอกบัตรประชาชนให้ครบ 13 หลัก',
                "",
                [
                    { text: 'ตกลง', cancelable: false },
                ])
        } else if (this.state.Mobileno.length == '') {
            Alert.alert('กรุณากรอกเบอร์โทร',
                "",
                [
                    { text: 'ตกลง', cancelable: false },
                ])
        } else if (this.state.Mobileno.length < 10) {
            Alert.alert('กรุณากรอกเบอร์โทรให้ครบ 10 หลัก',
                "",
                [
                    { text: 'ตกลง', cancelable: false },
                ])
        } else {
            const url = "http://203.113.11.167/api/addusers";
            // var Nationalid64 = base64.encode(this.state.Nationalid);
            // var Email64 = base64.encode(this.state.Email);
            // var Passwordbase64 = base64.encode(this.state.Password);
            // var Faceid64 = base64.encode(this.state.Faceid);
            const headers = {
                'Content-Type': 'application/json',
                'Authorization': 'JWT fefege...'
            }
            axios.post(url, {
                // ipaddress: this.state.Ipaddress,
                // auth_contracts: "easy2ife2ew",
                // auth_code: "6sd4f6@6cv4",
                // lang: "TH",
                prefix:this.state.prefix,
                name: this.state.Firstname,
                lastname: this.state.Lastname,
                Idcard: this.state.Nationalid,
                // nationaltype: "1",
                tel: this.state.Mobileno,
                // email: base64.encode(Email64),
                // password: base64.encode(Passwordbase64),
                // faceid: base64.encode(Faceid64),
                // birthdate: this.state.Birthdate
            },
                {
                    headers: headers
                }
            ).then(result => {
                // console.log(result);

                if (result.status == 200) {
                    // alert(JSON.stringify(result.data.statuscode))
                    // this.setState({ mCode: result.data.result.mCode })
                    // this.setState({ mConfirmAppCode: result.data.result.mConfirmAppCode })
                    if (result.data.message == 'มีข้อมูลข้อมูลอยู่แล้ว') {
                        Alert.alert(
                            "สมัครสมาชิกไม่สำเร็จ",
                            "เลขบัตรหรือเบอร์โทรศัพท์มือถือนี้มีผู้ใช้อยู่ในระบบแล้ว",
                            [
                                { text: 'ตกลง' },
                            ],
                            { cancelable: false },
                        );
                        // console.log(result.data.data.IDcard);
                    } else {

                        var userID = JSON.stringify(result.data.id);
                        // console.log('B'+userID);
        
                        AsyncStorage.setItem('userid', (userID))
                        Alert.alert(
                            "สมัครสมาชิกสำเร็จ",
                            "",
                            [
                                { text: 'ตกลง', onPress: () => (
                                  
                                    RNRestart.Restart()) },
                            ],
                            { cancelable: false },
                        );
                    }
                    // Alert.alert(
                    //     "สมัครสมาชิคสำเร็จ",
                    //     "",
                    //     [
                    //         { text: 'ตกลง', onPress: () => (RNRestart.Restart()) },
                    //     ],
                    //     { cancelable: false },
                    // );
                    // console.log(result.data.data.IDcard);

                    // this.props.navigation.navigate('Home')
                }
                // else {
                //     // var str = JSON.stringify(result.data.errors[0].msg._th)
                //     Alert.alert(
                //         "สมัครไม่สำเร็จ",
                //         "",
                //         [
                //             { text: 'ตกลง' },
                //         ],
                //         { cancelable: false },
                //     );
                // }
            }).catch(e => {
                console.log(e);

            });
        }
    }
    Randomotp = async () => {
        function makeid(length) {
            var result = '';
            var characters = '0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }
        await this.setState({ Randomotp: (makeid(6)) })
        await this.OtpSms()
    }
    NewRandomotp = async () => {
        function makeid(length) {
            var result = '';
            var characters = '0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }
        await this.setState({ Randomotp: (makeid(6)) })
        await this.NewOtpSms()
    }

    OtpSms = () => {
        const url = "http://203.113.11.167:3000/smpp"
        axios.post(url, {
            phone: this.state.Mobileno,
            // phone: "0826578133",
            message: "รหัสยืนยันของคุณคือ : " + this.state.Randomotp,
            from: "easylife"
        })
            .then(result => {
                this.toggleModal()
            })
            .catch(e => {
                console.log(e);

            });
    }
    NewOtpSms = () => {
        const url = "http://203.113.11.167:3000/smpp"
        axios.post(url, {
            phone: this.state.Mobileno,
            // phone: "0826578133",
            message: "รหัสยืนยันของคุณคือ : " + this.state.Randomotp,
            from: "easylife"
        })
            .then(result => {
                Alert.alert("กรุณารอสักครู่")
            })
            .catch(e => {
                console.log(e);

            });
    }

    CheckOtp = () => {
        if (this.state.OTPno == this.state.Randomotp) {
            this._ActivateMember();
            // alert("ถูกต้อง")
        }
        else {
            Alert.alert("OTPไม่ถูกต้อง")
        }
    }


    success = async () => {
        await this.toggleModal();
        AsyncStorage.clear().then(RNRestart.Restart())
    }


TestSelect = (index) => {
      

        // alert(JSON.stringify(this.state.prefix))
        for(var i = 0 ; i<this.state.data.length; i++){
            if(index == i){
                this.state.data[i].select = true
                this.setState({prefix : index == 0 ? 'นาย' : index == 1 ? 'นาง' : 'นางสาว'})
            }else{
                this.state.data[i].select = false
            }
        }
        // alert(JSON.stringify(index))
        // let a = this.state.data.slice(); //creates the clone of the state
        // // alert(JSON.stringify(a))
        // if (a[index].select == false) {
        //     a[index].select = true;
        //     this.setState({ data: a });
        // } else {
        //     a[index].select = false;
        //     this.setState({ data: a });
        // }

        // alert(JSON.stringify(this.state.data))
    }



    // TestSelect = (index) => {

    //     let a = this.state.data.slice(); //creates the clone of the state
    //     if (a[index].select == false) {
    //         a[index].select = true;
    //         this.setState({ data: a });
    //     } else {
    //         a[index].select = false;
    //         this.setState({ data: a });
    //     }

    //     // alert(JSON.stringify(this.state.data))
    // }

    render() {
        const { goBack } = this.props.navigation;
        return (

            <KeyboardAwareScrollView>

                <View>

                    <ImageBackground style={[styles.bnimage]} resizeMode='cover' source={require('../alliconESL/Iconnewsetapp/HomeNewTOTeasylifeAW118.png')} >

                        <TouchableOpacity style={{ marginLeft: wp('4%'), marginTop: wp('10%') }} onPress={() => goBack()} >
                            <Ionicons name='ios-arrow-back' size={30} />
                        </TouchableOpacity>
                        <Text style={{ color: 'white', alignSelf: 'center', justifyContent: 'center', marginTop: wp('29%'), fontFamily: 'Prompt-SemiBold', fontSize: wp('7%') }}>ลงทะเบียนเพื่อเข้าสู่ระบบ</Text>
                        <Text style={{ color: 'white', alignSelf: 'center', fontFamily: 'Prompt-SemiBold', fontSize: 11 }} >TOT PUBLIC COMPANY LIMITED</Text>
                        <Text style={{ color: 'white', alignSelf: 'center', fontFamily: 'Prompt-SemiBold', fontSize: 11 }} >@TOTPUBLIC</Text>



                    </ImageBackground>

                    <View style={{
                        flex: 1,
                        width: wp('80%'), height: wp('111%'), backgroundColor: '#FFFFFF',
                        borderRadius: 10, justifyContent: 'center', alignSelf: 'center', flexDirection: 'column', marginBottom: wp('8%'),
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 2,
                        },
                        shadowOpacity: 0.25,
                        shadowRadius: 3.84,
                        marginTop: wp("-37%"),
                        elevation: 10,

                    }}>

                        <View style={{ alignItems: 'center' }}>
                            {/* <Button onPress={() => this.Randomotp()} title="test" /> */}
                            <Text style={{ justifyContent: 'center', alignSelf: 'center', fontFamily: 'Prompt-SemiBold', fontSize: wp('5%'), color: '#117ffe', marginTop: wp('5%') }}>ลงทะเบียนเพื่อเข้าสู่ระบบ</Text>
                            <View style={styles.lineStylew4} />

                            <View style={{ flexDirection: 'row', }} >
                                {this.state.data.map((item, index) => {
                                    return (
                                        <View>
                                            <TouchableOpacity
                                                onPress={() => this.TestSelect(index)}
                                                style={[styles.customerService, { borderColor: item.select == true ? '#3d90ff' : '#b6bcc0' }]}
                                            >
                                                <Text style={{ fontSize: wp(3.5), fontFamily: 'prompt-light', color: item.select == true ? '#3d90ff' : '#b6bcc0' }}> {item.name} </Text>
                                            </TouchableOpacity>
                                        </View>
                                    )
                                })}

                            </View>

                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
                                style={{ alignItems: 'center', marginTop: wp('2%'), height: wp('12%'), width: wp('65%'), borderRadius: 30 }}>

                                <TextInput
                                    // maxLength={13}
                                    placeholder={'ชื่อจริง'}
                                    placeholderTextColor='grey'
                                    keyboardType='default'
                                    //paddingLeft= {20}
                                    style={{
                                        fontSize: wp('3.5%'), width: wp('64%'), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp('0.5%'),
                                        height: wp('11%'), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                    }}
                                    onChangeText={(text) => this.setState({ Firstname: text })}
                                />
                            </LinearGradient>
                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
                                style={{ alignItems: 'center', marginTop: wp('2%'), height: wp('12%'), width: wp('65%'), borderRadius: 30 }}>

                                <TextInput
                                    // maxLength={13}
                                    placeholder={'นามสกุล'}
                                    placeholderTextColor='grey'
                                    // keyboardType='default'
                                    //paddingLeft= {20}
                                    style={{
                                        fontSize: wp('3.5%'), width: wp('64%'), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp('0.5%'),
                                        height: wp('11%'), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                    }}
                                    onChangeText={(text) => this.setState({ Lastname: text })}
                                />
                            </LinearGradient>
                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
                                style={{ alignItems: 'center', marginTop: wp('2%'), height: wp('12%'), width: wp('65%'), borderRadius: 30 }}>
                                <TextInput
                                    maxLength={13}
                                    placeholder={'บัตรประชาชน'}
                                    placeholderTextColor='grey'
                                    keyboardType='number-pad'
                                    //paddingLeft= {20}
                                    style={{
                                        fontSize: wp('3.5%'), width: wp('64%'), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp('0.5%'),
                                        height: wp('11%'), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                    }}
                                    onChangeText={(text) => this.setState({ Nationalid: text })}
                                />
                            </LinearGradient>
                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
                                style={{ alignItems: 'center', marginTop: wp('2%'), height: wp('12%'), width: wp('65%'), borderRadius: 30 }}>

                                <TextInput
                                    maxLength={10}
                                    placeholder={'เบอร์โทรศัพท์มือถือ'}
                                    placeholderTextColor='grey'
                                    keyboardType='number-pad'
                                    //paddingLeft= {20}
                                    style={{
                                        fontSize: wp('3.5%'), width: wp('64%'), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp('0.5%'),
                                        height: wp('11%'), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                    }}
                                    onChangeText={(text) => this.setState({ Mobileno: text })}
                                />
                            </LinearGradient>
                            {/* <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}
                                style={{ alignItems: 'center', marginTop: wp('2%'), height: wp('12%'), width: wp('65%'), borderRadius: 30 }}>

                                <TextInput
                                    placeholder={'อีเมล'}
                                    keyboardType='email-address'
                                    //paddingLeft= {20}
                                    style={{
                                        fontSize: wp('3.5%'), width: wp('64%'), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp('0.5%'),
                                        height: wp('11%'), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                    }}
                                    onChangeText={(text) => this.setState({ Email: text })}
                                />
                            </LinearGradient>
                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}
                                style={{ alignItems: 'center', marginTop: wp('2%'), height: wp('12%'), width: wp('65%'), borderRadius: 30 }}>

                                <TextInput
                                    placeholder={'รหัสผ่าน'}
                                    secureTextEntry={true}
                                    keyboardType='default'
                                    //paddingLeft= {20}
                                    style={{
                                        fontSize: wp('3.5%'), width: wp('64%'), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp('0.5%'),
                                        height: wp('11%'), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                    }}
                                    onChangeText={(text) => this.setState({ Password: text })}
                                />
                            </LinearGradient> */}
                            {/* <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}
                                    style={{ alignItems: 'center', marginTop: wp(2), height: wp(12), width: wp(65), borderRadius: 30 }}>

                                    <TextInput
                                        placeholder={'facebook'}
                                        keyboardType='default'
                                        //paddingLeft= {20}
                                        style={{
                                            fontSize: wp(3.5), width: wp(64), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.5),
                                            height: wp(11), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                        }}
                                        onChangeText={(text) => this.setState({ Faceid: text })}
                                    />
                                </LinearGradient> */}
                            {/* <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}
                                style={{ alignItems: 'center', marginTop: wp('2%'), height: wp('12%'), width: wp('65%'), borderRadius: 30 }}>
                                <DatePicker
                                    style={{
                                        fontSize: wp('3.5%'), width: wp('64%'), textAlign: 'center', margin: wp('0.5%'),
                                        height: wp('11%'), backgroundColor: 'rgb(256,256,256)', borderRadius: 30, color: '#abb8c3'
                                    }}
                                    customStyles={{ dateInput: { borderWidth: 0 }, placeholderText: { fontFamily: 'Prompt-Light' }, }}
                                    placeholder="วันเดือนปีเกิด"
                                    date={this.state.Birthdate}
                                    // mode="date"
                                    androidMode="spinner"
                                    format="YYYY-MM-DD"
                                    confirmBtnText="OK"
                                    cancelBtnText="CANCEL"
                                    showIcon={false}
                                    onDateChange={(date) => { this.setState({ Birthdate: date }) }}
                                />
                            </LinearGradient> */}
                            <View>
                                <TouchableOpacity
                                    onPress={() => this._Register()}
                                // onPress={this.toggleModal}
                                // style={styles.login}
                                >
                                    {/* <Image  source={require('./img/icon-design-09.png')} style={{height:wp(12), width: wp(12)}} /> */}
                                    <LinearGradient style={styles.login} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}>
                                        <Text style={{ fontSize: wp('3.5%'), fontFamily: 'Prompt-Light', color: '#fff' }}> สมัครบริการ </Text>
                                    </LinearGradient>
                                </TouchableOpacity>

                            </View>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Modal
                                isVisible={this.state.isModalVisible}
                                style={styles.bottommodal}
                                onSwipeComplete={() => this.toggleModal()}

                            >
                                <View style={styles.content}>
                                    <View
                                        style={{
                                            alignItems: "flex-start",
                                            justifyContent: "flex-start",
                                            margin: 10
                                        }}
                                    >
                                        {/* <Icon
                                        name="times"
                                        size={28}
                                        onPress={this.toggleModal}
                                    /> */}
                                    </View>
                                    <View
                                        style={{
                                            width: "100%",
                                            height: "50%",
                                            backgroundColor: "white",
                                            marginBottom: 20,
                                            justifyContent: "center",
                                            alignItems: "center",
                                            paddingLeft: 30,
                                            paddingRight: 30
                                        }}
                                    >
                                        <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}
                                            style={{ alignItems: 'center', marginTop: wp(2), height: wp(12), width: wp(65), borderRadius: 30 }}>


                                            <TextInput
                                                maxLength={6}
                                                placeholder={'ยืนยัน OTP'}
                                                keyboardType='number-pad'
                                                //paddingLeft= {20}
                                                style={{
                                                    fontSize: wp(3.5), width: wp(64), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.5),
                                                    height: wp(11), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                                }}
                                                onChangeText={(text) => this.setState({ OTPno: text })}
                                            />
                                        </LinearGradient>
                                        <TouchableOpacity
                                            onPress={this.CheckOtp}
                                        >
                                            <LinearGradient style={styles.login} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}>
                                                <Text style={{ fontSize: wp(3.5), fontWeight: 'bold', color: '#fff' }}> ยืนยัน </Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                        <TouchableOpacity
                                            onPress={this.NewRandomotp}
                                        >
                                            <View style={{ marginTop: wp(5) }}>
                                                <Text style={{ fontSize: wp(3.5), fontWeight: 'bold', color: '#black' }}> รับรหัส OTP ใหม่ </Text>
                                            </View>
                                        </TouchableOpacity>

                                        {/* <Button
                                            onPress={() => this.toggleModal()}
                                            title="Close"
                                        /> */}
                                    </View>
                                </View>
                            </Modal>
                        </View>
                    </View>
                </View>

            </KeyboardAwareScrollView>


        );
    }
}
const styles = StyleSheet.create({
    bnimage: {
        //borderRadius: 10,
        height: hp('63%'),
        width: wp('100%'),
        alignSelf: 'center'
    },
    fontStyle: {
        fontFamily: 'Prompt-Light'
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 20,
        width: wp(85),
        marginTop: wp(20)
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    buttonSearch: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        padding: 10,
        backgroundColor: '#79cff7',
        width: wp(60),
        marginTop: 40,
        marginBottom: 10,
        //marginLeft:14,
        borderRadius: 30,
        justifyContent: 'center',
        flexDirection: 'column'
    },
    loginE: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        padding: 10,
        backgroundColor: '#fff',
        width: wp(60),
        marginTop: 40,
        marginBottom: 10,
        //marginLeft:14,
        borderRadius: 30,
        justifyContent: 'center',
        flexDirection: 'column',
        borderWidth: 3,
        borderColor: '#25bef8'
    },
    login: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        //padding: 10,
        // backgroundColor: '#8A23FC',//add8e6 ,79b6d2
        height: wp(12),
        width: wp(65),
        marginTop: wp(5),
        // marginBottom: wp(2),
        //marginLeft:14,
        justifyContent: 'center',
        flexDirection: 'column',
        // borderWidth: 1,
        borderRadius: 30,
        // borderColor: '#fff'
    },
    loginModal: {
        //alignSelf: 'flex-start',
        // backgroundColor: '#03a9f4',
        padding: 10,
        backgroundColor: '#79cff7',
        width: wp(60),
        marginTop: 70,
        marginBottom: 10,
        //marginLeft:14,
        borderRadius: 30,
        justifyContent: 'center',
        flexDirection: 'column'
    },
    otpAgain: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        //padding: 10,
        // backgroundColor: '#8A23FC',
        height: wp(12),
        width: wp(65),
        // marginTop: wp(2.5),
        // marginBottom: wp(2.5),
        //marginLeft:14,
        justifyContent: 'center',
        flexDirection: 'column',
        // borderWidth: 1,
        borderRadius: 30,
        // borderColor: '#fff'   //8A23FC / 1D0AFE
    },
    lineStylew3: {
        borderWidth: 1,
        borderColor: '#009ACD',
        //backgroundColor: '#8A23FC',
        marginTop: 10,
        alignSelf: 'center',
        width: wp('20'),

    },
    bottommodal: {
        justifyContent: "flex-end",
        margin: 0
    },
    content: {
        backgroundColor: "white",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    lineStylew4: {
        borderWidth: 1,
        borderColor: '#117ffe',
        //backgroundColor: '#8A23FC',
        marginTop: 10,
        alignSelf: 'center',
        width: wp('25'),
        marginBottom: wp(5)
    },

    customerService: {
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: '#fff',
        height: wp(9),
        width: wp(20),

        flexDirection: 'row',
        justifyContent: 'center',
        borderWidth: 2,
        borderRadius: 30,
        marginRight: wp(1),
        marginLeft: wp(1)

    }

})

export default register;
