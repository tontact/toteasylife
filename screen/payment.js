import React, { Component } from 'react';
import { Platform, FlatList, View, Text, Button, Alert, TouchableHighlight, StyleSheet, TextInput, ActivityIndicator, Image, ImageBackground, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';
import {
    widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';

import { Icon } from 'react-native-elements'
import { TextInput as TextInput2 } from 'react-native-paper';


import { SearchBar } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
// import Swiper from 'react-native-swiper';
import SwiperFlatList from 'react-native-swiper-flatlist';
import LinearGradient from 'react-native-linear-gradient';

const IoniconsHeaderButton = passMeFurther => (
    <HeaderButton {...passMeFurther} IconComponent={Ionicons} iconSize={30} color="white" />
);

export default class payment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            onPress: '',
            text: ''
        };
    }


    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        // const params = navigation.getParam('checklogin')
        // this.state.user
        // alert(params)
        return {
            title: 'Prepaid',

            // headerLeft: (
            //   <TouchableHighlight style={styles.headerHome} >
            //     <Ionicons name='md-person' size={30} color='#FFF'   />
            //   </TouchableHighlight>
            // ),
            // headerRight: (
            //   <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>

            //   </HeaderButtons>
            // ),
            // headerTitle: <Logo/>,

            headerStyle: {
                backgroundColor: '#3399FF',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                // fontFamily: "Prompt-Light",
                textAlign: 'center',
                flex: 1,

            },
        }
    };




    render() {
        const sizeIcon = wp('5%');
        return (
            // <LinearGradient colors={['#FFF5EE', '#FFEFDB']} style={styles.linearGradient}>
            <View>

                <ImageBackground style={{ width: '100%', height: '100%', }} source={require('../img/tot-prepaid_highlight-teaser_m_new.jpg')} >
                    <ScrollView >
                        <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>


                            <Text style={{ flexDirection: 'column', justifyContent: 'center', alignSelf: 'center', marginTop: 20, fontSize: 30, fontWeight: 'bold' }}>Top Up</Text>

                            <View style={styles.searchSection}>
                                <Icon style={styles.searchIcon} name='phone-iphone' type='ionicons' size={25} color="#1E90FF" />
                                <TextInput
                                    label='Phone number'
                                    keyboardType={'numeric'}
                                    style={styles.input}
                                    placeholder="Phone Number"
                                    onChangeText={(searchString) => { this.setState({ searchString }) }}

                                // underlineColorAndroid="transparent"
                                />

                            </View>
                            <View>
                                <Text style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', paddingRight: 120, marginTop: 20, fontSize: 20, fontWeight: 'bold' }}>Top up channels</Text>
                            </View>



                            <View style={styles.iconstyle4}>


                                <View style={styles.iconstyle1}>
                                    <Icon
                                        size={sizeIcon}
                                        raised
                                        reverse

                                        name='location-on'
                                        type='material'
                                        color='#00BFFF'

                                        onPress={() => this.setState({
                                            onPress: 'Mobile card'
                                        })}
                                        containerStyle={{ margin: 7 }}
                                    />
                                    <Text style={{ alignSelf: 'center' }}>Mobile card</Text>



                                </View>

                                <View style={styles.iconstyle1}>
                                    <Icon
                                        size={sizeIcon}
                                        raised

                                        name='inbox'
                                        type='font-awesome'
                                        color='#00BFFF'
                                        onPress={() => this.setState({
                                            onPress: 'Prepaid card'
                                        })}
                                        containerStyle={{ margin: 7 }}
                                    />
                                    <Text style={{ alignItems: 'center' }}>Prepaid card</Text>
                                </View>

                                <View style={styles.iconstyle1}>
                                    <Icon
                                        size={sizeIcon}
                                        raised
                                        reverse

                                        underlayColor='black'
                                        name="info"
                                        type='font-awesome'
                                        color='#00BFFF'
                                        onPress={() => this.setState({
                                            onPress: 'Credit card'
                                        })}
                                        containerStyle={{ margin: 7 }}
                                    />

                                    <Text style={{ alignItems: 'center' }}>Credit card</Text>
                                </View>



                            </View>

                            {
                                this.state.onPress == '' ? null :
                                    this.state.onPress == 'Mobile card' ?
                                        <View style={styles.searchSection}>
                                            {/* <Icon style={styles.searchIcon} name='phone-iphone' type='ionicons' size={25} color="#1E90FF" /> */}

                                            <TextInput
                                                keyboardType='numeric'
                                                style={styles.input}
                                                placeholder="Mobile card"
                                                borderRadius={8}
                                                onChangeText={(searchString) => { this.setState({ searchString }) }}
                                            // underlineColorAndroid="transparent"
                                            />

                                        </View> : this.state.onPress == 'Prepaid card' ?

                                            <View style={styles.searchSection}>
                                                <TextInput
                                                    style={styles.input}
                                                    placeholder="Prepaid card"
                                                    borderRadius={8}
                                                    onChangeText={(searchString) => { this.setState({ searchString }) }}
                                                // underlineColorAndroid="transparent"
                                                />
                                            </View> :
                                            <View style={styles.searchSection}>
                                                <TextInput
                                                    style={styles.input}
                                                    placeholder="Credit card"
                                                    borderRadius={8}
                                                    onChangeText={(searchString) => { this.setState({ searchString }) }}
                                                // underlineColorAndroid="transparent"
                                                />

                                            </View>

                            }

                            <TouchableOpacity
                                style={styles.buttonSearch}
                                onPress={this.onPress}>
                                <Text style={{ fontWeight: 'bold' }}> Search </Text>
                            </TouchableOpacity>






                        </View>

                    </ScrollView>

                </ImageBackground>

            </View>
            // </LinearGradient>
        );
    }
}



const styles = StyleSheet.create({
    headerHome: {
        borderBottomColor: '#FFF',
        borderBottomWidth: wp('1.5'),
        borderColor: '#FFF',
        justifyContent: 'center',
        marginLeft: wp('2'),
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 64,
        width: wp('8.6'),
        height: wp('8.6')
    },
    linearGradient: {
        flexDirection: 'column',
        flex: 1,
        alignSelf: 'center',
        borderRadius: 5,
        height: 220,
        width: wp('100')
    },
    input: {
        // flex: 1,

        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        backgroundColor: '#fff',
        color: '#424242',
    },
    searchSection: {
        height: hp(4.3), width: wp(72),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#fff',
        marginTop: 20,
        borderRadius: 8,
        // marginLeft: 50,
        // marginRight: 50,
    },
    searchIcon: {
        padding: 0,
    },
    iconstyle4: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        margin: '4%',

    },
    iconstyle1: {
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: 5

    },

    buttonSearch: {
        alignSelf: 'center',
        marginTop: 150,
        backgroundColor: '#6E7B8B',
        padding: 10,
        // marginTop: 60,
        //marginLeft:14,
        borderRadius: 5,
        justifyContent: 'center',
        flexDirection: 'column'



    },



})

