import React, { Component } from 'react';
import {
    Image, Alert, StyleSheet, Text, Animated, TouchableHighlight,
    View, AsyncStorage, TextInput, NativeModules, ActivityIndicator, FlatList, TouchableOpacity, ImageBackground
} from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as loc,
    removeOrientationListener as rol
} from 'react-native-responsive-screen';

import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';
import { FormLabel, Button, Card } from 'react-native-elements'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons';
//import Logo from '../components/logo/index'
import RNRestart from "react-native-restart";
import Modal from 'react-native-modal';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';



console.disableYellowBox = true;

var ImagePicker = NativeModules.ImageCropPicker;

const width = '100%';
const IoniconsHeaderButton = passMeFurther => (
    <HeaderButton {...passMeFurther} IconComponent={Icon} iconSize={30} color="white" />

);

export default class checkService extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [
                {
                    name: 'TOT Fiber 2U',
                    select: false
                },
                {
                    name: 'TOT 3G',
                    select: false
                },
                {
                    name: 'TOT IPTV',
                    select: false
                },
                {
                    name: 'Fixed Line',
                    select: false
                }
            ],
            datauser: this.props.navigation.getParam('data')
        };
    }

    async Login() {
     
        axios.post('http://203.113.11.167/api/insert',{
            Idcard:this.state.datauser.Idcard,
            name:this.state.datauser.name,
            lastname:this.state.datauser.lastname,
            tel:this.state.datauser.tel,
            email:this.state.datauser.email,
            Service:JSON.stringify(this.state.data),
        }).then(result=>{
             AsyncStorage.setItem('user' , (JSON.stringify(result.data.id)))

                   RNRestart.Restart()
        })

        // axios.post('http://203.113.11.167/api/insert?Idcard='+this.state.datauser.Idcard+'&name=Jeerawut&lastname=Buayoy&email=k12club@hotmail.com&tel=0912312344&service=[{name:"TOT Fiber 2U" , select:true},{name:"TOT IPTV" , select:false}]')
    }

    
    TestSelect = (index) => {

        let a = this.state.data.slice(); //creates the clone of the state
        if (a[index].select == false) {
            a[index].select = true;
            this.setState({ data: a });
        } else {
            a[index].select = false;
            this.setState({ data: a });
        }

        // alert(JSON.stringify(this.state.data))
    }
    componentDidMount(){
        alert(JSON.stringify(this.state.datauser))
    }
    render() {

        return (
            <View style={{ flex: 1 }}>
                <KeyboardAwareScrollView>
                    <View style={{ alignSelf: 'center', flex: 1, width: wp(90), alignItems: 'center' }}>


                        {/* source={{ uri: 'http://203.113.11.192:3000/easylife/user1.png' }} */}

                        <Image style={[styles.bnimage]} source={require('../img/serviceHome1.png')} />

                        {/* <View style={{ flexDirection: 'column',alignSelf: 'center',alignItems: 'center'}}>
                        <Text style={{fontSize: wp(2), color: '#79b6d2', marginTop: wp(-72)}}>
                            TOT PUBLIC COMPANY LIMITED
                        </Text>
                        <Text style={{fontSize: wp(1.5), color: '#fff' }}>
                            @TOTPUBLIC
                        </Text>
                    </View> */}

                        <View style={{
                            width: wp(80), height: wp(85), backgroundColor: '#FFFFFF',
                            borderRadius: 10, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', flexDirection: 'column', marginTop: hp(-17),
                            shadowOffset: {width: 0,height: 2,},shadowOpacity: 0.25,shadowRadius: 3.84,}}>
         

                            <Text style={{ justifyContent: 'center', alignSelf: 'center', marginTop: wp(2), fontWeight: 'bold', fontSize: wp(4), color: '#009ACD' }}>บริการที่ลูกค้าใช้</Text>
                            <View style={styles.lineStylew3} />
                            {this.state.data.map((item, index) => {
                                return (
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => this.TestSelect(index)}
                                            style={[styles.customerService, { borderColor: item.select == true ? '#009ACD' : '#b6bcc0' }]}
                                        >
                                            <Text style={{ fontSize: wp(3.5), fontWeight: 'bold', color: item.select == true ? '#009ACD' : '#b6bcc0' }}> {item.name} </Text>
                                        </TouchableOpacity>
                                    </View>
                                )
                            })}

                        </View>

                        <View>
                            <TouchableOpacity
                                onPress={() => this.Login()}
                            // style={styles.login}
                            >
                                <LinearGradient style={styles.login} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}>
                                    <Text style={{ fontSize: wp(4), fontWeight: 'bold', color: '#fff' }}> ยืนยัน </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>

                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    bnimage: {
        //borderRadius: 10,
        height: hp(63),
        width: wp(100),
        alignSelf: 'center'
    },
    fontStyle: {
        fontFamily: 'Prompt-Light'
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 20,
        width: wp(85),
        marginTop: wp(20)
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    buttonSearch: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        padding: 10,
        backgroundColor: '#79cff7',
        width: wp(60),
        marginTop: 40,
        marginBottom: 10,
        //marginLeft:14,
        borderRadius: 30,
        justifyContent: 'center',
        flexDirection: 'column'
    },
    loginE: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        padding: 10,
        backgroundColor: '#fff',
        width: wp(60),
        marginTop: 40,
        marginBottom: 10,
        //marginLeft:14,
        borderRadius: 30,
        justifyContent: 'center',
        flexDirection: 'column',
        borderWidth: 3,
        borderColor: '#25bef8'
    },
    login: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        //padding: 10,
        alignSelf: 'center',
        // backgroundColor: '#8A23FC',
        height: wp(12),
        width: wp(65),
        marginTop: wp(5),
        marginBottom: 10,
        //marginLeft:14,
        justifyContent: 'center',
        flexDirection: 'column',
        // borderWidth: 1,
        borderRadius: 30,
        // borderColor: '#8A23FC'
    },
    loginModal: {
        //alignSelf: 'flex-start',
        // backgroundColor: '#03a9f4',
        padding: 10,
        backgroundColor: '#79cff7',
        width: wp(60),
        marginTop: 70,
        marginBottom: 10,
        //marginLeft:14,
        borderRadius: 30,
        justifyContent: 'center',
        flexDirection: 'column'
    },
    otpAgain: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        //padding: 10,
        backgroundColor: '#8A23FC',
        height: wp(12),
        width: wp(65),
        marginTop: wp(2.5),
        marginBottom: 10,
        //marginLeft:14,
        justifyContent: 'center',
        flexDirection: 'column',
        borderWidth: 1,
        borderRadius: 30,
        borderColor: '#8A23FC'
    },
    lineStylew3: {
        borderWidth: 1,
        borderColor: '#009ACD',
        //backgroundColor: '#8A23FC',
        marginTop: 10,
        marginBottom: wp(7.5),
        alignSelf: 'center',
        width: wp('20'),

    },
    customerService: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        //padding: 10,
        alignSelf: 'center',
        backgroundColor: '#fff',
        height: wp(12),
        width: wp(65),
        marginTop: wp(2.5),
        //marginBottom: 10,
        //marginLeft:14,
        justifyContent: 'center',
        flexDirection: 'column',
        borderWidth: 2,
        borderRadius: 30,
        // borderColor: '#c8cdd1'
    }
})