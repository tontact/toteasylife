// import React, { Component } from 'react';
// import { View, Text, Alert, AsyncStorage, TouchableOpacity,SafeAreaView } from 'react-native';
// import PinView from 'react-native-pin-view';
// import Icon from "react-native-vector-icons/Ionicons";
// import {
//     widthPercentageToDP as wp,
//     heightPercentageToDP as hp,
//     listenOrientationChange as loc,
//     removeOrientationListener as rol
// } from 'react-native-responsive-screen';
// import { exportDefaultDeclaration } from '@babel/types';
// import Ionicons from 'react-native-vector-icons/Ionicons';

// export default class changePassword extends Component {
//     constructor(props) {
//         super(props)
//         this.onComplete = this.onComplete.bind(this);
//         this.onReComplete = this.onReComplete.bind(this);
//         this.state = {
//             pin: "",
//             textcode: "รหัสผ่านเดิม",
//             pinoncomplete: this.onComplete,
//         };

//     }
//     componentDidMount() {
//         this.getPincode();
//         console.log("pin:" + this.state.pin)
//     }
//     getPincode = async () => {
//         try {
//             var pincode = await AsyncStorage.getItem('pincode');
//             this.setState({ pin: pincode })
//             console.log("pincode: " + pincode);
//         }
//         catch (error) {
//             alert(error)
//         }
//     }
//     Changepincode = async () => {
//         try {
//             const { pin } = this.state
//             await AsyncStorage.setItem('pincode', pin)
//         } catch (error) {
//             alert(error)
//         }
//     }
//     onComplete(inputtedPin, clear) {
//         if (inputtedPin !== this.state.pin) {
//             alert("รหัสไม่ถูกต้อง")
//             clear();
//         } else {
//             // alert("รหัสถูกต้อง")
//             clear();
//             this.setState({ textcode: "รหัสผ่านใหม่" })
//             this.setState({ pinoncomplete: this.onReComplete })
//         }
//     }
//     onReComplete(inputtedPin, clear) {
//         if (inputtedPin !== this.state.pin) {
//             this.setState({ pin: inputtedPin })
//             this.setState({ pinoncomplete: this.onComplete })
//             this.Changepincode();
//             clear();
//             Alert.alert(
//                 "เปลี่ยนแปลงรหัสผ่านแล้ว",
//                 "",
//                 [
//                     { text: 'OK', onPress: () => this.props.navigation.navigate('settingPage') },
//                 ],
//                 { cancelable: false },
//             );

//         } else {
//             alert("รหัสผ่านซ้ำกับรหัสผ่านเก่า")
//             clear();
//         }
//     }
//     onPress(inputtedPin, clear, pressed) {
//         console.log("Pressed: " + pressed);
//         console.log("inputtedPin: " + inputtedPin)
//         // clear()
//     }
//     render() {
//         console.log("555", this.state.pin)
//         const { goBack } = this.props.navigation;
//         return (

//             <View style={{ flexDirection: "column", alignItems: "center", }}>
//                 <SafeAreaView style={{ alignItems: "center", flexDirection: "column" }}>
//                     <View
//                         style={{
//                             alignSelf: "flex-start",
//                             // justifyContent: "flex-start",

//                         }}
//                     >
//                         <TouchableOpacity style={{ marginLeft: wp(-5), marginTop: wp(1) }} onPress={() => goBack()} >
//                             <Ionicons name='ios-arrow-back' size={30} />
//                         </TouchableOpacity>

//                     </View>
//                     <Text style={{ color: "#008DFF", fontSize: 30, fontFamily: "Prompt-Light", fontWeight: '500', marginTop: wp(20) }}>Enter the Code</Text>
//                     <Text style={{ color: "#008DFF", fontSize: 18, fontFamily: "Prompt-Regular", }}>{this.state.textcode}</Text>
//                     <PinView
//                         onPress={this.onPress}
//                         onComplete={this.state.pinoncomplete}
//                         pinLength={this.state.pin.length}
//                         buttonTextColor="#008DFF"
//                         buttonBgColor="white"
//                         inputActiveBgColor="#008DFF"
//                         keyboardViewStyle={{ borderWidth: 1, borderColor: '#008DFF', }}
//                         inputViewStyle={{ width: 15, height: 15, marginTop: wp(2) }}
//                         keyboardViewTextStyle={{ fontSize: 30, fontWeight: '300' }}
//                         buttonDeletePosition="right"
//                         deleteText={<Icon name="ios-backspace" size={32} color="black" />}
//                         buttonDeleteStyle={{ borderWidth: 0 }}
//                     />
//                 </SafeAreaView>
//             </View>
//         );
//     }
// }

import React, { Component } from 'react';
import { View, Text, Alert, AsyncStorage, TouchableOpacity, SafeAreaView } from 'react-native';
import PinView from 'react-native-pin-view';
import Icon from "react-native-vector-icons/Ionicons";
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as loc,
    removeOrientationListener as rol
} from 'react-native-responsive-screen';
export default  class Changepassword extends Component {
    constructor(props) {
        super(props)
        this.onComplete = this.onComplete.bind(this);
        this.onReComplete = this.onReComplete.bind(this);
        this.state = {
            pin: "",
            textcode: "รหัสผ่านเดิม",
            pinoncomplete: this.onComplete,
        };

    }
    static navigationOptions = ({ navigation }) => {
        return {
            headerTransitionPreset: 'fade-in-place',
            // title: 'ชำระค่าบริการ',
            headerTitle: (<Text style={{ fontFamily: "Prompt-SemiBold", color: "#000", textAlign: "center", flex: 1, fontSize: 20 }}>เปลี่ยนรหัสผ่าน</Text>),

            // headerRight: (
            //     <TouchableOpacity style={{ marginRight: wp(2.5) }} >
            //         <Icon name='ios-menu' size={30} color='#000' />
            //     </TouchableOpacity>
            // ),
            // headerRight: (
            //   <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>

            //   </HeaderButtons>
            // ),
            // //  headerTitle: <Logo/>,

            headerStyle: {
                backgroundColor: '#FFFAFA'
            },
            headerTintColor: '#000',
            headerTitleStyle: {
                fontFamily: "Prompt-Light",
                textAlign: 'center',
                flex: 1,
                fontSize: 20,
            },
        }
    };
    componentDidMount() {
        this.getPincode();
        console.log("pin:" + this.state.pin)
    }
    getPincode = async () => {
        try {
            var pincode = await AsyncStorage.getItem('pincode');
            this.setState({ pin: pincode })
            console.log("pincode: " + pincode);
        }
        catch (error) {
            alert(error)
        }
    }
    Changepincode = async () => {
        try {
            const { pin } = this.state
            await AsyncStorage.setItem('pincode', pin)
        } catch (error) {
            alert(error)
        }
    }
    onComplete(inputtedPin, clear) {
        if (inputtedPin !== this.state.pin) {
            Alert.alert("รหัสผ่านไม่ถูกต้อง", "กรุณาใส่รหัสผ่านใหม่")
            clear();
        } else {
            // alert("รหัสถูกต้อง")
            clear();
            this.setState({ textcode: "รหัสผ่านใหม่" })
            this.setState({ pinoncomplete: this.onReComplete })
        }
    }
    onReComplete(inputtedPin, clear) {
        if (inputtedPin !== this.state.pin) {
            this.setState({ pin: inputtedPin })
            this.setState({ pinoncomplete: this.onComplete })
            this.Changepincode();
            clear();
            Alert.alert(
                "เปลี่ยนแปลงรหัสผ่านแล้ว",
                "",
                [
                    { text: 'ตกลง', onPress: () => this.props.navigation.navigate('settingPage') },
                ],
                { cancelable: false },
            );

        } else {
            alert("รหัสผ่านซ้ำกับรหัสผ่านเก่า")
            clear();
        }
    }
    onPress(inputtedPin, clear, pressed) {
        console.log("Pressed: " + pressed);
        console.log("inputtedPin: " + inputtedPin)
        // clear()
    }
    render() {
        console.log("555", this.state.pin)
        const { goBack } = this.props.navigation;
        return (
            <View style={{ backgroundColor: 'white', flexDirection: "column", alignItems: "center" }}>
                <SafeAreaView style={{ alignItems: "center", flexDirection: "column" }}>
                <View
                        style={{
                             alignSelf: "flex-start",
                             // justifyContent: "flex-start",

                         }}
                     >
                         <TouchableOpacity style={{ marginLeft: wp(-5), marginTop: wp(1) }} onPress={() => goBack()} >
                             <Ionicons name='ios-arrow-back' size={30} />
                         </TouchableOpacity>

                     </View>
                <Text style={{ color: "#008DFF", fontSize: wp(8), fontFamily: "Prompt-Light", fontWeight:'500', marginTop:wp(10) }}>Enter the Code</Text>
                <Text style={{ color: "#008DFF", fontSize: wp(5), fontFamily: "Prompt-Light" , fontWeight:'400'}}>{this.state.textcode}</Text>
                <PinView
                    onPress={this.onPress}
                    onComplete={this.state.pinoncomplete}
                    pinLength={this.state.pin.length}
                    buttonTextColor="#008DFF"
                    buttonBgColor="white"
                    inputActiveBgColor="#008DFF"
                    keyboardViewStyle={{ borderWidth: 1, borderColor: '#008DFF', }}
                    inputViewStyle={{  width: wp(3), height: wp(3), marginTop:wp(2) }}
                    keyboardViewTextStyle={{ fontSize: wp(8), fontWeight: "300" }}
                    buttonDeletePosition="right"
                    deleteText={<Icon name="ios-backspace" size={32} color="black" />}
                    buttonDeleteStyle={{ borderWidth: 0 }}
                />
                </SafeAreaView>
            </View>
        );
    }
}