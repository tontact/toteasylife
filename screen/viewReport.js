import React, { Component } from 'react';
import { Platform, FlatList, View, Text, Button, StyleSheet, TextInput, Image, ImageBackground, TouchableOpacity, Modal } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
    widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';

import { ScrollView, ForceTouchGestureHandler } from 'react-native-gesture-handler';
// import Swiper from 'react-native-swiper';

import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';


export default class viewReportPage extends Component {



    constructor(props) {
        super(props);
        this.state = {
            Mobileno: '',
            SMSnumber: '',
            data: ''
        };

    }



    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        // const params = navigation.getParam('checklogin')
        // this.state.user
        // alert(params)
        return {
            headerTitle: (<Text style={{ fontFamily: "Prompt-Light", color: "#000", textAlign: "center", flex: 1, fontSize: 20, fontWeight: '500' }}>แจ้งเหตุเสีย</Text>),

            // headerRight: (

            // ),

            headerLeft: (
                <TouchableOpacity style={{ marginLeft: wp(3), }} >
                    <Ionicons name='ios-arrow-down' size={30} onPress={() => navigation.goBack()} />
                </TouchableOpacity>
            ),
            // //  headerTitle: <Logo/>,

            headerStyle: {
                backgroundColor: '#F9F9F9'
            },
            headerTintColor: '#000',
            headerTitleStyle: {
                fontFamily: "Prompt-Light",
                textAlign: 'center',
                flex: 1,
                fontSize: 20,
                fontWeight: 500


            },
        }
    };


    checkSMSNumber() {
        const url = "http://203.113.14.20:3000/getstatusbynumber";

        console.log(url);

        axios.post(url,
            {
                number: this.state.SMSnumber

            }
        )

            .then(result => {
                var data = Object.values(result.data)

                // this.setState({ data: result.data })
                this.setState ({data: data})
                alert(this.state.data.length)
            })
            .catch(err => {
                alert(JSON.stringify(err));
            })
    }



    render() {
        return (



            <ScrollView>
                <View style={{ alignItems: 'center', }} >

                    <View style={{ alignSelf: 'flex-start', flexDirection: 'row', marginTop: wp(8), marginLeft: wp(16.3) }}>
                        <Image source={require('../alliconESL/HomeIcons/HomeIconsAW114.png')} style={{ width: wp(9), height: wp(9), }} />
                        <Text style={{ marginTop: wp(1.7), color: '#3d90ff', fontWeight: '500', fontFamily: 'Prompt-Light', fontSize: wp(4) }}> หมายเลข:</Text>
                    </View>

                    <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#3ad1ff', '#117ffe']}
                        style={{ alignItems: 'center', marginTop: wp(2), height: wp(12), width: wp(65), borderRadius: 30 }}>

                        <TextInput
                            maxLength={10}
                            placeholder={'กรุณากรอกหมายตรวจสอบจาก SMS'}
                            keyboardType='default'
                            //paddingLeft= {20}
                            style={{
                                fontSize: wp(3.5), width: wp(64), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.4),
                                height: wp(11), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                            }}
                            onChangeText={(text) => this.setState({ SMSnumber: text })}
                        />
                    </LinearGradient>





                    <TouchableOpacity onPress={() => { this.checkSMSNumber() }} >

                        <LinearGradient style={styles.report2} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#3ad1ff', '#117ffe']}>
                            <Text style={{ fontSize: wp(4), fontWeight: '500', color: '#fff', fontFamily: 'Prompt-Light' }}>ตรวจสอบสถานะ</Text>
                        </LinearGradient>
                    </TouchableOpacity>

                    {
                        this.state.data.length == '' ?

                            null :

                            this.state.data.length > 1 ?

                                <View style={{ alignSelf: 'center', flexDirection: 'row', marginTop: wp(8) }} >
                                    <Text style={{ color: '#CC0000', fontWeight: '500', fontFamily: 'Prompt-Light', fontSize: wp(6) }}>*</Text>
                                    <Text style={{ marginLeft: wp(1), marginTop: wp(1.7), color: '#3d90ff', fontWeight: '500', fontFamily: 'Prompt-Light', fontSize: wp(4) }}>ไม่พบข้อมูลการแจ้งเหตุเสีย</Text>

                                </View>

                                : <View style={{ alignSelf: 'center', flexDirection: 'row', marginTop: wp(8) }} >
                                    <Text style={{ color: '#CC0000', fontWeight: '500', fontFamily: 'Prompt-Light', fontSize: wp(6) }}>success</Text>

                                </View>


                    }




                </View>
            </ScrollView>


        );
    }
}


const styles = StyleSheet.create({
    headerHome: {

        marginRight: wp(2),

    },

    report: {
        alignItems: 'center',
        height: wp(11),
        width: wp(60),
        justifyContent: 'center',
        flexDirection: 'column',
        borderRadius: 30,
        marginBottom: wp(5)

    },
    report1: {
        alignItems: 'center',
        height: wp(11),
        width: wp(50),
        justifyContent: 'center',
        flexDirection: 'column',
        borderRadius: 30,
        marginTop: wp(8)

    },
    report2: {
        alignItems: 'center',
        height: wp(11),
        width: wp(50),
        justifyContent: 'center',
        flexDirection: 'column',
        borderRadius: 30,
        marginTop: wp(8)

    },

});


