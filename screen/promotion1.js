import React, { Component } from 'react';
import { Animated, Platform, FlatList, View, Text, Button, Alert, StyleSheet, Image, TouchableOpacity, Dimensions, ActivityIndicator, Modal } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';
import {
    widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';

import { SearchBar } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
// import Swiper from 'react-native-swiper';
import SwiperFlatList from 'react-native-swiper-flatlist';
import LinearGradient from 'react-native-linear-gradient';
import Modal2 from 'react-native-modal';
import { FluidNavigator, Transition } from 'react-navigation-fluid-transitions';
import { Header } from 'react-navigation';
import axios from 'axios';
import Carousel from 'react-native-snap-carousel';
import ImageViewer from 'react-native-image-zoom-viewer';
import { Icon } from 'react-native-elements';


const HEADER_MAX_HEIGHT = wp(66);
const HEADER_MIN_HEIGHT = wp(20);
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;


const IoniconsHeaderButton = passMeFurther => (
    <HeaderButton {...passMeFurther} IconComponent={Ionicons} iconSize={28} color="white" />
);



let screenWidth = Dimensions.get('window').width;

// const {
//   height: SCREEN_HEIGHT,
// } = Dimensions.get('window');

// const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
// const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
// const NAV_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 64;

// const SCROLL_EVENT_THROTTLE = 16;
// const DEFAULT_HEADER_MAX_HEIGHT = 170;
// const DEFAULT_HEADER_MIN_HEIGHT = NAV_BAR_HEIGHT;
// const DEFAULT_EXTRA_SCROLL_HEIGHT = 30;
// const DEFAULT_BACKGROUND_IMAGE_SCALE = 1.5;




export default class promotion3 extends Component {
    constructor(props) {
        super(props);
        this.index2 = 0;

    }

    state = {
        zoomImage: false,
        ListPromotion: [],
        scrollY: new Animated.Value(0),
        pickerDisplayed: false,
        test: null,





    };




    //   test(){
    //       Alert.alert('in')
    //   }



    // renderItem(item) {
    //   return (

    //   );
    // }



    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        // const params = navigation.getParam('checklogin')
        // this.state.user
        // alert(params)


        return {

            headerTransitionPreset: 'fade-in-place',
            // title: 'ชำระค่าบริการ',
            headerTitle: (<Text style={{ fontFamily: "Prompt-Light", color: "#000", textAlign: "center", flex: 1, fontSize: 20, fontWeight: '500' }}>โปรโมชั่น</Text>),


            headerLeft: (
                <TouchableOpacity style={{ marginLeft: wp(3), }} >
                    <Ionicons name='ios-arrow-down' size={30} onPress={() => navigation.goBack()} />
                </TouchableOpacity>
            ),
            // //  headerTitle: <Logo/>,

            headerStyle: {
                backgroundColor: '#F9F9F9'
            },
            headerTintColor: '#000',
            headerTitleStyle: {
                fontFamily: "Prompt-Light",
                textAlign: 'center',
                flex: 1,
                fontSize: 20,
            },
        }
    };
    componentDidMount() {
        this.getPromotion();
    }

    getPromotion() {
        const url = "http://203.113.11.167/api/promotion";
        console.log(url);
        axios.get(url)
            .then(result => {
                this.setState({ ListPromotion: result.data })
                let data = []

                this.state.ListPromotion.forEach(item => {
                    if (item.type_id == 1) {
                        data.push(item)
                    }
                })
                this.setState({ ListPromotion: data })
                // alert(JSON.stringify(data))

                // alert(JSON.stringify(result.data))
            })

            .catch(err => {
                alert(JSON.stringify(err));
            })

    }


    isLegitIndex1(index, length) {
        if (index < 0 || index >= length) return false;
        return true;
    }

    pagination2 = (velocity) => {
        let nextIndex;
        if (Platform.OS == "ios")
            nextIndex = velocity > 0 ? this.index2 + 1 : this.index2 - 1;
        else
            nextIndex = velocity < 0 ? this.index2 + 1 : this.index2 - 1;
        if (this.isLegitIndex1(nextIndex, this.state.ListPromotion.length)) {
            this.index2 = nextIndex;
        }
        this.flatlist2.scrollToIndex({ index: this.index2, animated: true });
    }




    renderItem(item) {

        // const { } = style;
        return (
            <View style={{ flex: 1, alignSelf: 'center' }}>

                <TouchableOpacity
                    // underlayColor={false}
                    onPress={() => {

                        this.setState({
                            test: { name: item.name, shortdetail: item.shortdetail, fulldetail: item.fulldetail, image: item.image, image_details: item.image_details },
                            pickerDisplayed: true
                        })
                    }}>

                    <View style={{ alignSelf: 'center', marginBottom: wp(4), marginTop: wp('2%'), }} >


                        <View style={{ width: wp(100), overflow: 'hidden', shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5, }}>
                            <Image source={{ uri: item.image }} style={{ width: wp('90%'), height: wp('45%'), alignSelf: 'center', borderRadius: 10, marginTop: wp(2), marginBottom: wp(2), }} />
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            {/* <Image source={require('../img/2548.png')} style={{ width: 40, height: 40, borderRadius: (40 / 2), margin: 5 }} /> */}

                            <View style={{ flex: 1, flexDirection: 'column', }}>
                                <Text numberOfLines={2} style={{ alignSelf: 'center', color: "#000000EE", width: wp(90), fontWeight: '500', fontFamily: 'Prompt-Regular', fontSize: wp('4%') }}>{item.name}</Text>
                                <View style={{ flexDirection: 'column', marginTop: 1 }}>
                                    <Text numberOfLines={2} style={{ alignSelf: 'center', color: "#000000AA", width: wp(90), fontSize: wp('3.5%'), fontFamily: 'Prompt-Light' }}>{item.shortdetail}</Text>
                                </View>
                                {/* <View style={styles.lineStylew4} />

<Text style={{ justifyContent: 'center', alignSelf: 'center', fontWeight: 'bold', color: '#1E90FF' }}>View detail</Text> */}
                            </View>
                        </View>


                    </View>
                </TouchableOpacity>

            </View>
        );
    }

    modaldetail = () => {
        const imageOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });
        const imageTranslate = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -50],
            extrapolate: 'clamp',
        });
        //let data = [1, 2, 3]
        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
            extrapolate: 'clamp',
        });
        return (
            <LinearGradient colors={['#FFF', '#FFF']} style={styles.linearGradient}>
                <View style={{ flex: 1, flexDirection: 'column', }}>
                    <ScrollView style={{ flex: 1, }}
                        showsVerticalScrollIndicator={false}
                        style={styles.fill}
                        scrollEventThrottle={16}
                        onScroll={Animated.event(
                            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }]
                        )}>


                        <Text style={{ marginTop: wp(70), marginLeft: wp(3), marginRight: wp(3), width: wp(95), fontFamily: 'Prompt-SemiBold', fontSize: 18, color: 'black' }}>
                            {this.state.test.name}
                        </Text>

                        <View style={{ alignItems: 'center' }}>
                            <Text style={{ marginTop: wp(5), marginLeft: wp(3), marginRight: wp(3), width: wp(85), textAlign: 'justify', fontFamily: 'Prompt-Light', color: 'black' }}>
                                {this.state.test.shortdetail}
                            </Text>


                            <View style={styles.cardNewsContainer}>
                                <Carousel
                                    ref={(c) => { this._carousel = c; }}
                                    data={this.state.test.image_details}
                                    sliderWidth={wp('100%')}
                                    itemWidth={wp(70)}
                                    enableSnap={true}
                                    loop={true}
                                    lockScrollWhileSnapping={true}
                                    renderItem={({ item }) => {
                                        return (
                                            <View style={{ shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, }}>
                                                <TouchableOpacity onPress={() => { this.setState({ zoomImage: true }) }}>
                                                    <Image style={{ alignSelf: 'center', recideMode: 'contian', width: wp(70), height: wp(50), borderRadius: 10, marginTop: wp(5) }} source={{ uri: item.imagedetail }}
                                                        PlaceholderContent={<ActivityIndicator />} />
                                                </TouchableOpacity>

                                                <Modal visible={this.state.zoomImage} transparent={true} swipeDirection='down' swipeThreshold={50} onSwipeComplete={() => this.setState({ zoomImage: false })} >
                                                    <ImageViewer imageUrls={this.state.test.image_details.map((item, index) => { return { url: item.imagedetail } })} />
                                                    <Icon
                                                        underlayColor={false}
                                                        // raised
                                                        name='md-close'
                                                        type='ionicon'
                                                        color='#FFF'
                                                        size={wp(7)}
                                                        containerStyle={{ position: 'absolute', right: wp(10), top: wp(13), alignSelf: 'flex-end', marginRight: wp(-2) }}
                                                        onPress={() => this.setState({
                                                            zoomImage: false
                                                        })}
                                                    />
                                                </Modal>
                                            </View>
                                        );
                                    }}
                                />
                            </View>

                            <Text style={{ marginTop: wp(5), marginLeft: wp(3), marginRight: wp(3), width: wp(85), textAlign: 'justify', fontFamily: 'Prompt-Light', color: 'black' }}>

                                {this.state.test.fulldetail}
                            </Text>

                        </View>

                        <View style={styles.lineStylew1}></View>

                        <View style={{ height: wp(70), width: wp(100), flex: 1, backgroundColor: 'rgb(247, 247, 247)' }} >
                            <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), fontWeight: '500', marginLeft: wp(7), marginTop: wp(5), }}>โปรโมชั่นที่เกี่ยวข้อง</Text>
                            <FlatList

                                style={{ alignSelf: 'center', }}
                                showsHorizontalScrollIndicator={false}
                                horizontal
                                data={this.state.ListPromotion}
                                ref={ref => (this.flatlist2 = ref)}
                                onScrollEndDrag={e => {
                                    this.pagination2(e.nativeEvent.velocity.x);
                                }}
                                renderItem={({ item }) => {
                                    return (

                                        <TouchableOpacity

                                            // underlayColor={false}
                                            onPress={() => {
                                                this.setState({
                                                    test: { name: item.name, shortdetail: item.shortdetail, fulldetail: item.fulldetail, image: item.image, image_details: item.image_details },
                                                    pickerDisplayed: true
                                                })
                                            }}
                                            style={styles.BlockPromo1}>



                                            <View style={{ width: wp('42%'), alignSelf: 'center', borderTopRightRadius: 10, borderTopLeftRadius: 10, overflow: 'hidden', }}>
                                                <Image source={{ uri: item.image }} style={{ width: wp('42%'), height: wp('36%') }} />
                                            </View>
                                            <View style={{ flexDirection: 'row' }}>
                                                {/* <Image source={require('../img/2548.png')} style={{ width: 40, height: 40, borderRadius: (40 / 2), margin: 5 }} /> */}

                                                <View style={{ flex: 1, flexDirection: 'column', marginLeft: wp(3), marginTop: 8, }}>
                                                    <Text numberOfLines={1} style={{ color: "#000000EE", width: wp('35'), fontWeight: '500', fontFamily: 'Prompt-Regular', fontSize: 13, }}>{item.name}</Text>
                                                    <View style={{ flexDirection: 'column', marginTop: 1, marginBottom: wp(2) }}>
                                                        <Text numberOfLines={1} style={{ color: "#000000AA", width: wp('35'), fontSize: 10, fontFamily: 'Prompt-Light' }}>{item.shortdetail}</Text>
                                                    </View>
                                                    {/* <View style={styles.lineStylew4} />
    
                <Text style={{ justifyContent: 'center', alignSelf: 'center', fontWeight: 'bold', color: '#1E90FF' }}>View detail</Text> */}
                                                </View>
                                            </View>


                                        </TouchableOpacity>
                                    )
                                }}
                            />
                        </View>



                    </ScrollView>


                    <Animated.View style={[styles.header, { height: headerHeight }]}>
                        <Animated.View>
                            <View style={styles.bar}>
                                <Text style={styles.title}  >TOT Easy Life</Text>
                            </View>
                        </Animated.View>

                        <Animated.Image
                            style={[styles.backgroundImage, { opacity: imageOpacity, transform: [{ translateY: imageTranslate }] },]}
                            source={{ uri: this.state.test.image }} />

                        {/* <Text style={{ justifyContent: 'flex-start', alignItems: 'baseline', marginLeft: wp(3), fontWeight: '900', fontSize: 20 }} onPress={() => { this.setState({ pickerDisplayed: false, test: null }) }} >X</Text> */}

                        <View style={{ marginTop: wp(-10), marginLeft: wp(4) }}>
                            <Ionicons
                                containerStyle={{ justifyContent: 'flex-start', alignSelf: 'flex-start' }}
                                name='ios-close'
                                color='white'
                                size={45}

                                onPress={() => { this.setState({ pickerDisplayed: false, test: null }) }} />
                        </View>
                    </Animated.View>



                </View>
            </LinearGradient>
        )
    }


    render() {
        const sizeIcon = wp('5%');
        return (
            // <LinearGradient colors={['#FFFAFA', '#FFFAF0']} style={styles.linearGradient}>
            <ScrollView style={{ flex: 1, }}>

                <View style={{ flex: 1, marginBottom: wp(20) }}>
                    <LinearGradient start={{ x: 1.5, y: 1 }} end={{ x: 0.5, y: 2 }} colors={['#00FFFF', '#0089D7',]} style={styles.linearGradient}>



                        <View style={styles.detailTopBottomSubContainer}>

                            <Text style={{ color: 'white', fontSize: scaleToDimension(35), fontWeight: '500', fontFamily: 'Prompt-Light', }}>INTERNET</Text>
                            <Text style={{ color: 'white', fontSize: scaleToDimension(35), fontWeight: '500', fontFamily: 'Prompt-Light', marginTop: wp(-5) }}>& FIBER</Text>

                            <Text style={{ color: 'white', fontSize: scaleToDimension(18), fontWeight: '500', fontFamily: 'Prompt-Light', marginTop: wp(0) }}>อินเตอร์เน็ต / ไฟเบอร์</Text>
                        </View>

                        <View style={{ width: wp(57), height: wp(57), marginLeft: wp(43), marginTop: wp(-3), }}>
                            <Image resizeMode="contain" style={{ width: wp(70), height: wp(70), }}
                                source={require('../alliconESL/NewsIcons/PromotionTOTeasylifeAW1copy_18.png')}
                            />
                        </View>

                    </LinearGradient>

                    <View style={{ marginTop: wp(5) }}>
                        <FlatList

                            showsHorizontalScrollIndicator={false}

                            data={this.state.ListPromotion}
                            ref={ref => (this.flatlist2 = ref)}
                            renderItem={({ item }) => this.renderItem(item)}>
                        </FlatList>

                    </View>

                    {this.state.test != null ?
                        <Modal2

                            animationType={"slide"}
                            isVisible={this.state.pickerDisplayed}
                            onSwipeComplete={() => this.setState({ pickerDisplayed: false })}
                            // swipeDirection={['down']}
                            style={styles.bottomModal}
                            onBackdropPress={() => this.setState({ pickerDisplayed: false })}

                        >

                            {
                                this.modaldetail()
                            }

                        </Modal2> : null
                    }

                </View>




            </ScrollView>
            // </LinearGradient>
        );
    }
}

const scaleToDimension = (size) => {
    return screenWidth * size / 375
};

const styles = StyleSheet.create({


    linearGradient: {
        flexDirection: 'column',
        flex: 1,
        alignSelf: 'center',

        height: wp(60),
        width: wp('100')
    },



    detailTopBottomSubContainer: {
        width: screenWidth - 30,
        backgroundColor: 'transparent',
        position: 'absolute',
        // bottom: wp(6),
        // left: wp(7),
        // right: wp(5),
        flexDirection: "column",
        marginTop: wp(10),
        alignSelf: 'center',
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
        flex: 1,

    },
    cardNewsContainer: {
        alignSelf: 'center',
        marginTop: wp(5),
        shadowColor: "#000",
        backgroundColor: 'rgb(238, 238, 238)',
        width: wp(100), height: wp(60),
    },
    lineStylew1: {
        borderWidth: 0.25,
        borderColor: '#DCDCDC',
        alignSelf: 'center',
        width: wp(100),
        height: wp(3),
        marginTop: wp(15),
        backgroundColor: 'rgb(239, 239, 239)'
    },
    BlockPromo1: {
        flex: 1,
        backgroundColor: '#FFF',
        margin: wp(1.5),
        borderRadius: 10,
        shadowColor: "#000",
        marginBottom: wp(3), marginTop: wp(3),
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.23, shadowRadius: 2.62, elevation: 4,
        alignSelf: 'center'
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#000',
        overflow: 'hidden',
    },
    bar: {
        marginTop: wp(8),
        height: wp(8),
        alignItems: 'center',
        justifyContent: 'center',
      

    },
    title: {
        backgroundColor: 'transparent',
        color: 'white',
        fontSize: wp('4.5%'), fontWeight: '500',
        fontFamily: 'prompt-light'
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: wp(100),
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
    },
})










