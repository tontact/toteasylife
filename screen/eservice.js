import React, { Component } from 'react'
import { Text, View ,TouchableOpacity} from 'react-native'
import { WebView } from 'react-native-webview';

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as loc,
    removeOrientationListener as rol
  } from 'react-native-responsive-screen';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class eservice extends Component {
    constructor(props) {
        super(props);

        this.state = {
            url: this.props.navigation.getParam('url')

        }
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
   
       // const params = navigation.getParam('checklogin')
       // this.state.user
       // alert(params)
       return {
         headerTransitionPreset: 'fade-in-place',
         // title: 'ชำระค่าบริการ',
         headerTitle: (<Text style={{  fontFamily: "Prompt-Light", color: "#000", fontSize: 20, fontWeight: '500' , textAlign: "center", flex: 1,}}>{'ชำระค่าบริการ'}</Text>),
   
   
         headerLeft: (
           <TouchableOpacity style={{ marginLeft: wp(3), }} >
             <Ionicons name='ios-arrow-back' size={30} onPress={() => navigation.goBack()} />
           </TouchableOpacity>
         ),
         // headerRight: (
         //   <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>
   
         //   </HeaderButtons>
         // ),
         // //  headerTitle: <Logo/>,
         headerRight: (
           <TouchableOpacity style={{ marginRight: wp(2.5) }} >
   
           </TouchableOpacity>
         ),
   
         headerStyle: {
           backgroundColor: '#F9F9F9'
         },
         headerTintColor: '#000',
         headerTitleStyle: {
           // fontFamily: "Prompt-Light",
           textAlign: 'center',
           flex: 1,
   
         },
       }
     };
   
    render() {
        return (
            // <View>
            // <WebView source={{ uri: "http://google.com" }} />
            <WebView source={{ uri: this.props.navigation.getParam('url') }} />


            // </View>
        )
    }
}
