import React, { Component } from 'react';
import { Platform, FlatList, View, Text, Button, StyleSheet, TextInput, Image, ImageBackground, TouchableOpacity, Modal, Alert } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';

import { ScrollView,  } from 'react-native-gesture-handler';
// import Swiper from 'react-native-swiper';

import LinearGradient from 'react-native-linear-gradient';

import axios from 'axios';


export default class report extends Component {



  constructor(props) {
    super(props);
    this.state = {
      serviceNumber: '',
      visibleModalId: null,
    };

  }



  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    // const params = navigation.getParam('checklogin')
    // this.state.user
    // alert(params)
    return {
      headerTitle: (<Text style={{ fontFamily: "Prompt-Light", color: "#000", textAlign: "center", flex: 1, fontSize: 20, fontWeight: '500' }}>แจ้งเหตุเสีย</Text>),

      // headerRight: (

      // ),

      headerLeft: (
        <TouchableOpacity style={{ marginLeft: wp(3), }} >
          <Ionicons name='ios-arrow-back' size={30} onPress={() => navigation.goBack()} />
        </TouchableOpacity>
      ),
      // //  headerTitle: <Logo/>,

      headerStyle: {
        backgroundColor: '#F9F9F9'
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        fontFamily: "Prompt-Light",
        textAlign: 'center',
        flex: 1,
        fontSize: 20,
        fontWeight: 500


      },
    }
  };

  checkServiceNumber() {
    const url = "http://app1.toteservice.com/index.php?r=api/Easylife/ChkSVNO&username=easylife&password=e@sylifeP@ss&udid=&serviceNo=" + this.state.serviceNumber;

    console.log(url);
    axios.get(url)
      .then(result => {
        var data = result.data.text
        if (result.data.acss == 1) {
          if (result.data.chkDebt == "no") {
            this.props.navigation.navigate('reportProblem',
              {

                serviceNumber: this.state.serviceNumber
              }
            )

          }
          else
          
          {
            Alert.alert(
              "ระงับใช้บบริการ เนื่องจากมียอดค้างชำระ ",
              "*กรุณากดตกลงเพื่อเพิ่มข้อมูลบริการและเลือกการชำระเงิน",
              [
                { text: 'ปิด', onPress: () => this.setState({ visibleModal: null }) },
              ],
              { cancelable: false },
            );
          }
          

        }

        else if (result.data.acss == 0) {
          Alert.alert(
            "หมายเลขบริการไม่ถูกต้อง",
            "กรุณากรอกหมายเลขบริการให้ถูกต้องตามที่แสดงไว้บนหน้าบิล",
            [
              { text: 'ปิด', onPress: () => this.setState({ visibleModal: null }) },
            ],
            { cancelable: false },
          );
        }
      })
      .catch(err => {
    //   alert(JSON.stringify(err));
      })
  }




  render() {
    return (



      <ScrollView>
        <View style={{ alignItems: 'center', }} >
          <Image source={require('../alliconESL/Iconnewsetapp/PAYTOTeasylifeAW1copy06.png')} style={{ width: wp(55), height: wp(55), marginTop: wp(8) }} />
          {/* <TouchableOpacity >

            <LinearGradient style={styles.report} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}>
              <Text style={{ fontSize: wp(4), fontWeight: '500', color: '#fff', fontFamily: 'Prompt-Light' }}>แจ้งปัญหาการใช้งาน</Text>
            </LinearGradient>
          </TouchableOpacity> */}
          <Image source={require('../alliconESL/HomeIcons/HomeIconsAW112.png')} style={{ width: wp(40), height: wp(7), marginTop: wp(8), alignSelf: 'flex-start', marginLeft: wp(16.3) }} />

          <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#3ad1ff', '#117ffe']}
            style={{ alignItems: 'center', marginTop: wp(2), height: wp(12), width: wp(65), borderRadius: 30 }}>

            <TextInput
              maxLength={10}
              placeholder={'กรุณากรอกหมายเลขบริการ'}
              keyboardType='default'
              placeholderTextColor = "grey"
          
              //paddingLeft= {20}
              style={{
                fontSize: wp(3.5), width: wp(64), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.4),
                height: wp(11), backgroundColor: 'rgb(256,256,256)', borderRadius: 30,
              }}
              onChangeText={(text) => this.setState({ serviceNumber: text })}
            />
          </LinearGradient>

          {/* <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
            style={{ alignItems: 'center', marginTop: wp(2), height: wp(12), width: wp(65), borderRadius: 30 }}>

            <TextInput
              maxLength={10}
              placeholder={'ปัญหาการใช้งานที่พบอื่น'}
              keyboardType='default'
              //paddingLeft= {20}
              style={{
                fontSize: wp(3.5), width: wp(64), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.5),
                height: wp(11), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
              }}
              onChangeText={(text) => this.setState({ otherProblem: text })}
            />
          </LinearGradient>

          <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
            style={{ alignItems: 'center', marginTop: wp(2), height: wp(12), width: wp(65), borderRadius: 30 }}>

            <TextInput
              maxLength={10}
              placeholder={'ชื่อ - นามสกุล'}
              keyboardType='default'
              //paddingLeft= {20}
              style={{
                fontSize: wp(3.5), width: wp(64), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.5),
                height: wp(11), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
              }}
              onChangeText={(text) => this.setState({ name: text })}
            />
          </LinearGradient>

          <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
            style={{ alignItems: 'center', marginTop: wp(2), height: wp(12), width: wp(65), borderRadius: 30 }}>
            <TextInput
              maxLength={10}
              placeholder={'หมายเลขติดต่อกลับ'}
              keyboardType='number-pad'
              //paddingLeft= {20}
              style={{
                fontSize: wp(3.5), width: wp(64), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.5),
                height: wp(11), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
              }}
              onChangeText={(text) => this.setState({ callbackNo: text })}
            />
          </LinearGradient>

          <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
            style={{ alignItems: 'center', marginTop: wp(2), height: wp(12), width: wp(65), borderRadius: 30 }}>
            <TextInput
              maxLength={10}
              placeholder={'ที่อยู่อีเมลล์'}
              keyboardType='email-address'
              //paddingLeft= {20}
              style={{
                fontSize: wp(3.5), width: wp(64), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.5),
                height: wp(11), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
              }}
              onChangeText={(text) => this.setState({ Email: text })}
            />
          </LinearGradient> */}
          <TouchableOpacity
            onPress={() => {

              this.checkServiceNumber()
              // this.props.navigation.navigate('reportProblem',
              // {

              //   serviceNumber:this.state.serviceNumber
              // }
              // )

            }}

          >

            <LinearGradient style={styles.report1} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#3ad1ff', '#117ffe']}>
              <Text style={{ fontSize: wp(4), fontWeight: '500', color: '#fff', fontFamily: 'Prompt-Light' }}>แจ้งเหตุเสีย</Text>
            </LinearGradient>
          </TouchableOpacity>

          {/* <TouchableOpacity onPress={() => { this.props.navigation.navigate('viewReportPage') }}>
            <LinearGradient style={styles.report2} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#3ad1ff', '#117ffe']}>
              <Text style={{ fontSize: wp(4), fontWeight: '500', color: '#fff', fontFamily: 'Prompt-Light' }}>ตรวจสอบสถานะ</Text>
            </LinearGradient>
          </TouchableOpacity> */}


        </View>
      </ScrollView>


    );
  }
}


const styles = StyleSheet.create({
  headerHome: {

    marginRight: wp(2),

  },

  report: {
    alignItems: 'center',
    height: wp(11),
    width: wp(60),
    justifyContent: 'center',
    flexDirection: 'column',
    borderRadius: 30,
    marginBottom: wp(5)

  },
  report1: {
    alignItems: 'center',
    height: wp(11),
    width: wp(50),
    justifyContent: 'center',
    flexDirection: 'column',
    borderRadius: 30,
    marginTop: wp(8)

  },
  report2: {
    alignItems: 'center',
    height: wp(11),
    width: wp(50),
    justifyContent: 'center',
    flexDirection: 'column',
    borderRadius: 30,
    marginTop: wp(2)

  },

});


