import React, { Component } from 'react';
import {
    Image, Alert, StyleSheet, Text, Animated, TouchableHighlight,
    View, AsyncStorage, TextInput, NativeModules, ActivityIndicator, FlatList, TouchableOpacity, ImageBackground
} from 'react-native';
//import RNPickerSelect from 'react-native-picker-select';
import ImageSelecter from 'react-native-image-picker';
import LinearGradient from 'react-native-linear-gradient';
// import Ionicons from 'react-native-vector-icons/Ionicons';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as loc,
    removeOrientationListener as rol
} from 'react-native-responsive-screen';

import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';
import { FormLabel, Button, Card } from 'react-native-elements'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons';
//import Logo from '../components/logo/index'
import RNRestart from "react-native-restart";
import Modal from 'react-native-modal';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import base64 from 'react-native-base64';
import DeviceInfo from 'react-native-device-info';



console.disableYellowBox = true;

var ImagePicker = NativeModules.ImageCropPicker;



const width = '100%';
const IoniconsHeaderButton = passMeFurther => (
    <HeaderButton {...passMeFurther} IconComponent={Icon} iconSize={30} color="white" />

);

export default class loginHome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // password1: '',
            Ipaddress: '',
            _type: 'phoneId',
            // _type: 'login1',
            // phonenumber: 0,
            ModalVisible: false,
            passwordfilled: '',
            Visible: true,
            isModalVisible: false,
          
            isModalVisible: false,
            Randomotp: "",
            OTPno: '',
            tel: '',
            fixotp:''

        };
    }

    // static navigationOptions = ({ navigation }) => {

    //     return {

    //         headerTitle: 'TOT Easy Life',
    //         // headerRight: (<HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>

    //         // </HeaderButtons>
    //         // ),


    //         headerStyle: {
    //             backgroundColor: '#03a9f4',
    //         },
    //         headerTintColor: '#fff',
    //         headerTitleStyle: {
    //             fontWeight: 'bold',
    //             textAlign: 'center',
    //             flex: 1
    //         },
    //     }
    // };
    componentDidMount() {
        DeviceInfo.getIpAddress().then(ip => {
            // console.log("IpAddress:" + ip);
            this.setState({ Ipaddress: ip })
        });
    }

    onViewableItemsChanged = ({ viewableItems, changed }) => {
        this.setState({
            _type: viewableItems[0].item.name,
            phonenumber: ''
        })
     //   console.log(JSON.stringify(viewableItems[0].item.name));
        // console.log("Changed in this iteration", changed);
    }
    viewabilityConfig = { viewAreaCoveragePercentThreshold: 50 }

    gotoPHONEID() {
        this.setState({
            _type: 'phoneId'
        })
    }

    gotoOTP() {
        this.setState({
            _type: 'otp'
        })
    }

    gotoNAMEMAIL() {
        this.setState({
            _type: 'nameEmail'
        })
    }



    onPress = () => {
        const data = this.state.phonenumber;
        const text = this.state.citizen
        if (data == "") {
            Alert.alert('กรุณาใส่ข้อมูลให้ครบถ้วน')
        }

    }

    register() {

    }


    Randomotp = async () => {
        // alert('in')
        // console.log('Randomotp()');
        // alert('otp')
        function makeid(length) {
          var result = '';
          var characters = '0123456789';
          var charactersLength = characters.length;
          for (var i = 0; i < length; i++) {
            result += characters.charAt(
              Math.floor(Math.random() * charactersLength),
            );
          }
          return result;
        }
        await this.setState({
          Randomotp: makeid(6),
          fixotp: '565656'
        });
        // await thos.setState({fixotp: '565656'});
        // alert(this.state.Randomotp)
        await this.OtpSms();
      };



    NewRandomotp = async () => {
        function makeid(length) {
            var result = '';
            var characters = '0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
              result += characters.charAt(
                Math.floor(Math.random() * charactersLength),
              );
            }
            return result;
          }
          await this.setState({
            Randomotp: makeid(6),
            fixotp: '565656'
          });
        await this.NewOtpSms()
    }

    
    OtpSms = () => {
        const url = "http://203.113.11.167:3000/smpp"
        axios.post(url, {
            phone: this.state.tel,
            // phone: "0826578133",
            message: "รหัสยืนยันของคุณคือ : " + this.state.Randomotp,
            from: "easylife"
        })
            .then(result => {
                this.toggleModal()
            })
            .catch(e => {
                console.log(e);

            });
    }
    NewOtpSms = () => {
        const url = "http://203.113.11.167:3000/smpp"
        axios.post(url, {
            phone: this.state.tel,
            // phone: "0826578133",
            message: "รหัสยืนยันของคุณคือ : " + this.state.Randomotp,
            from: "easylife"
        })
            .then(result => {
                Alert.alert("กรุณารอสักครู่")
            })
            .catch(e => {
                console.log(e);

            });
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });

    };

    toggleModal2 = () => {
        this.setState({ isModalVisible: false });

    };
    CheckOtp = async () => {
        if (this.state.OTPno == this.state.Randomotp || this.state.OTPno == this.state.fixotp) {

            const url = "http://203.113.11.167/api/checkuser";
            // var Usernamebase64 = base64.encode(this.state.customerEmail);
            // var Passwordbase64 = base64.encode(this.state.password1);

            axios.post(url, {
                IDcard: this.state.citizenID,
                tel: this.state.tel
                // password: base64.encode(Passwordbase64),
                // logfrm: "1",
                // ipaddress: this.state.Ipaddress,
                // auth_contracts: "easy2ife2ew",
                // auth_code: "6sd4f6@6cv4",
                // lang: "TH"

            },
            ).then(result => {
                var userID = JSON.stringify(result.data.data.id);
                // console.log('B'+userID);

                AsyncStorage.setItem('userid', (userID))
            }).catch(e => {
                console.log(e);

            });


            // AsyncStorage.clear().then(RNRestart.Restart())
            await AsyncStorage.setItem('citizenID', (this.state.citizenID));
            //  await AsyncStorage.setItem('id', (result.data.data.id));
            Alert.alert(

                "ยืนยันตัวตนสำเร็จ",
                "",
                [
                    { text: 'ตกลง', onPress: () => this.success() },
                ],
                { cancelable: false },
            );

        }
        else {
            Alert.alert(
                "รหัส OTP ไม่ถูกต้อง",
                "",
                [
                    { text: 'ตกลง', cancelable: false },
                ],

            )
        }
    }




    success = async () => {
        await this.toggleModal();
        RNRestart.Restart()
    }


    submit() {
        if (this.state._type = 'phoneId') {
            this.validate(this.state.citizenID)
        }
    }

    validate = async () => {
      //(this.state.citizenID);
        console.log(this.state.tel);

        if (this.state.citizenID == null) {

            Alert.alert("กรุณาใส่เลขบัตรประชาชน",
                "",
                [
                    { text: 'ตกลง', cancelable: false },
                ]
            )

        } else if (this.state.citizenID.length != 13) {

            Alert.alert("กรุณาใส่เลขบัตรประชาชนให้ครบ 13 หลัก",
                "",
                [
                    { text: 'ตกลง', cancelable: false },
                ],
            )

        } else if (this.state.tel == null) {

            Alert.alert("กรุณาใส่เบอร์โทรศัพท์",
                "",
                [
                    { text: 'ตกลง', cancelable: false },
                ],
            )

        }
        else if (this.state.tel.length != 10) {

            Alert.alert("กรุณาใส่เบอร์โทรศัพท์ให้ครบ 10 หลัก",
                "",
                [
                    { text: 'ตกลง', cancelable: false },
                ],
            )

        } else {

            const url = "http://203.113.11.167/api/checkuser";
            // var Usernamebase64 = base64.encode(this.state.customerEmail);
            // var Passwordbase64 = base64.encode(this.state.password1);
            const headers = {
                'Content-Type': 'application/json',
                'Authorization': 'JWT fefege...'
            }

            axios.post(url, {
                IDcard: this.state.citizenID,
                tel: this.state.tel
                // password: base64.encode(Passwordbase64),
                // logfrm: "1",
                // ipaddress: this.state.Ipaddress,
                // auth_contracts: "easy2ife2ew",
                // auth_code: "6sd4f6@6cv4",
                // lang: "TH"

            },
                {
                    headers: headers
                }
            ).then(result => {
                // console.log(result+ "Welcome");
                //  console.log(result.data.data.id + "Welcome");

                if (result.data.status == true) {
                    // alert(result.data.data.id)
                    // this.setState({ userID: result.data.data.id});
                    // console.log(result.data.data.id);

                    var userID = JSON.stringify(result.data.data.id);
                    // console.log('B'+userID);

                    // AsyncStorage.setItem('userid', (userID))

                    Alert.alert(
                        "กรุณายืนยันรหัส OTP",
                        "",
                        [
                            { text: 'ตกลง', onPress: () => this.Randomotp() },
                        ],
                        { cancelable: false },
                    );

                    // Alert.alert(
                    //     "เข้าสู่ระบบสำเร็จ",
                    //     "",
                    //     [
                    //         { text: 'ตกลง', onPress: () => (RNRestart.Restart()) },
                    //     ],
                    //     { cancelable: false },
                    // );
                    // AsyncStorage.setItem('citizenID', (this.state.citizenID));


                    // AsyncStorage.setItem('usrtoken', (result.data.result.usrtoken))
                    // AsyncStorage.setItem('email', (this.state.customerEmail))


                    // this.props.navigation.navigate('Home')
                } else {
                    // var str = JSON.stringify(result.data.errors[0].msg._th)
                    Alert.alert(
                        "เข้าสู่ระบบไม่สำเร็จ",
                        "เลขบัตรประชาชนหรือเบอร์โทรศัพท์ไม่ถูกต้อง",
                        [
                            { text: 'ตกลง' },
                        ],
                        { cancelable: false },
                    );
                }
                // console.log(result)
                // alert(JSON.stringify(result.data.statuscode))
                // AsyncStorage.setItem('usrtoken' , (JSON.stringify(result.data.result.usrtoken)))
                // this.props.navigation.navigate('Home')

            }).catch(e => {
                console.log(e);

            });

            //     let password = '7777'
            //     if (this.state.password1 == password) {
            //         axios.post('http://203.113.11.167/api/checkuser', {
            //             email: this.state.customerEmail,
            //             // tel: this.state.phonenumber3,
            //         }).then(async result => {
            //             // alert(this.state.citizen3+' '+this.state.phonenumber3)
            //             if (result.data.status == true) {
            //                 // alert(JSON.stringify(result.data.data))
            //                 let data = result.data.data;
            //                 // AsyncStorage.setItem('user',data )
            //                 await AsyncStorage.setItem('user', (JSON.stringify(data.id)))
            //                 RNRestart.Restart()

            //             } else {

            //                 this.gotoNAMEMAIL();
            //             }
            //         })


            //     } else {
            //         alert('รหัสผ่านไม่ถูกต้อง')
            //     }


        }
    }


    submit1() {
        if (this.state._type = 'login1') {
            this.validate1(this.state.email1)
        }
    }

    validate1 = (text) => {
       // console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (this.state.citizen == null) {

            alert('กรุณาใส่รหัสบัตรประชาชน')

        } else if (this.state.citizen.length != 13) {

            alert('กรุณาใส่รหัสบัตรประชาชนให้ถูกต้อง')

        } else if (this.state.phonenumber == null) {

            alert('กรุณาใส่เบอร์โทรศัพท์')

        } else if (this.state.phonenumber.length != 10) {

            alert('กรุณาใส่เบอร์โทรศัพท์ให้ถูกต้อง')

        } else if (this.state.nameSurname == null) {

            alert('กรุณาใส่ชื่อจริงและนามสุกล')

        } else if ((text) == null) {

            alert('กรุณาใส่อีเมลด้วย')

        } else if (reg.test(text) === false) {

            alert('กรุณาใส่อีเมลให้ถูกต้อง')

        } else {

            Alert.alert(
                'OK', '',
                [
                    { text: 'OK', onPress: () => this.setState({ _type: 'user1' }) },
                ],
                { cancelable: false },
            );

        }

    }



    submit2() {
        if (this.state._type = 'user1') {
            this.validate2(this.state.email2)
        }
    }

    validate2 = (text) => {
      //  console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (this.state.citizen2 == null) {

            alert('ใส่เลขบัตรประชาชนด้วยครับ')

        } else if (this.state.citizen2.length != 13) {

            alert('รหัสบัตรประชาชนไม่ถูกต้องครับ')

        } else if (this.state.phonenumber2 == null) {

            alert('ใส่เบอร์โทรศัพท์ด้วยนะครับ')

        } else if (this.state.phonenumber2.length != 10) {

            alert('เบอร์ไม่ถูกต้องนะครับ')

        } else if ((text) == null) {

            alert('ใส่อีเมล์ด้วยนะครับ')

        } else if (reg.test(text) == false) {

            alert('อีเมลไม่ถูกต้องนะครับ')

        } else {

            Alert.alert(
                'เข้าสู่ระบบสำเร็จ', '',
                [
                    { text: 'OK', onPress: () => this.Login() },
                ],
                { cancelable: false },
            );


        }

    }


    submitFirst() {
        if (this.state._type = 'phoneId') {
            this.validate3(this.state.citizen3)
        }
    }

    validate3() {
        if (this.state.citizen3 == null) {

            alert('กรุณาใส่เลขบัตรประชาชน')

        } else if (this.state.citizen3.length != 13) {

            alert('กรุณาใส่เลขบัตรประชาชนให้ถูกต้อง')

        } else if (this.state.phonenumber3 == null) {

            alert('กรุณาใส่เบอร์โทรศัพท์')

        }
        else if (this.state.phonenumber3.length != 10) {

            alert('กรุณาใส่เบอร์โทรศัพท์ให้ถูกต้อง')

        } else {

            setTimeout(() => {
                this.gotoOTP();
            }, 700);

        }
    }

    submitSecond() {
        if (this.state._type = 'otp') {
            this.validate4(this.state.otp)
        }
    }

    validate4() {
        let otp = '1234'
        if (this.state.otp != otp) {

            alert('รหัส OTP ไม่ถูกต้อง')

        } else {
            // alert('in else')
            axios.post('http://203.113.11.167/api/checkuser', {
                Idcard: this.state.citizen3,
                tel: this.state.phonenumber3,
            }).then(async result => {
                // alert(this.state.citizen3+' '+this.state.phonenumber3)
                if (result.data.status == true) {
                    // alert(JSON.stringify(result.data.data))
                    let data = result.data.data;
                    // AsyncStorage.setItem('user',data )
                    await AsyncStorage.setItem('user', (JSON.stringify(data.id)))

                    RNRestart.Restart()

                } else {

                    this.gotoNAMEMAIL();
                }
            })
        }
    }

    submitThird() {
        if (this.state._type = 'nameEmail') {
            this.validate5(this.state.email3)
        }
    }

    validate5 = (text) => {
      //  console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (this.state.firstName == null) {

            alert('กรุณาใส่ชื่อให้ถูกต้อง')

        } else if (this.state.surname == null) {

            alert('กรุณาใส่นามสกุลให้ถูกต้อง')

        } else if (reg.test(text) === false) {

            alert('กรุณากรอกอีเมลให้ถูกต้อง')

        } else {

            setTimeout(() => {
                this.props.navigation.navigate('checkCustomerService', { data: { name: this.state.firstName, lastname: this.state.surname, tel: this.state.phonenumber3, Idcard: this.state.citizen3, email: this.state.email3 } });
            }, 700);

        }
    }

    // Login() {
    //     // alert('in')
    //     let a = 'login'
    //     AsyncStorage.getItem('login')
    //         .then(data => {
    //             if (data == 'login') {
    //                 AsyncStorage.clear()
    //                     .then(async data => {
    //                         await this.setState({ isModalVisible: false })
    //                     });
    //                 setTimeout(() => {
    //                     RNRestart.Restart();
    //                 }, 700);

    //             } else {
    //                 AsyncStorage.setItem('login', a)
    //                     .then(async result => {
    //                         // alert(JSON.stringify(result))
    //                         await this.setState({ isModalVisible: false })
    //                     })
    //                 setTimeout(() => {
    //                     RNRestart.Restart();
    //                 }, 700);
    //             }

    //         })
    // }
    render() {
        let data = [
            {
                name: 'phoneId'
            },
            {
                name: 'otp'
            },
            {
                name: 'nameEmail'
            }
        ]
        return (

            <View style={{ flex: 1 }}>

                <KeyboardAwareScrollView>
                    {this.state._type == 'phoneId' ?
                        <View style={{ alignSelf: 'center', flex: 1, width: wp('90%'), alignItems: 'center' }}>


                            {/* source={{ uri: 'http://203.113.11.192:3000/easylife/user1.png' }} */}
                            <View>

                                <ImageBackground style={[styles.bnimage]} source={require('../alliconESL/Iconnewsetapp/HomeNewTOTeasylifeAW119.png')} >
                                    <Text style={{ color: 'white', alignSelf: 'center', justifyContent: 'center', marginTop: wp('50%'), fontFamily: 'Prompt-SemiBold', fontSize: wp('8%') }} >ล็อคอินเข้าสู่ระบบ</Text>
                                    <Text style={{ color: 'white', alignSelf: 'center', fontFamily: 'Prompt-SemiBold', fontSize: 11 }} >TOT PUBLIC COMPANY LIMITED</Text>
                                    <Text style={{ color: 'white', alignSelf: 'center', fontFamily: 'Prompt-SemiBold', fontSize: 11 }} >@TOTPUBLIC</Text>

                                </ImageBackground>
                            </View>
                            <View style={{ flexDirection: 'column', alignItems: 'center', }}>
                                {/* <Text style={{fontSize: wp(7), color: '#79b6d2', marginTop: wp(-55)}}>
                                HELLO
                            </Text> */}
                                {/* <Text style={{fontSize: wp(7), color: '#fff', marginTop: wp(-55) }}>
                                Welcome To TOT Easy Life
                            </Text> */}

                            </View>





                            <View style={{
                                width: wp('80%'), height: wp('93%'), backgroundColor: '#FFFFFF',
                                borderRadius: 10, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', flexDirection: 'column', marginTop: hp(-10), marginBottom: wp('10%'),
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 2,
                                },
                                shadowOpacity: 0.25,
                                shadowRadius: 3.84,
                                elevation: 10
                            }}>

                                <Text style={{ justifyContent: 'center', alignSelf: 'center', fontFamily: 'Prompt-SemiBold', fontSize: wp('5%'), color: '#009ACD', marginTop: wp('5%') }}>เข้าสู่ระบบ</Text>
                                <View style={styles.lineStylew3} />
                                <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}
                                    style={{ alignItems: 'center', marginTop: wp('7%'), height: wp('12%'), width: wp('65%'), borderRadius: 30 }}>

                                    <TextInput
                                        maxLength={13}
                                        placeholder={'เลขบัตรประชาชน'}
                                        placeholderTextColor='grey'
                                        keyboardType='numeric'
                                        //paddingLeft= {20}
                                        value={this.state.citizenID}
                                        style={{
                                            fontSize: wp('3.5%'), width: wp('64%'), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp('0.5%'),
                                            height: wp('11%'), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                        }}
                                        onChangeText={(text) => this.setState({ citizenID: text })}
                                    />

                                </LinearGradient>

                                <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}
                                    style={{ alignItems: 'center', marginTop: wp('2.5%'), height: wp('12%'), width: wp('65%'), borderRadius: 30 }}>

                                    <TextInput
                                         maxLength={10}
                                        placeholder={'เบอร์โทรศัพท์มือถือ'}
                                        keyboardType='numeric'
                                        placeholderTextColor='grey'
                                        secureTextEntry={true}
                                        //paddingLeft= {20}
                                        value={this.state.tel}
                                        style={{
                                            fontSize: wp('3.5%'), width: wp('64%'), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp('0.5'),
                                            height: wp('11%'), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                        }}
                                        onChangeText={(text) => this.setState({ tel: text })}
                                    />

                                </LinearGradient>

                                <View style={{ marginTop: wp('4%') }} >

                                    <TouchableOpacity
                                        onPress={() => this.submit()}
                                    // style={styles.login}

                                    >
                                        {/* <Image  source={require('./img/icon-design-09.png')} style={{height:wp(12), width: wp(12)}} /> */}
                                        <LinearGradient style={styles.login} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#4190cc', '#71ecf6']}>
                                            <Text style={{ fontSize: wp('4%'), fontWeight: '400', fontFamily: 'Prompt-Light', color: '#fff' }}> เข้าสู่ระบบ </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => { this.props.navigation.navigate('registerPage') }}
                                    // style={styles.login}
                                    >
                                        {/* <Image  source={require('./img/icon-design-09.png')} style={{height:wp(12), width: wp(12)}} /> */}
                                        <LinearGradient style={styles.login} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}>
                                            <Text style={{ fontSize: wp('4%'), fontWeight: '400', fontFamily: 'Prompt-Light', color: '#fff' }}> สมัครบริการ </Text>
                                        </LinearGradient>
                                    </TouchableOpacity>

                                    
                                </View>
                                <View style={{ flex: 1 }}>

                                    <Modal
                                        isVisible={this.state.isModalVisible}
                                        style={{ alignItems: "center", }}
                                        onSwipeComplete={() => this.toggleModal()}

                                    >
                                        <View >
                                            <View
                                                style={{
                                                    alignItems: "center",
                                                    justifyContent: "center",

                                                }}
                                            >
                                                {/* <Icon
                                        name="times"
                                        size={28}
                                        onPress={this.toggleModal}
                                    /> */}
                                            </View>
                                            <View
                                                style={{
                                                    width: wp(80),
                                                    height: wp(60),
                                                    backgroundColor: "white",

                                                    justifyContent: "center",
                                                    alignItems: "center",
                                                    borderRadius: 10

                                                }}
                                            >
                                                <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}
                                                    style={{ alignItems: 'center', marginTop: wp(2), height: wp(12), width: wp(65), borderRadius: 30 }}>


                                                    <TextInput
                                                        maxLength={6}
                                                        placeholder={'ยืนยัน OTP'}
                                                        keyboardType='number-pad'
                                                        placeholderTextColor='grey'
                                                        //paddingLeft= {20}
                                                        style={{
                                                            fontSize: wp(3.5), width: wp(64), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.5),
                                                            height: wp(11), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                                        }}
                                                        onChangeText={(text) => this.setState({ OTPno: text })}
                                                    />
                                                </LinearGradient>
                                                <View style={{ flexDirection: 'row', marginTop: wp(5), alignSelf: 'center', justifyContent: 'center' }}>

                                                    <TouchableOpacity
                                                        onPress={this.toggleModal2}
                                                    >
                                                        <LinearGradient style={{ width: wp(25), height: wp(10), marginRight: wp(1), marginLeft: wp(1), borderRadius: 30, alignItems: 'center', justifyContent: 'center' }} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}>
                                                            <Text style={{ fontSize: wp(3.5), fontFamily: 'Prompt-Light', color: '#fff' }}> ยกเลิก </Text>
                                                        </LinearGradient>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity
                                                        onPress={this.CheckOtp}
                                                    >
                                                        <LinearGradient style={{ width: wp(25), height: wp(10), marginRight: wp(1), marginLeft: wp(1), borderRadius: 30, alignItems: 'center', justifyContent: 'center' }} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}>
                                                            <Text style={{ fontSize: wp(3.5), fontFamily: 'Prompt-Light', color: '#fff' }}> ยืนยัน  </Text>
                                                        </LinearGradient>
                                                    </TouchableOpacity>
                                                </View>

                                                <TouchableOpacity
                                                    onPress={this.NewRandomotp}
                                                >
                                                    <View style={{ marginTop: wp(5) }}>
                                                        <Text style={{ fontSize: wp(3.5), fontFamily: 'Prompt-Light', color: '#black' }}> รับรหัส OTP ใหม่ </Text>
                                                    </View>
                                                </TouchableOpacity>

                                                {/* <Button
                                            onPress={() => this.toggleModal()}
                                            title="Close"
                                        /> */}
                                            </View>
                                        </View>
                                    </Modal>
                                </View>
                            </View>




                            {/* <View>

                                <TouchableOpacity
                                    //onPress={() => this.props.navigation.navigate('checkCustomerService') }
                                    onPress={() => this.gotoNAMEMAIL()}
                                // style={[styles.login,{marginTop: wp(1)}]}
                                >
                                    <LinearGradient style={[styles.login, { marginTop: wp(2.5), marginBottom: wp(1) }]} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}>
                                        <Text style={{ fontSize: wp(3.5), fontWeight: 'bold', color: '#fff' }}> ไปหน้าหลัก </Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                            </View> */}

                        </View>
                        //****************************************************************************************************************************/
                        : <View>
                            {this.state._type == 'otp' ?
                                <View style={{ alignSelf: 'center', flex: 1, width: wp(90), alignItems: 'center' }}>

                                    <Image style={[styles.bnimage]} source={require('../img/otpHome1.png')} />


                                    <View style={{
                                        width: wp(80), height: wp(55), backgroundColor: '#FFFFFF',
                                        borderRadius: 10, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', flexDirection: 'column', marginTop: hp(-10),
                                        shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84,
                                    }}>

                                        <Text style={{ justifyContent: 'center', alignSelf: 'center', marginTop: wp(2), fontWeight: 'bold', fontSize: wp(4), color: '#009ACD' }}>กรอกรหัส OTP</Text>
                                        <View style={styles.lineStylew3} />
                                        <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}
                                            style={{ alignItems: 'center', marginTop: wp(10), height: wp(12), width: wp(65), borderRadius: 30 }}>
                                            <TextInput
                                                maxLength={13}
                                                placeholder={'รหัส OTP'}
                                                keyboardType='numeric'
                                                //paddingLeft= {20}
                                                value={this.state.otp}
                                                style={{
                                                    fontSize: wp(3.5), width: wp(64), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.5),
                                                    height: wp(11), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                                }}
                                                onChangeText={(text) => this.setState({ otp: text })}
                                            />
                                        </LinearGradient>



                                        <View style={{ marginTop: wp(2.5) }}>
                                            <TouchableOpacity
                                            //onPress={() => this.gotoNAMEMAIL()}
                                            //  style={styles.otpAgain}
                                            >
                                                <LinearGradient style={[styles.otpAgain]} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}>
                                                    <Text style={{ fontSize: wp(3.5), fontWeight: 'bold', color: '#fff' }}> ขอรหัส OTP อีกครั้ง </Text>
                                                </LinearGradient>
                                            </TouchableOpacity>
                                        </View>
                                    </View>



                                    <View>
                                        <TouchableOpacity
                                            onPress={() => this.submitSecond()}

                                        //  style={[styles.login]}
                                        >
                                            <LinearGradient style={[styles.login]} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}>
                                                {/* <Image source={require('./img/icon-design-09.png')} style={{height: wp(12), width:wp(12)}}/> */}
                                                <Text style={{ fontSize: wp(3.5), fontWeight: 'bold', color: '#fff' }}> ยืนยัน </Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => this.gotoPHONEID()}
                                        //  style={[styles.login,{marginTop: wp(1)}]}
                                        >
                                            <LinearGradient style={[styles.login, { marginTop: wp(2.5), marginBottom: wp(1) }]} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}>
                                                {/* <Image source={require('./img/icon-design-08.png')} style={{height: wp(12), width:wp(12)}}/> */}
                                                <Text style={{ fontSize: wp(3.5), fontWeight: 'bold', color: '#fff' }}> ย้อนกลับ </Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                    </View>

                                </View>

                                //****************************************************************************************************************************/
                                : <View style={{ alignSelf: 'center', flex: 1, width: wp(90), alignItems: 'center' }}>


                              
                                    <Image style={[styles.bnimage]} source={require('../img/moredetailHome1.png')} />


                                    <View style={{
                                        width: wp(80), height: wp(65), backgroundColor: '#FFFFFF',
                                        borderRadius: 10, justifyContent: 'center', alignItems: 'center', alignSelf: 'center', flexDirection: 'column', marginTop: hp(-10),
                                        shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84,
                                    }}>

                                        <Text style={{ justifyContent: 'center', alignSelf: 'center', marginTop: wp(2), fontWeight: 'bold', fontSize: wp(4), color: '#009ACD' }}>รายละเอียดเพิ่มเติม</Text>
                                        <View style={styles.lineStylew3} />
                                        <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}
                                            style={{ alignItems: 'center', marginTop: wp(5), height: wp(12), width: wp(65), borderRadius: 30 }}>
                                            <TextInput
                                                maxLength={20}
                                                placeholder={'ชื่อจริง'}
                                                //paddingLeft= {20}
                                                value={this.state.firstName}
                                                style={{
                                                    fontSize: wp(3.5), width: wp(64), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.5),
                                                    height: wp(11), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                                }}
                                                onChangeText={(text) => this.setState({ firstName: text })}
                                            />
                                        </LinearGradient>
                                        <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}
                                            style={{ alignItems: 'center', marginTop: wp(2.5), height: wp(12), width: wp(65), borderRadius: 30 }}>
                                            <TextInput
                                                maxLength={20}
                                                placeholder={'นามสกุล'}
                                                //paddingLeft= {20}
                                                value={this.state.surname}
                                                style={{
                                                    fontSize: wp(3.5), width: wp(64), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.5),
                                                    height: wp(11), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                                }}
                                                onChangeText={(text) => this.setState({ surname: text })}
                                            />
                                        </LinearGradient>


                                        <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}
                                            style={{ alignItems: 'center', marginTop: wp(2.5), height: wp(12), width: wp(65), borderRadius: 30 }}>
                                            <TextInput
                                                maxLength={32}
                                                placeholder={'อีเมล'}
                                                //paddingLeft= {20}
                                                value={this.state.email3}
                                                style={{
                                                    fontSize: wp(3.5), width: wp(64), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.5),
                                                    height: wp(11), backgroundColor: 'rgb(256,256,256)', borderRadius: 30
                                                }}
                                                onChangeText={(text) => this.setState({ email3: text })}
                                            />
                                        </LinearGradient>

                                    </View>





                                    <View>
                                        <TouchableOpacity
                                            onPress={() => this.submitThird()}
                                        //  style={[styles.login]}
                                        >
                                            <LinearGradient style={styles.login} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}>
                                                {/* <Image source={require('./img/icon-design-09.png')} style={{height: wp(12), width: wp(12)}} /> */}
                                                <Text style={{ fontSize: wp(3.5), fontWeight: 'bold', color: '#fff' }}> ยืนยัน </Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <TouchableOpacity
                                            onPress={() => this.gotoOTP()}
                                        //  style={[styles.login,{marginTop: wp(1)}]}
                                        >
                                            <LinearGradient style={[styles.login, { marginTop: wp(2.5), marginBottom: wp(1) }]} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}>
                                                {/* <Image source={require('./img/icon-design-08.png')} style={{height: wp(12), width: wp(12)}}  /> */}
                                                <Text style={{ fontSize: wp(3.5), fontWeight: 'bold', color: '#fff' }}> ย้อนกลับ </Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                    </View>

                                </View>}
                        </View>}
                </KeyboardAwareScrollView>

            </View>

        );
    }
}

const styles = StyleSheet.create({
    bnimage: {
        //borderRadius: 10,
        height: wp('135%'),
        width: wp('101%'),
        alignSelf: 'center',


    },
    fontStyle: {
        fontFamily: 'Prompt-Light'
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 20,
        width: wp(85),
        marginTop: wp(20)
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    buttonSearch: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        padding: 10,
        backgroundColor: '#79cff7',
        width: wp(60),
        marginTop: 40,
        marginBottom: 10,
        //marginLeft:14,
        borderRadius: 30,
        justifyContent: 'center',
        flexDirection: 'column'
    },
    loginE: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        padding: 10,
        backgroundColor: '#fff',
        width: wp(60),
        marginTop: 40,
        marginBottom: 10,
        //marginLeft:14,
        borderRadius: 30,
        justifyContent: 'center',
        flexDirection: 'column',
        borderWidth: 3,
        borderColor: '#25bef8'
    },
    login: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        //padding: 10,
        // backgroundColor: '#8A23FC',//add8e6 ,79b6d2
        height: wp(12),
        width: wp(65),
        marginTop: wp(2),
        // marginBottom: wp(2),
        //marginLeft:14,
        justifyContent: 'center',
        flexDirection: 'column',
        // borderWidth: 1,
        borderRadius: 30,
        // borderColor: '#fff'
    },
    loginModal: {
        //alignSelf: 'flex-start',
        // backgroundColor: '#03a9f4',
        padding: 10,
        backgroundColor: '#79cff7',
        width: wp(60),
        marginTop: 70,
        marginBottom: 10,
        //marginLeft:14,
        borderRadius: 30,
        justifyContent: 'center',
        flexDirection: 'column'
    },
    otpAgain: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        //padding: 10,
        // backgroundColor: '#8A23FC',
        height: wp(12),
        width: wp(65),
        // marginTop: wp(2.5),
        // marginBottom: wp(2.5),
        //marginLeft:14,
        justifyContent: 'center',
        flexDirection: 'column',
        // borderWidth: 1,
        borderRadius: 30,
        // borderColor: '#fff'   //8A23FC / 1D0AFE
    },
    lineStylew3: {
        borderWidth: 1,
        borderColor: '#009ACD',
        //backgroundColor: '#8A23FC',
        marginTop: 10,
        alignSelf: 'center',
        width: wp('25'),

    },
    content: {
        backgroundColor: "white",
        borderRadius: 10,
        borderColor: "rgba(0, 0, 0, 0.1)"
    }
})




// import React, { Component } from 'react';
// import { View, Text, StyleSheet, Alert, SafeAreaView, Image, ScrollView, AsyncStorage } from 'react-native';
// import axios from "axios";
// import base64 from 'react-native-base64';
// import { LoginButton, AccessToken,LoginManager } from 'react-native-fbsdk';
// import DeviceInfo from 'react-native-device-info';
// import {
//     widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
// } from 'react-native-responsive-screen';
// import { TextInput } from 'react-native-paper';
// import { Icon, ThemeConsumer, SocialIcon, Button } from 'react-native-elements'
// import { TouchableOpacity } from 'react-native-gesture-handler';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
// import { RNRestart } from 'react-native-restart'


// export default class Login extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             Username: '',
//             Password: '',
//             Ipaddress: '',
//             test: base64.encode('Some string to encode to base64'),
//             test2: base64.decode('SW4gbXkgZXllcywgaW5kaXNwb3NlZA0KSW4gZGlzZ3Vpc2VzIG5vIG9uZSBrbm93cw0KUklQIEND=='),
//             text: '',
//             text1: '',
//             text2: '',


//         };
//     }



//     componentDidMount() {
//         DeviceInfo.getIpAddress().then(ip => {
//             console.log("IpAddress:" + ip);
//             this.setState({ Ipaddress: ip })
//         });
//     }

//     _Login = () => {
//         const url = "https://deves.toteservice.com/esapi/Getlogin/index";
//         var Usernamebase64 = base64.encode(this.state.Username);
//         var Passwordbase64 = base64.encode(this.state.Password);
//         const headers = {
//             'Content-Type': 'application/json',
//             'Authorization': 'JWT fefege...'
//         }

//         axios.post(url, {
//             username: base64.encode(Usernamebase64),
//             password: base64.encode(Passwordbase64),
//             logfrm: "1",
//             ipaddress: this.state.Ipaddress,
//             auth_contracts: "easy1ife2ew",
//             auth_code: "6sd4g6xv@",
//             lang: "TH"

//         },
//             {
//                 headers: headers
//             }
//         )
//             .then(async result => {
//                 console.log(result)
//                 alert(JSON.stringify(result.data.result.usrtoken + result.data.result.mbrid))

//                 await AsyncStorage.setItem('user', (JSON.stringify(result.data.result.usrtoken)))
//                 // AsyncStorage.setItem('mbrid' , (JSON.stringify(result.data.result.mbrid)))
//                 setTimeout(() => {
//                     RNRestart.Restart();
//                 }, 700);


//             }).catch(e => {
//                 console.log(e);

//             });
//     }



//     _fbAuth() {
//         LoginManager.logInWithReadPermissions(['public_profile']).then(function(result) {
//           if (result.isCancelled) {
//             console.log("Login Cancelled");
//           } else {
//             console.log("Login Success permission granted:" + result.grantedPermissions);
//           }
//         }, function(error) {
//            console.log("some error occurred!!");
//         })
//       }

//     render() {
//         const { isFocused } = this.state;
//         const { onFocus } = this.props;
//         return (

//             <KeyboardAwareScrollView>
//                 <SafeAreaView>
//                     <View style={{ alignItems: "center" }}>

//                         <Image
//                             source={require('../alliconESL/tot-logo-share.jpg')}
//                             style={{ width: wp(65), height: wp(50), }}
//                         />

//                         <View style={{ marginTop: 10 }}>
//                             {/* <TextInput style={styles.textInput1} placeholder="Username:" placeholderTextColor="#555555" onChangeText={(text) => this.setState({ Username: text })}></TextInput>
//                         <View style={styles.lineStylew5} />

//                         <TextInput style={styles.textInput2} placeholder="Password:" placeholderTextColor="#555555" secureTextEntry={true} keyboardType={"default"} onChangeText={(text) => this.setState({ Password: text })}></TextInput>



//                         <View style={styles.lineStylew5} />
//  */}


//                         </View>

//                         <TextInput style={{ width: wp(85), height: wp(13), fontFamily: 'Prompt-Light', fontSize: 'bold' }}
//                             mode='outlined'
//                             placeholder="Ex: toteasylife@tot.co.th"
//                             label='Username'
//                             onFocus={this.handleFocus}
//                             keyboardType={"email-address"}
//                             onChangeText={(text) => this.setState({ Username: text })}
//                             theme={{
//                                 colors: {
//                                     //placeholder: 'white',
//                                     //text: 'black', 
//                                     primary: '#1874CD',
//                                     underlineColor: 'transparent',
//                                     background: 'white',

//                                 }
//                             }}
//                         />

//                         <TextInput style={{ width: wp(85), height: wp(13), marginTop: wp(2), fontFamily: 'Prompt-Light' }}
//                             mode='outlined'
//                             placeholder="At least 8 characters"
//                             secureTextEntry={true}
//                             label='Password'
//                             selectionColor='black'

//                             onChangeText={(text) => this.setState({ Password: text })}
//                             theme={{
//                                 colors: {
//                                     // placeholder: 'white',
//                                     //text: 'black', 
//                                     primary: '#1874CD',
//                                     underlineColor: 'transparent',
//                                     background: 'white'
//                                 }
//                             }}

//                         />

//                         <View style={{ marginTop: 30, marginLeft: 50, marginRight: 50 }}>

//                             <TouchableOpacity
//                                 onPress={this._Login}
//                                 style={[styles.Loginbutton,]}
//                             >
//                                 <Text style={{ fontSize: wp(4), fontWeight: '600', fontFamily: 'Prompt-Light', color: '#555' }}> Log In</Text>
//                             </TouchableOpacity>


//                         </View>


//                         <TouchableOpacity
//                             onPress={() => this.props.navigation.navigate('registerPage')}
//                             style={[styles.Loginbutton,]}
//                         >
//                             <Text style={{ fontSize: wp(4), fontWeight: '600', color: '#555', fontFamily: 'Prompt-Light' }}>สมัคร TOT ID</Text>
//                         </TouchableOpacity>

//                         <View style={{marginTop:wp(10)}}>
//                             <LoginButton

//                                 onLoginFinished={
//                                     (error, result) => {
//                                         if (error) {
//                                             console.log("Login failed with error: " + result.error);
//                                         } else if (result.isCancelled) {
//                                             console.log("Login was cancelled");
//                                         } else {
//                                             AccessToken.getCurrentAccessToken().then(
//                                                 (data) => {
//                                                     console.log(data.accessToken.toString())
//                                                 }
//                                             )
//                                         }
//                                     }
//                                 }
//                                 onLogoutFinished={() => console.log("User logged out")} />
//                         </View>
//                     </View>
//                 </SafeAreaView>
//             </KeyboardAwareScrollView>
//         );
//     }
// }
// const styles = StyleSheet.create(
//     {
//         textInput1: {
//             color: 'black',
//             backgroundColor: 'white',
//             borderColor: '#126635',
//             // borderWidth:1,
//             borderRadius: 10,
//             height: 40,
//             width: 320,

//             fontFamily: 'Prompt-Light'
//         },
//         textInput2: {
//             color: 'black',
//             backgroundColor: 'white',
//             borderColor: '#126635',
//             // borderWidth:1,
//             borderRadius: 10,
//             height: 40,
//             width: 320,
//             marginTop: 3,
//             fontFamily: 'Prompt-Light'
//         },

//         lineStylew5: {
//             borderWidth: 0.5,
//             borderColor: '#DCDCDC',
//             backgroundColor: '#9B9B9B',
//             marginTop: -3,
//             alignSelf: 'center',
//             width: wp(85),

//         },
//         Loginbutton: {
//             alignItems: 'center',
//             // backgroundColor: '#03a9f4',
//             //padding: 10,
//             alignSelf: 'center',
//             backgroundColor: '#fff',
//             height: wp(12),
//             width: wp(65),
//             marginTop: wp(2.5),
//             //marginBottom: 10,
//             //marginLeft:14,
//             justifyContent: 'center',
//             flexDirection: 'column',
//             borderWidth: 2,
//             borderRadius: 30,
//             borderColor: '#1C86EE'
//             // borderColor: '#c8cdd1'
//         }
//     }
// )

