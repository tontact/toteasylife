


import React, { Component } from 'react';
import { View, Text, Button, Alert, StyleSheet, TextInput, Image, TouchableOpacity, AsyncStorage } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';
import {
    widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';

import { Icon, ThemeConsumer, } from 'react-native-elements'

import RNPickerSelect from 'react-native-picker-select';
import { SearchBar } from 'react-native-elements';
import { ScrollView, } from 'react-native-gesture-handler';
// import Swiper from 'react-native-swiper';
import SwiperFlatList from 'react-native-swiper-flatlist';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';

import ImageSelecter from 'react-native-image-picker';

import axios from 'axios';
import base64 from 'react-native-base64';
import DeviceInfo from 'react-native-device-info';
import RNRestart from "react-native-restart";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import Loading from '../loading'
export default class editProfile extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        imageSource: null,
        Province: [],
        District: [],
        Subdistrict: [],
        getvillage: [],
        data: [],
        data1: [],
        userid: 0,
        checkbill: [],
        m1: [],
        m2: [],
        bano: [],
        svno: [],
        sum: 0,
        Ipaddress: '',
        mbrid: '',
        usrtoken: '',
        password: '',
        oldpassword: '',
        newpassword: '',
        baid: '',
        Firstname: '',
        Firstname1: '',
        Lastname: '',
        Lastname1: '',
        Mobileno: '',
        Mobileno1: '',
        Email: '',
        Email1: '',
        Personalid: '',
        updatetype: '',
        visibleModal: false,
        id: '',
        Address: '',
        Postcode: '',

        UserImage: null,

        userImage: null,

        loading: true

    };


    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        // const params = navigation.getParam('checklogin')
        // this.state.user
        // alert(params)
        return {
            title: 'ข้อมูลส่วนตัว',

            headerLeft: (
                <TouchableOpacity style={{ marginLeft: wp(3), }} >
                    <Ionicons name='ios-close' size={40} onPress={() => navigation.goBack()} />

                </TouchableOpacity>
            ),
            // headerRight: (
            //   <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>

            //   </HeaderButtons>
            // ),
            // //  headerTitle: <Logo/>,

            headerStyle: {
                backgroundColor: '#F9F9F9'
            },
            headerTintColor: '#000',
            headerTitleStyle: {
                // fontFamily: "Prompt-Light",
                textAlign: 'center',
                flex: 1,
                fontFamily: "Prompt-Light", color: "#000", fontSize: 20, fontWeight: '500'

            },
        }
    };





  



    UpdateType() {
        if (this.state.newpassword == "") {
            this.UpdateProfile1()
        } else {
            this.UpdateProfile2()
        }
    }



    success = async () => {
        // this.setState({ oldpassword: this.state.newpassword })
        await this.getProfile()
        await this.setState({ visibleModal: null })
        this.Restart()

    }


    success1 = async () => {
        // this.setState({ oldpassword: this.state.newpassword })
 
        RNRestart.Restart();

    }
    // Restart(){
    //     setTimeout = () => {
    //         RNRestart.Restart()

    //     }, 3000
    // }

    PrepareforEdit() {
        if (this.state.Firstname != '') {
            this.Editprofile()
        }
    }

    Editprofile() {
        if (this.state.Firstname1.length == '') {
            Alert.alert('กรุณากรอกชื่อจริง')
        } else if (this.state.Lastname1.length == '') {
            Alert.alert('กรุณากรอกนามสกุล')
        } else if (this.state.Mobileno1.length == '') {
            Alert.alert('กรุณากรอกเบอร์โทรศัพท์')
        } else if (this, this.state.Mobileno1.length != 10) {
            Alert.alert('กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง')
        } else {
            const url = "http://203.113.11.167/api/editprofile";

            axios.post(url, {
                id: this.state.id,
                name: this.state.Firstname1,
                lastname: this.state.Lastname1,
                tel: this.state.Mobileno1,
                email: this.state.Email1,
                userImage: this.state.UserImage,
                address: this.state.Address,
                province: this.state.Province,
                district: this.state.District,
                subdistrict: this.state.Subdistrict,
                postcode: this.state.Postcode

            },

            ).then(result => {
                Alert.alert(
                    "ยืนยันการแก้ไขโปรไฟล์",
                    "",

                    [
                            { text: 'ยืนยัน', onPress: () => this.success() },
                            { text: "ยกเลิก" }
                    ],
                    { cancelable: false },
            );
                // if (result.data.statuscode == 200) {
                //     // alert(JSON.stringify(result.data.statuscode))

                //     Alert.alert(
                //         "เปลี่ยนรหัสผ่านสำเร็จ",
                //         "",

                //         [
                //             { text: 'ตกลง', onPress: () => this.success() },
                //         ],
                //         { cancelable: false },
                //     );
                //     console.log(result.data);

                //     // this.props.navigation.navigate('Home')
                // } else {
                //     console.log(this.state.password);
                //     Alert.alert(
                //         "เปลี่ยนรหัสผ่านไม่สำเร็จ",
                //         "กรุณาใส่รหัสผ่านเดิมให้ถูกต้อง",
                //         [
                //             { text: 'ตกลง', },
                //         ],
                //         { cancelable: false },
                //     );

                // }
            }).catch(e => {
                console.log(e);

            });
        }
    }



    getData1() {
        let url = "http://npcrapi.netpracharat.com/api/getallprovince";
        axios.get(url).then(data => {
            // alert(JSON.stringify(data.data.data))
            this.setState({
                getprovince: data.data.data
            });
        });
    }

    getData2(item1, item2, index) {
        if (item1 == "province") {
            this.setState({ province: item2 });
            axios
                .get(
                    "http://npcrapi.netpracharat.com/api/getallprovince2?type=province&province=" + item2
                )
                .then(data => {
                    // alert(JSON.stringify(data.data) + "id:" + item2);
                    this.setState({
                        district: "",
                        subdistrict: "",
                        village: "",
                        getdistrict: data.data.data.map((i, index) => {
                            return { value: i.name_th, label: i.name_th };
                        })
                    });
                });
        } else if (item1 == "district") {
            this.setState({ district: item2 });
            // alert(item2)
            // this.setState({ district:this.state.getdistrict[index].name_th,})
            axios
                .get(
                    "http://npcrapi.netpracharat.com/api/getallprovince2?type=district&province=" +
                    this.state.province + '&district=' + item2
                )
                .then(data => {
                    // alert(JSON.stringify(data.data) + "id:" + item2);
                    this.setState({
                        subdistrict: "",
                        village: "",
                        getsubdistrict: data.data.data.map((i, index) => {
                            return { value: i.name_th, label: i.name_th };
                        })
                    });
                });
        } else if (item1 == "sub_district") {
            this.setState({
                subdistrict: item2
            });
            // alert( JSON.stringify(this.state.getsubdistrict))
            // this.setState({subdistrict: this.state.getsubdistrict[index].name_th})
            axios
                .get(
                    "http://npcrapi.netpracharat.com/api/checkprovince?province=" +
                    this.state.province +
                    "&district=" +
                    this.state.district +
                    "&subdistrict=" +
                    item2
                )
                .then(data => {
                    // alert(data.data.data.length)
                    let dataCount = data.data.data.length;
                    this.villageCount = dataCount;
                    // alert(dataCount)
                    let village = data.data.data.concat({ village: "อื่นๆ" });
                    this.setState({ village: "" });
                    if (dataCount != 0) {
                        this.setState({
                            getvillage: village.map((i, index) => {
                                return { value: i.village, label: i.village };
                            })
                        });
                    } else {
                        this.setState({ village: "อื่นๆ" });
                    }
                });
        } else if (item1 == 'village') {
            this.setState({ village: item2 })
        }
    }

    componentDidMount = async () => {
        DeviceInfo.getIpAddress().then(ip => {
            console.log("IpAddress:" + ip);
            this.setState({ Ipaddress: ip })
        });
        var mbrid = await AsyncStorage.getItem('mbrid');
        var usrtoken = await AsyncStorage.getItem('usrtoken');
        var email = await AsyncStorage.getItem('email');
        var id = await AsyncStorage.getItem('userid');
        // var password = await AsyncStorage.getItem('passwordfilled');
        console.log(usrtoken);
        this.setState({ id: id })
        this.setState({ mbrid: mbrid })
        this.setState({ usrtoken: usrtoken })
        this.setState({ email: email })
        // this.setState({ password: password })
        // console.log(this.state.password);
        console.log(this.state.password);



        this.getProfile();
        this.getData1();


    }



    // componentDidMount() {
    //     this.getData1();
    //     this._getUser()
    // }

    SelectCameraRoll = () => {
        //   ImagePicker.openPicker({
        //     width: 300,
        //     height: 300,
        //     cropping: true,
        //     includeBase64: true,
        //     includeExif: true,
        // }).then(image => {
        //     console.log('received base64 image');
        //     this.setState({
        //         imageSource: {uri: 'data:image/jpeg;base64,'+image.data}
        //     });
        // })
        const options = {
            // mediaType:'photo',
            quality: 0.5,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };
        ImageSelecter.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                // alert('response image ....')
                // const source = { uri: data:${response.data.mime};base64, + response.data };
                // let source = { uri: response.uri };
                // console.log(response);
                // alert(JSON.stringify(response.data))
                this.state.loading = true


                axios.post(
                    "http://203.113.11.167/api/saveimage",
                    {
                        id: this.state.id,
                        userImage: response.data
                    }
                ).then(result => {

                    Alert.alert(
                        "เปลี่ยนรูปโปร์ไฟล์สำเร็จ",
                        "",

                        [
                            { text: 'ตกลง', onPress: () => this.success1() },
                        ],
                        { cancelable: false },
                    );
                    
                    this.state.loading = true
                    this.setState({
                        UserImage: result.data.userImage
                    })

                    // alert(JSON.stringify(this.state.UserImage));

                    // this.setState({
                    //     UserImage: 'data:image/jpeg;base64,' + response.data,
                    // });
                    // Alert.alert("เสร็จสิ้น");

                });

                // this.setState({
                //     UserImage: {
                //         uri: 'data:image/jpeg;base64,' + response.data,
                //     }
                // });
                // alert();
                //this.uploadPhoto(this.state.citizen);
            }
        });
    };


    getProfile() {
        const url = "http://203.113.11.167/api/myuser/" + this.state.id;

        console.log(url);
        // const headers = {
        //   'Content-Type': 'application/json',
        //   'Authorization': 'JWT fefege...'
        // }
        axios.get(url)
            .then(result => {
                // var userprofile = JSON.stringify(result);
                // console.log(userprofile);
                console.log(result);
                console.log(JSON.stringify(result.data.data.id) + 'edit');

                this.setState({
                    data1: result.data.data,
                    Firstname: result.data.data.name,
                    Firstname1: result.data.data.name,
                    Lastname: result.data.data.lastname,
                    Lastname1: result.data.data.lastname,
                    Mobileno: result.data.data.tel,
                    Mobileno1: result.data.data.tel,
                    Email: result.data.data.email,
                    Email1: result.data.data.email,
                    Personalid: result.data.data.IDcard,
                    Address: result.data.data.address,
                    Province: result.data.data.province,
                    District: result.data.data.district,
                    Subdistrict: result.data.data.subdistrict,
                    Postcode: result.data.data.postcode,
                    UserImage: result.data.data.userImage

                })
                // Alert.alert(JSON.stringify(result.data.data.userImage));
                // console.log(UserImage + 'editImage');
            })
            .catch(err => {
                alert(JSON.stringify(error));
            })
    }


  
  Logout() {

    Alert.alert(
      "ยืนยันการออกจากระบบ",
      "",
      [
        {
          text: "ยืนยัน",
          onPress: () => AsyncStorage.clear().then(RNRestart.Restart())
        },
        { text: "ยกเลิก" }
      ],
      { cancelable: false }
    );
  }






    toggleModal() {
        this.setState({ visibleModal: ! false });
    };

    renderViewProfile() {
        this.setState({ visibleModal: false })
        this.props.navigation.navigate('account')
    }


    renderEditProfile() {
        this.setState({ visibleModal: false })
        this.props.navigation.navigate('editProfilePage')
    }




    renderModalContent = () => (


        <View style={styles.content}>

            {/* <Text onPress={() => this.renderViewProfile()} style={styles.contentTitle1}>View profile</Text>
           
            <Text onPress={() => this.renderEditProfile()} style={styles.contentTitle2}>Edit profile</Text>
           
           
            /> */}

            <View style={{ shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, }}>



                <LinearGradient start={{ x: 1.5, y: 0 }} end={{ x: 0, y: 0 }} colors={['#3ad1ff', '#117ffe']} style={{ width: wp(90), height: wp(12), borderTopRightRadius: 10, borderTopLeftRadius: 10, marginTop: wp(5) }}>


                    <Text style={styles.buttonText}>
                        แก้ไขข้อมูลส่วนตัว
                        </Text>

                    <View style={{ alignSelf: 'flex-end', marginTop: wp('-11%') }}>
                        <TouchableOpacity onPress={() => this.toggleModal()} style={{ marginRight: wp('5%'), }} >
                            <Ionicons
                                name='ios-close' size={wp('10%')} color='white' />
                        </TouchableOpacity>
                    </View>



                </LinearGradient>
            </View>

            <View style={{
                width: wp(90), height: wp(140), backgroundColor: '#FFF', borderBottomRightRadius: 10, borderBottomLeftRadius: 10, justifyContent: 'center', alignSelf: 'center', flexDirection: 'column'
                , shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, marginTop: wp(0),
            }}>
                <KeyboardAwareScrollView style={{}} >
                    <View style={{ marginLeft: wp(8), flexDirection: 'row', marginTop: wp(3) }}>
                        <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#3d90ff', marginTop: wp(1.5), marginRight: wp(2) }} />
                        <View style={{ flexDirection: 'column', }} >
                            <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, color: '#3d90ff', fontWeight: '500' }}>ชื่อ :</Text>
                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
                                style={{ alignItems: 'center', marginTop: wp(2), height: wp(8), width: wp(75), borderRadius: 30, marginLeft: wp(-4) }}>

                                <TextInput
                                    // maxLength={30}
                                    placeholder="ชื่อจริง"
                                    placeholderTextColor='gray'
                                    //paddingLeft= {20}
                                    value={this.state.Firstname1}
                                    style={{
                                        fontSize: 12, width: wp(74), textAlign: 'center', fontFamily: 'Prompt-Light', marginTop: wp('0.4%'),
                                        height: wp(7), backgroundColor: '#fff', borderRadius: 30, borderColor: "gray",
                                    }}
                                    onChangeText={(value) => this.setState({ Firstname1: value })}
                                />

                            </LinearGradient>

                        </View>
                    </View>
                    {/* <View style={styles.lineStylew5} /> */}

                    <View style={{ marginTop: wp(3), marginLeft: wp(8), flexDirection: 'row' }}>
                        <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#3d90ff', marginTop: wp(1.5), marginRight: wp(2) }} />
                        <View style={{ flexDirection: 'column' }} >
                            <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, color: '#3d90ff', fontWeight: '500' }}>นามสกุล :</Text>

                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
                                style={{ alignItems: 'center', marginTop: wp(2), height: wp(8), width: wp(75), borderRadius: 30, marginLeft: wp(-4) }}>

                                <TextInput
                                    // maxLength={30}
                                    placeholder="นามสกุล"
                                    placeholderTextColor='gray'
                                    //paddingLeft= {20}
                                    value={this.state.Lastname1}
                                    style={{
                                        fontSize: 12, width: wp(74), textAlign: 'center', fontFamily: 'Prompt-Light', marginTop: wp('0.4%'),
                                        height: wp(7), backgroundColor: '#FFF', borderRadius: 30, borderColor: "gray",
                                    }}
                                    onChangeText={(value) => this.setState({ Lastname1: value })}
                                />

                            </LinearGradient>

                        </View>
                    </View>

                    {/* <View style={styles.lineStylew5} /> */}

                    {/* 
                    <View style={{ marginLeft: wp(8), flexDirection: 'row', marginTop: wp(3) }}>
                        <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#1C86EE', marginTop: wp(1.5), marginRight: wp(2) }} />
                        <View style={{ flexDirection: 'column' }} >
                            <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, color: '#1C86EE', fontWeight: '500' }}>รหัสลูกค้า: </Text>
                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}
                            style={{ alignItems: 'center', marginTop: wp(2), height: wp(8), width: wp(75), borderRadius: 30, marginLeft: wp(-4) }}>

                            <TextInput
                                maxLength={13}
                                placeholder={'แก้ไชรหัสลูกค้า'}
                                placeholderTextColor='gray'
                                //paddingLeft= {20}
                                keyboardType='numeric'
                                value={this.state.customerID}
                                style={{
                                    fontSize: 12, width: wp(75), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.5),
                                    height: wp(7), backgroundColor: '#FFF', borderRadius: 6, borderColor: "gray", borderWidth: 0.5, marginLeft: wp(-4)
                                }}
                                onChangeText={(text) => this.setState({ customerID: text })}
                            />

                            </LinearGradient>

                        </View>
                    </View> */}

                    {/* <View style={styles.lineStylew5} /> */}

                    {/* <View style={{ marginLeft: wp(8), flexDirection: 'row', marginTop: wp(3) }}>
                        <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#1C86EE', marginTop: wp(1.5), marginRight: wp(2) }} />
                        <View style={{ flexDirection: 'column' }} >
                            <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, color: '#1C86EE', fontWeight: '500' }}>หมายเลชบริการ:</Text>
                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}
                            style={{ alignItems: 'center', marginTop: wp(2), height: wp(8), width: wp(75), borderRadius: 30, marginLeft: wp(-4) }}>

                            <TextInput
                                maxLength={13}
                                placeholder={'แก้ไขหมายเลขบริการ'}
                                placeholderTextColor='gray'
                                //paddingLeft= {20}
                                keyboardType='default'
                                value={this.state.ba}
                                style={{
                                    fontSize: 12, width: wp(75), textAlign: 'center', fontFamily: 'Prompt-Light', margin: wp(0.5),
                                    height: wp(7), backgroundColor: '#FFF', borderRadius: 6, borderColor: "gray", borderWidth: 0.5, marginLeft: wp(-4)
                                }}
                                onChangeText={(text) => this.setState({ ba: text })}
                            />

                            </LinearGradient>

                        </View>
                    </View>
                   */}
                    {/* <View style={styles.lineStylew5} /> */}

                    <View style={{ marginLeft: wp(8), flexDirection: 'row', marginTop: wp(3) }}>
                        <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#3d90ff', marginTop: wp(1.5), marginRight: wp(2) }} />
                        <View style={{ flexDirection: 'column' }} >
                            <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, color: '#3d90ff', fontWeight: '500' }}>โทรศัพท์ :</Text>
                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
                                style={{ alignItems: 'center', marginTop: wp(2), height: wp(8), width: wp(75), borderRadius: 30, marginLeft: wp(-4) }}>

                                <TextInput
                                    maxLength={10}
                                    placeholder="เบอร์โทรศัพท์มือถือ"
                                    placeholderTextColor='gray'
                                    //paddingLeft= {20}
                                    keyboardType='numeric'
                                    value={this.state.Mobileno1}
                                    style={{
                                        fontSize: 12, width: wp(74), textAlign: 'center', fontFamily: 'Prompt-Light', marginTop: wp('0.4%'),
                                        height: wp(7), backgroundColor: '#FFF', borderRadius: 30, borderColor: "gray",
                                    }}
                                    onChangeText={(text) => this.setState({ Mobileno1: text })}
                                />

                            </LinearGradient>

                        </View>
                    </View>


                    <View style={{ marginLeft: wp(8), flexDirection: 'row', marginTop: wp(3) }}>
                        <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#3d90ff', marginTop: wp(1.5), marginRight: wp(2) }} />
                        <View style={{ flexDirection: 'column' }} >
                            <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, color: '#3d90ff', fontWeight: '500' }}>อีเมล :</Text>
                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
                                style={{ alignItems: 'center', marginTop: wp(2), height: wp(8), width: wp(75), borderRadius: 30, marginLeft: wp(-4) }}>

                                <TextInput
                                    // maxLength={30}
                                    placeholder={'อีเมล'}
                                    placeholderTextColor='gray'
                                    //paddingLeft= {20}
                                    keyboardType='email-address'
                                    value={this.state.Email1}
                                    style={{
                                        fontSize: 12, width: wp(74), textAlign: 'center', fontFamily: 'Prompt-Light', marginTop: wp('0.4%'),
                                        height: wp(7), backgroundColor: '#FFF', borderRadius: 30, borderColor: "gray",
                                    }}
                                    onChangeText={(text) => this.setState({ Email1: text })}
                                />

                            </LinearGradient>

                        </View>
                    </View>

                    <View style={{ marginLeft: wp(8), flexDirection: 'row', marginTop: wp(3) }}>
                        <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#3d90ff', marginTop: wp(1.5), marginRight: wp(2) }} />
                        <View style={{ flexDirection: 'column' }} >
                            <Text style={{ fontFamily: 'Prompt-Light', fontSize: 12, color: '#3d90ff', fontWeight: '500' }}>ที่อยู่ :</Text>
                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
                                style={{ alignItems: 'center', marginTop: wp(2), height: wp(8), width: wp(75), borderRadius: 30, marginLeft: wp(-4) }}>

                                <TextInput
                                    // maxLength={13}
                                    placeholder={'บ้านเลขที่'}
                                    placeholderTextColor='gray'
                                    //paddingLeft= {20}
                                    keyboardType='default'
                                    value={this.state.Address}
                                    style={{
                                        fontSize: 12, width: wp(74), textAlign: 'center', fontFamily: 'Prompt-Light', marginTop: wp('0.4%'),
                                        height: wp(7), backgroundColor: '#FFF', borderRadius: 30, borderColor: "gray",
                                    }}
                                    onChangeText={(text) => this.setState({ Address: text })}
                                />

                            </LinearGradient>

                        </View>
                    </View>


                    <View style={{ marginLeft: wp('7.65%'), flexDirection: 'row', marginTop: wp('1%') }}>

                        <View style={{ flexDirection: 'column' }} >

                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
                                style={{ alignItems: 'center', marginTop: wp(2), height: wp(8), width: wp(75), borderRadius: 30, }}>

                                <TextInput
                                    // maxLength={13}
                                    placeholder="ตำบล / แขวง"
                                    placeholderTextColor='gray'

                                    //paddingLeft= {20}
                                    keyboardType='default'
                                    value={this.state.Subdistrict}
                                    style={{
                                        fontSize: 12, width: wp(74), textAlign: 'center', fontFamily: 'Prompt-Light', marginTop: wp('0.4%'),
                                        height: wp(7), backgroundColor: '#FFF', borderRadius: 30, borderColor: "gray",
                                    }}
                                    onChangeText={(text) => this.setState({ Subdistrict: text })}
                                />

                            </LinearGradient>

                        </View>
                    </View>

                    <View style={{ marginLeft: wp('7.65%'), flexDirection: 'row', marginTop: wp('1%') }}>

                        <View style={{ flexDirection: 'column' }} >

                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
                                style={{ alignItems: 'center', marginTop: wp(2), height: wp(8), width: wp(75), borderRadius: 30, }}>

                                <TextInput
                                    // maxLength={13}
                                    placeholder="อำเภอ / เขต"
                                    placeholderTextColor='gray'

                                    //paddingLeft= {20}
                                    keyboardType='default'
                                    value={this.state.District}
                                    style={{
                                        fontSize: 12, width: wp(74), textAlign: 'center', fontFamily: 'Prompt-Light', marginTop: wp('0.4%'),
                                        height: wp(7), backgroundColor: '#FFF', borderRadius: 30, borderColor: "gray",
                                    }}
                                    onChangeText={(text) => this.setState({ District: text })}
                                />

                            </LinearGradient>

                        </View>
                    </View>




                    <View style={{ marginLeft: wp('7.65%'), flexDirection: 'row', marginTop: wp('1%') }}>

                        <View style={{ flexDirection: 'column' }} >

                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
                                style={{ alignItems: 'center', marginTop: wp(2), height: wp(8), width: wp(75), borderRadius: 30, }}>

                                <TextInput
                                    maxLength={13}
                                    placeholder="จังหวัด"
                                    placeholderTextColor='gray'

                                    //paddingLeft= {20}
                                    keyboardType='default'
                                    value={this.state.Province}
                                    style={{
                                        fontSize: 12, width: wp(74), textAlign: 'center', fontFamily: 'Prompt-Light', marginTop: wp('0.4%'),
                                        height: wp(7), backgroundColor: '#FFF', borderRadius: 30, borderColor: "gray",
                                    }}
                                    onChangeText={(text) => this.setState({ Province: text })}
                                />

                            </LinearGradient>

                        </View>
                    </View>

                    <View style={{ marginLeft: wp('7.65%'), flexDirection: 'row', marginTop: wp('1%'), marginBottom: wp(5) }}>

                        <View style={{ flexDirection: 'column' }} >

                            <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#117ffe', '#3ad1ff']}
                                style={{ alignItems: 'center', marginTop: wp(2), height: wp(8), width: wp(75), borderRadius: 30, }}>

                                <TextInput
                                    maxLength={13}
                                    placeholder="รหัสไปรษณย์"
                                    placeholderTextColor='gray'

                                    //paddingLeft= {20}
                                    keyboardType='default'
                                    value={this.state.Postcode}
                                    style={{
                                        fontSize: 12, width: wp(74), textAlign: 'center', fontFamily: 'Prompt-Light', marginTop: wp('0.4%'),
                                        height: wp(7), backgroundColor: '#FFF', borderRadius: 30, borderColor: "gray",
                                    }}
                                    onChangeText={(text) => this.setState({ Postcode: text })}
                                />

                            </LinearGradient>

                        </View>
                    </View>




                </KeyboardAwareScrollView>



            </View>


            <TouchableOpacity
                    onPress={() => this.Editprofile()}
                    // style={styles.login}
                    style={{ justifyContent: 'flex-end', alignSelf: 'center', marginTop: wp('12%'), marginBottom: wp('1%') }}
                >
                    {/* <Image  source={require('./img/icon-design-09.png')} style={{height:wp(12), width: wp(12)}} /> */}
                    <LinearGradient style={styles.submit} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#3ad1ff', '#117ffe',]}>
                        <Text style={{ fontSize: wp('4%'), fontWeight: '400', fontFamily: 'Prompt-Light', color: '#fff' }}> บันทึก</Text>
                    </LinearGradient>
                </TouchableOpacity>

            {/* <Button
                onPress={() => this.Editprofile()}
                containerStyle={{ width: wp(20), fontFamily: 'Prompt-Light' }}
                titleStyle={{ fontFamily: 'Prompt-Light' }}
                type="outline"
                title="บันทึก" /> */}


        </View>
    );
    

    render() {

        return (


            <ScrollView
                showsVerticalScrollIndicator={false}
            >
                <View style={{ backgroundColor: 'white' }}>
                    <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>



                        <View style={{ marginTop: wp('5%'), alignSelf: 'center' }} >
                            <View style={{ width: wp('45%'), height: wp('45%'), borderRadius: wp('45%') / 2, borderColor: '#008cff', borderWidth: wp(0.7), }}>
                                {

                                    this.state.UserImage == null ?
                                        <Image
                                            source={require('../alliconESL/HomeIcons/HomeTOTeasylifeAW111.png')}
                                            style={{
                                                width: wp('41%'), height: wp('41%'), borderRadius: wp('41%') / 2, alignSelf: 'center', marginTop: wp('1.3%')

                                            }}
                                        /> :

                                        <Image
                                            source={{ uri: 'http://203.113.11.167/uploads/imagesaveuser/' + this.state.UserImage }}
                                            style={{
                                                width: wp('41%'), height: wp('41%'), borderRadius: wp('41%') / 2, alignSelf: 'center', marginTop: wp('1.3%')

                                            }}
                                        />


                                }
                            </View>

                            <View style={{ marginTop: wp('2%'), flexDirection: 'row', alignSelf: 'center', }} >
                                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp('4%'), color: '#3d90ff', fontWeight: '500' }}>เปลี่ยนรูปภาพโปร์ไฟล์</Text>

                                <TouchableOpacity
                                    style={{ width: wp('11%'), height: wp('11%'), borderRadius: wp('11%') / wp('2%'), marginTop: wp('-2.7%'), marginLeft: wp('2%'), }}
                                    onPress={() => this.SelectCameraRoll()}>
                                    <Image
                                        source={require('../ICON_Png/กรอบเท่ากัน/icondesign-11.png')}
                                        style={{ width: 40, height: 40, borderRadius: 40 / 2, }}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>


                    </View>

                    {/* <View style={{
                    width: wp(90), height: wp(10), backgroundColor: '#F5F5F5', borderTopLeftRadius: 10, borderTopRightRadius: 10, justifyContent: 'center', alignSelf: 'center', flexDirection: 'column'
                    , shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, borderTopColor: '#DCDCDC', marginTop: wp(38)
                }}>

                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: 16 ,fontWeight:400, marginTop: wp(1.7), alignSelf:'center' }}>ข้อมูลส่วนตัว</Text>
                </View> */}




                </View>



                <View style={{ marginTop: wp(5), marginLeft: wp(12), flexDirection: 'row' }}>
                    <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#3d90ff', marginTop: wp(1.8), marginRight: wp(2) }} />
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp('3.5%'), color: '#3d90ff', fontWeight: '500' }}>ชื่อ - นามสกุล : </Text>
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp('3.5%'), marginLeft: wp('1%') }} >{this.state.Firstname1} {this.state.Lastname1}</Text>

                </View>
                <View style={styles.lineStylew5} />

                <View style={{ marginLeft: wp(12), flexDirection: 'row', marginTop: wp(3) }}>
                    <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#3d90ff', marginTop: wp(1.8), marginRight: wp(2) }} />
                    <Text style={{ fontFamily: 'Prompt-Light', fontWeight: '500', fontSize: wp('3.5%'), color: '#1C86EE', letterSpacing: 0.35 }}>บัตรประชาชน :</Text>
                    {/* <Text numberOfLines={3} style={{ fontFamily: 'Prompt-Light', width:wp(68), fontSize:12}} > 22 ประชาอุทิศ 75 แยก 5 ถนนประชาอุทิศ แขวงทุ่งครุ เขตทุ่งครุ กรุงเทพมหานคร 10140</Text> */}
                    <Text numberOfLines={2} style={{ fontFamily: 'Prompt-Light', width: wp(55), fontSize: wp('3.5%'), marginLeft: wp(2.5), }}>{this.state.Personalid}</Text>
                </View>
                <View style={styles.lineStylew5} />

                <View style={{ marginLeft: wp(12), flexDirection: 'row', marginTop: wp(3) }}>
                    <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#3d90ff', marginTop: wp(1.8), marginRight: wp(2) }} />
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp('3.5%'), color: '#3d90ff', fontWeight: '500' }}>โทรศัพท์ : </Text>
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp('3.5%'), marginLeft: wp('9.5%') }} >{this.state.Mobileno1}</Text>

                </View>
                <View style={styles.lineStylew5} />

                <View style={{ marginLeft: wp(12), flexDirection: 'row', marginTop: wp(3) }}>
                    <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#3d90ff', marginTop: wp(1.8), marginRight: wp(2) }} />
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp('3.5%'), color: '#3d90ff', fontWeight: '500' }}>อีเมล :</Text>
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp('3.5%'), marginLeft: wp('15.5%') }} >{this.state.Email1}</Text>

                </View>
                <View style={styles.lineStylew5} />

                <View style={{ marginLeft: wp(12), flexDirection: 'row', marginTop: wp(3) }}>
                    <View style={{ width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: '#3d90ff', marginTop: wp(1.8), marginRight: wp(2) }} />
                    <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp('3.5%'), color: '#3d90ff', fontWeight: '500' }}>ที่อยู่ :</Text>
                    <Text numberOfLines={3} style={{ fontFamily: 'Prompt-Light', fontSize: wp('3.5%'), marginLeft: wp('16.5%'), width: wp(48), }} >{this.state.Address} {this.state.Subdistrict} {this.state.District} {this.state.Province} {this.state.Postcode} </Text>

                </View>
                <View style={styles.lineStylew5} />






                {/* <TouchableOpacity
                    style={{ marginRight: wp('7%'), }}
                    onPress={() => this.setState({ visibleModal: 'default' })}>
                    <Image style={{ width: wp(11), height: wp(5), }} source={require('../alliconESL/HomeIcons/HomeIconsAW141.png')} />
                </TouchableOpacity> */}



                <TouchableOpacity
                    onPress={() => this.setState({ visibleModal: 'default' })}
                    // style={styles.login}
                    style={{ justifyContent: 'flex-end', alignSelf: 'center', marginTop: wp('8%'), marginBottom: wp('1%') }}
                >
                    {/* <Image  source={require('./img/icon-design-09.png')} style={{height:wp(12), width: wp(12)}} /> */}
                    <LinearGradient style={styles.Logout} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#3ad1ff', '#117ffe',]}>
                        <Text style={{ fontSize: wp('4%'), fontWeight: '400', fontFamily: 'Prompt-Light', color: '#fff' }}> แก้ไขข้อมูลส่วนตัว </Text>
                    </LinearGradient>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => this.Logout()}
                    // style={styles.login}
                    style={{ justifyContent: 'flex-end', alignSelf: 'center', marginBottom: wp('8%') }}
                >
                    {/* <Image  source={require('./img/icon-design-09.png')} style={{height:wp(12), width: wp(12)}} /> */}
                    <LinearGradient style={styles.Logout} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#c82c2d', '#c82c2d']}>
                        <Text style={{ fontSize: wp('4%'), fontWeight: '400', fontFamily: 'Prompt-Light', color: '#fff' }}> ออกจากระบบ </Text>
                    </LinearGradient>
                </TouchableOpacity>





                <Modal
                    isVisible={this.state.visibleModal === 'default'}
                // onSwipeComplete={() => this.setState({ visibleModal: false })}
                // swipeDirection={['down']}

                // style={styles.bottomModal}
                >
                    {this.renderModalContent()}

                </Modal>
            </ScrollView>





        );
    }
}

const styles = StyleSheet.create({
    lineStylew3: {
        borderWidth: 1,
        borderColor: '#1E90FF',
        //backgroundColor: '#8A23FC',
        marginTop: wp('3%'),
        alignSelf: 'center',
        height: wp('16%'),
        marginLeft: wp('3%'),
        marginRight: wp('3%')

    },
    linearGradient: {
        flex: 1,
        width: wp(90),
        height: wp(12),
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        marginTop: wp(38),
        alignSelf: 'center',


    },
    linearGradient1: {
        flex: 1,
        width: wp(90),
        height: wp(12),
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        marginTop: wp(7),
        alignSelf: 'center',

    },
    buttonText: {
        fontSize: wp('4.5%'),
        fontFamily: 'Prompt-Light',
        textAlign: 'center',
        margin: wp('2.5%'),
        fontWeight: '500',
        color: '#ffffff',
        backgroundColor: 'transparent',
        alignSelf: 'center'
    },
    lineStylew5: {
        borderWidth: 0.5,
        borderColor: '#DCDCDC',
        backgroundColor: '#9B9B9B',
        marginTop: 10,
        alignSelf: 'center',
        width: wp(80),


    },
    content: {
        backgroundColor: 'white',
        height: wp(10),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    contentTitle1: {
        //marginTop:wp(2),
        fontSize: 16,
        marginBottom: wp(2),
        fontFamily: 'Prompt-Light'
    },
    contentTitle2: {
        marginTop: wp(2),
        fontSize: 16,
        marginBottom: wp(2),
        fontFamily: 'Prompt-Light'
    },
    lineStylew6: {
        borderWidth: 0.25,
        borderColor: '#DCDCDC',
        alignSelf: 'center',
        width: wp(100),
    },

    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
        flex: 1,

    },
    dropdown: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: wp(2),

    },
    Logout: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        //padding: 10,
        // backgroundColor: '#8A23FC',//add8e6 ,79b6d2
        height: wp(10),
        width: wp(65),
        marginTop: wp(2),
        // marginBottom: wp(2),
        //marginLeft:14,
        justifyContent: 'center',
        flexDirection: 'column',
        // borderWidth: 1,
        borderRadius: 30,
        // borderColor: '#fff'
    },
    submit: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        //padding: 10,
        // backgroundColor: '#8A23FC',//add8e6 ,79b6d2
        height: wp(10),
        width: wp(35),
        marginTop: wp(2),
        // marginBottom: wp(2),
        //marginLeft:14,
        justifyContent: 'center',
        flexDirection: 'column',
        // borderWidth: 1,
        borderRadius: 30,
        // borderColor: '#fff'
    },

})

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 12,
        width: wp(75),
        //   paddingTop: 13,
        height: wp(8),
        //   paddingHorizontal: 10,
        //   paddingBottom: 12,
        borderWidth: 0.5,
        borderColor: "gray",
        borderRadius: 6,
        backgroundColor: "white",
        color: "black",
        alignSelf: "center",
        marginLeft: wp(-4),
        fontFamily: 'Prompt-Light',


    },
    inputAndroid: {
        fontSize: 12,
        width: wp(75),
        paddingTop: 13,
        height: wp(12),
        paddingHorizontal: 10,
        paddingBottom: 12,
        borderWidth: 1,
        borderColor: "gray",
        borderRadius: 4,
        backgroundColor: "white",
        color: "black",
        alignSelf: "center", marginLeft: wp(-4)
    },


})