import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet, ScrollView, TouchableHighlight, ImageBackground, TouchableOpacity } from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as loc,
    removeOrientationListener as rol
} from 'react-native-responsive-screen';
import { Icon } from 'react-native-elements';
import Moment from 'moment';
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
import Ionicons from 'react-native-vector-icons/Ionicons';
const hi = Dimensions.get('window').height
export default class allweatherscreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            latitude: 0,
            longitude: 0,
            data: [],
            today: [],
            WeatherDescription: null,
            loading: true,
            RegionsForecast: null,
            SevenDaysForecast: []
        };
    }

    getLocation() {

        // Get the current position of the user
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState(
                    (prevState) => ({
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    }), () => { this.getData(); }
                );
            },
            (error) => this.setState({ forecast: error.message }),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
    }
    getData() {
        // alert(this.state.latitude+','+this.state.longitude)
        // axios.get('http://npcrapi.netpracharat.com:3000/latlng/13.8849447,100.57561149999992')
        axios.get('http://npcrapi.netpracharat.com:3000/latlng/' + this.state.latitude + ',' + this.state.longitude)
            .then(result => {
                let data = result.data.data[0].SevenDaysForecast
                var today = data.slice(0, 1);
                this.setState({

                    data: data,
                    today: today,
                    province: result.data.data[0].ProvinceNameTh
                })
                // alert(JSON.stringify(today))
                this.getDataWeatherDescription()
            }).catch(err => {
                this.getData()
            })
    }
    getDataWeatherDescription() {
        axios.get('http://203.113.11.192:3000/weatherdescription')
            .then(result => {
                this.setState({
                    WeatherDescription: result.data.DailyForecast,
                    RegionsForecast: result.data.DailyForecast.RegionsForecast,
                    loading: false
                })
                // alert(JSON.stringify(result.data.DailyForecast.RegionsForecast))
                // alert(JSON.stringify(this.state.RegionsForecast.length))
            })
    }
    componentDidMount() {
        this.getLocation()
    }

    Test = () => { }
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
            // header: null,
            // header:{
            //   style: { shadowColor: 'transparent' },
            // },
            headerTitle: <View>
                <Text style={[styles.itemText, { fontSize: wp(5), color: 'black' }]}>
                    {'สภาพภูมิอากาศ'}
                </Text>
            </View>,
            headerLeft: (
                <View>
                    <TouchableOpacity style={{ marginLeft: wp(4), marginTop: wp(12) }} onPress={() => goBack()} >
                        <Ionicons name='ios-arrow-back' size={30} color='white' />
                    </TouchableOpacity>
                </View>
            ),
            headerRight: (
                // <View>
                //   <Icon
                //     size={30}
                //     type='ionicon'
                //     name='ios-gift'
                //     color='rgb(48,142,140)'
                //     containerStyle={{ marginRight: wp(4) }}
                //   />
                // </View>
                ''
            ),
            headerStyle: {
                backgroundColor: '#FFFAFA'
                // borderBottomWidth: 0,
            },
            headerTintColor: '#000',
            headerTitleStyle: {
                fontWeight: 'bold',
                textAlign: 'center',
                flex: 1,
            },
        }
    };
    render() {
        var today = Moment();
        let thaiday = Moment(today).format('ddd');
        let day = Moment(today).format('D');
        let thaiday1 = thaiday == 'Mon' ? 'จันทร์' :
            thaiday == 'Tue' ? 'อังคาร' :
                thaiday == 'Wed' ? 'พุธ' :
                    thaiday == 'Thu' ? 'พฤหัสบดี' :
                        thaiday == 'Fri' ? 'ศุกร์' :
                            thaiday == 'Sat' ? 'เสาร์' : 'อาทิตย์'

        let thaimount = Moment(today).format('M')
        let year = Moment(today).format('Y');
        let year2 = parseInt(year) + 543
        let thaimount1 =
            thaimount == 1 ? 'มกราคม' :
                thaimount == 2 ? 'กุมภาพันธ์' :
                    thaimount == 3 ? 'มีนาคม' :
                        thaimount == 4 ? 'เมษายน' :
                            thaimount == 5 ? 'พฤษภาคม' :
                                thaimount == 6 ? 'มิถุนายน' :
                                    thaimount == 7 ? 'กรกฎาคม' :
                                        thaimount == 8 ? 'สิงหาคม' :
                                            thaimount == 9 ? 'กันยายน' :
                                                thaimount == 10 ? 'ตุลาคม' :
                                                    thaimount == 11 ? 'พฤษจิกายน' : 'ธันวาคม'
        const tomorrow = Moment().add(1, 'days');
        const { goBack } = this.props.navigation;
        //  var time = 5
        var time = Moment().format('H');
        var color_font = time < 18 && time > 5 ? "#000" : '#FFF'
        return (
            this.state.loading ? null :
                <View style={{ flex: 1, }}>

                    <ImageBackground style={{ width: '100%', }}
                        source={time < 18 && time > 5 ? require('../alliconESL/Iconnewsetapp/HomeNewTOTeasylifeAW120.png') :
                            require('../alliconESL/Iconnewsetapp/HomeNewTOTeasylifeAW121.png')} >
                        <ScrollView
                            style={{ height: '100%' }}
                            showsVerticalScrollIndicator={false}
                        >
                            <View>

                                <TouchableOpacity style={{ marginLeft: wp(4), marginTop: wp(12) }} onPress={() => goBack()} >
                                    <Ionicons name='ios-arrow-down' size={30} color={color_font} />
                                </TouchableOpacity>
                            </View>

                            {/* <TouchableHighlight
                        style={ {height:wp(10),width:wp(20)}}
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Icon

                                size={40}
                                type='ionicon'
                                name='ios-close'
                                color='rgb(80,80,80)'
                                containerStyle={{ width: wp(5), position: 'absolute', left: wp(10), marginTop: hi * 0.05 }}
                            />
                        </TouchableHighlight> */}

                            <View style={{ flex: 1, marginBottom: wp(7) }}>


                                <View style={{ alignSelf: 'center', }}>
                                    <View style={{ alignItems: 'center' }}>

                                        <Text style={[styles.fontStyle, { fontSize: wp(7), fontWeight: '500', letterSpacing: 1.0, }]}>
                                            {this.state.today[0].WeatherDescription}
                                        </Text>
                                    </View>

                                </View>
                                <View style={{ alignItems: 'center' }}>

                                    <Text style={[styles.fontStyle, { fontSize: wp(4), fontWeight: '400', letterSpacing: 4.0 }]}>{this.state.province}</Text>
                                    <View style={{ flexDirection: 'row', }}>
                                        <Text style={[styles.fontStyle, { fontSize: wp(26), marginTop: wp(-5) }]}>{this.state.today[0].MaxTemperature.Value}</Text>
                                        <Text style={[styles.fontStyle, { fontSize: wp(17), marginTop: wp(-2), fontWeight: '500' }]}>{'\u00B0'}</Text>
                                    </View>
                                    {/* <Image style={{ height: wp(50), width: wp(50), marginTop: wp(-15), alignSelf: 'center' }} source={{ uri: 'http://203.113.11.192:3000/weather/' + this.state.today[0].WeatherDescription + '.png' }} /> */}
                                    <Text style={[styles.fontStyle, { fontSize: wp(5), fontWeight: '300', alignSelf: 'flex-start', marginLeft: wp(8.5), marginTop: wp(8) }]}>
                                        {thaiday1 + ' ' + Moment(today).format('D ') + thaimount1 + ' ' + year2}
                                    </Text>

                                    <View style={styles.lineStylew6} />
                                </View>
                                {
                                    this.state.data.map((item, index) => {
                                        var today = Moment();
                                        let thaiday = Moment().add(index, 'days').format('ddd')

                                        let day = Moment(today).format('D');
                                        let thaiday1 = thaiday == 'Mon' ? 'วันจันทร์' :
                                            thaiday == 'Tue' ? 'วันอังคาร' :
                                                thaiday == 'Wed' ? 'วันพุธ' :
                                                    thaiday == 'Thu' ? 'วันพฤหัสบดี' :
                                                        thaiday == 'Fri' ? 'วันศุกร์' :
                                                            thaiday == 'Sat' ? 'วันเสาร์' : 'วันอาทิตย์'

                                        let thaimount = Moment(today).format('M')
                                        let year = Moment(today).format('Y');
                                        let year2 = parseInt(year) + 543
                                        let thaimount1 =
                                            thaimount == 1 ? 'มกราคม' :
                                                thaimount == 2 ? 'กุมภาพันธ์' :
                                                    thaimount == 3 ? 'มีนาคม' :
                                                        thaimount == 4 ? 'เมษายน' :
                                                            thaimount == 5 ? 'พฤษภาคม' :
                                                                thaimount == 6 ? 'มิถุนายน' :
                                                                    thaimount == 7 ? 'กรกฎาคม' :
                                                                        thaimount == 8 ? 'สิงหาคม' :
                                                                            thaimount == 9 ? 'กันยายน' :
                                                                                thaimount == 10 ? 'ตุลาคม' :
                                                                                    thaimount == 11 ? 'พฤษจิกายน' : 'ธันวาคม'
                                        var color_font = time < 18 && time > 5 ? "#000" : '#FFF'
                                        return (
                                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: wp(3), }}>
                                                <View style={{ flexDirection: 'column', alignSelf: 'center', marginLeft: wp(7), }}>
                                                    <Text style={{ fontSize: wp(4), fontWeight: '600', fontFamily: 'Prompt-Light', color: color_font, marginLeft: wp(3), textAlign: 'left', width: wp(35) }}>
                                                        {thaiday1}
                                                    </Text>
                                                    <Text style={[styles.fontStyle, { fontSize: wp(3.8), fontWeight: '300', marginLeft: wp(2.8) }]}>
                                                        {item.WeatherDescription}
                                                    </Text>

                                                </View>
                                                <View>
                                                    <Image style={{
                                                        height: wp(10), width: wp(10),
                                                    }} source={{ uri: 'http://203.113.11.192:3000/weather/' + item.WeatherDescription + '.png' }} />

                                                </View>
                                                <View style={{ width: wp(27), flexDirection: 'row', marginLeft: wp(10), }}>
                                                    <Text style={{ fontFamily: 'Prompt-Light', color: color_font, fontSize: wp(6), fontWeight: '500', width: wp(12) }}>
                                                        {item.MinTemperature.Value + '\u00B0' + '  '}
                                                    </Text>
                                                    <Text style={{ textAlign: 'left', fontFamily: 'Prompt-Light', color: '#cccccc', fontSize: wp(6), fontWeight: '500', width: wp(15) }}>
                                                        {item.MaxTemperature.Value + '\u00B0' + '\u0043'}
                                                    </Text>
                                                </View>
                                            </View>
                                        )
                                    })
                                }

                            </View>
                            {/* <Text style={{ marginTop: wp(2), textDecorationLine: 'underline', marginBottom: wp(2), color: '#000', marginLeft: wp(8.5), fontSize: wp(4.5), fontFamily: 'Prompt-Light', fontWeight: '500' }}>
                                {'รายละเอียด'}
                            </Text>
                            <Text style={{ textAlign: 'justify', color: '#000', marginLeft: wp(8.5), marginRight: wp(8.5), fontSize: wp(4), fontFamily: 'Prompt-Light', fontWeight: '500' }}>
                                {this.state.WeatherDescription.DescTh}
                            </Text> */}

                            {/* {this.state.RegionsForecast.map((item, index) => {
                                return (
                                    <View>
                                        <Text style={{ marginTop: wp(1), textDecorationLine: 'underline', marginBottom: wp(2), color: '#000', marginLeft: wp(8.5), fontSize: wp(4.5), fontFamily: 'Prompt-Light', fontWeight: '500' }}>
                                            {item.RegionName}
                                        </Text>
                                        <Text style={{ textAlign: 'justify', color: '#000', marginLeft: wp(8.5), marginRight: wp(8.5), fontSize: wp(4), fontFamily: 'Prompt-Light', fontWeight: '500' }}>
                                            {item.Description}
                                        </Text>
                                    </View>

                                )

                            })} */}
                        </ScrollView>
                    </ImageBackground>
                </View>
        );
    }
}
var time = Moment().format('H');
var color_font = time < 18 && time > 5 ? "#000" : '#FFF'

const styles = StyleSheet.create({
    containerLoading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    container: {
        backgroundColor: '#FFF',
        flex: 1
    },
    itemText: {
        color: "rgba(56,158,158,0.9)",
        fontFamily: 'Prompt-SemiBold',
        marginBottom: wp('0.5'),
        fontSize: wp('3.5'),
        marginLeft: 10,
        marginRight: 10

    },
    fontStyle: {
        fontFamily: 'Prompt-Light',
        color: color_font
    },
    lineStylew6: {
        borderWidth: 1,
        borderColor: color_font,
        alignSelf: 'center',
        width: wp(90),
        height: wp(0.1),
        marginTop: wp(1)

    },




});