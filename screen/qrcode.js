import React, { Component } from 'react';
import {
  Platform, FlatList, View, Text,
  Button, Alert, TouchableHighlight, StyleSheet,
  TextInput, ActivityIndicator, Image, ImageBackground,
  TouchableOpacity, Modal, Linking ,AsyncStorage
} from 'react-native';
import { Icon } from 'react-native-elements'
import QRCodeScanner from 'react-native-qrcode-scanner';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';
import { QRScannerView } from 'react-native-qrcode-scanner-view';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import { TSpan } from 'react-native-svg';


export default class qrcode extends Component {

  constructor(props) {
    super(props);

    state = {
      qrRead: "",
      id:'',
      Ipaddress: '111.111.111.111',
      urlpayment: '',
      data1:[],

    }


  }


  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    // const params = navigation.getParam('checklogin')
    // this.state.user
    // alert(params)
    return {
      title: 'สแกนจ่ายบิล',

      headerLeft: (
        <TouchableOpacity style={{ marginLeft: wp(3), }} >
          <Ionicons name='ios-close' size={40} onPress={() => navigation.goBack()} />
        </TouchableOpacity>
      ),
      // headerRight: (
      //   <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>

      //   </HeaderButtons>
      // ),
      // //  headerTitle: <Logo/>,

      headerStyle: {
        backgroundColor: '#F9F9F9'
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        // fontFamily: "Prompt-Light",
        textAlign: 'center',
        flex: 1,
        fontFamily: "Prompt-Light", color: "#000", fontSize: 20, fontWeight: '500'

      },
    }
  };


  // onSuccess = (e) => {

  //   Linking
  //     .openURL(e.data) 
  //     // .catch(err => console.error('An error occured', err));
  // }

  // scan=()=>{
  //   this.scanner.reactivate();

  // }


  getProfile() {
    const url = "http://203.113.11.167/api/myuser/" + this.state.id;
    console.log(url);
    // const headers = {
    //   'Content-Type': 'application/json',
    //   'Authorization': 'JWT fefege...'
    // }
    axios.get(url)
      .then(result => {
        // var userprofile = JSON.stringify(result);
        // console.log(userprofile);
        console.log(result, 'resultProfile');
        console.log(JSON.stringify(result.data.data.id) + 'checkbill');
        this.setState({
          data1: result.data.data,
          Firstname: result.data.data.name,
          Firstname1: result.data.data.name,
          Lastname: result.data.data.lastname,
          Lastname1: result.data.data.lastname,
          Mobileno: result.data.data.tel,
          Mobileno1: result.data.data.tel,
          Email: result.data.data.email,
          Email1: result.data.data.email,
          Personalid: result.data.data.IDcard,
          Address: result.data.data.address,
          Province: result.data.data.province,
          District: result.data.data.district,
          Subdistrict: result.data.data.subdistrict,
          Postcode: result.data.data.postcode

        })
        console.log(this.state.data1,'myprofile')
        // console.log(this.state.Lastname1)



      })
      .catch(err => {
        alert(JSON.stringify(error));
      })
  }

  // renderTitleBar = () => <Text style={{ color: 'white', textAlign: 'center', padding: 16 }}>Title</Text>

  renderMenu = () => <Text style={{ color: 'white', textAlign: 'center', padding: 16 }}>Menu</Text>

  barcodeReceived = (event) => {

    // alert(JSON.stringify(event))
    // var res = '|010754500016105 01000101508597 200000755923458 35310'
   
    var n = event.data.split('\r');
    var n2 = []
    n2.push(n[1].substring(2,14));

    const url = "https://www.toteservice.com/esapi/AipEasyPay/setpymbill";

    const headers = {
      'Content-Type': 'application/json',
      'Authorization': 'JWT fefege...'
    }

    axios.post(url, {
      ipaddress: this.state.Ipaddress,
      auth_contracts: "easy1ife2ew@",
      auth_code: "s3f1@6d54b",
      lang: "TH",
      easylife_member_id: JSON.stringify(this.state.id),
      firstname: this.state.Firstname,
      lastname: this.state.Lastname,
      ba_list: n2,
      mobile_contact: this.state.data1.tel,
      email: this.state.data1.email
    },

      {
        headers: headers
      }

    ).then(result => {
      console.log(result);


        console.log('in')
        // var getbaid = (result.data.result.balist.fixed[0].baid)
        console.log(result.data.rurl);


        this.props.navigation.navigate('eservicePage', { url: result.data.rurl.toString() })
        // Linking.openURL()


        this.setState({ urlpayment: result.data.rurl })
        // this.setState({ loading: false })
        // alert(this.state.urlpayment)
   
    }).catch(e => {
      console.log(e);
    });


  }

  componentDidMount = async () => {
    DeviceInfo.getIpAddress().then(ip => {
        console.log("IpAddress:" + ip);
        this.setState({ Ipaddress: ip })
    });
    var id = await AsyncStorage.getItem('userid');

    // console.log(usrtoken);
    this.setState({ id: id })

    this.getProfile();
    this.barcodeReceived();
    


  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        < QRScannerView
          hintText={<Text style={{fontFamily:'Prompt-Light', fontSize:wp(5), fontWeight:'500'}}>สแกนจ่ายบิล</Text>}
          onScanResult={this.barcodeReceived}
          renderHeaderView={this.renderTitleBar}
        // renderFooterView={ this.renderMenu }
        // scanBarAnimateReverse={ true }

        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
    fontFamily: "Prompt-Light",
    fontWeight: '400'
  },
  buttonTouchable: {
    padding: 16,
  },
  headerHome: {
    borderBottomColor: '#FFDEAD',
    borderBottomWidth: wp('1.5'),
    borderColor: '#FFDEAD',
    justifyContent: 'center',
    marginLeft: wp('2'),
    alignItems: 'center',
    borderWidth: 2,
    borderRadius: 100,
    width: wp('8.6'),
    height: wp('8.6'),
    backgroundColor: '#0099FF'
  },
});
