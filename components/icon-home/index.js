import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Image } from 'react-native'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen';
import Modal from 'react-native-modal';


export default class index extends Component {

  constructor(props) {
    super(props);
    this.index = 0;
    this.index2 = 0;

    this.state = {

      visibleModal: false,

    }
  }



  renderModalContent = () => (

    <View style={styles.content}>



      <Text onPress={() => this.renderSetting()} style={styles.contentTitle1}>ตั้งค่า</Text>
      {/* <View style={styles.lineStylew6} /> */}
      <Text onPress={() => this.renderEditProfile()} style={styles.contentTitle2}>แก้ไขข้อมูลส่วนตัว</Text>
      {/* <View style={styles.lineStylew6} /> */}
      <TouchableOpacity
        onPress={() => this.setState({ visibleModal: false })}

      >
        <Text style={{ fontWeight: '500', fontFamily: 'prompt-light', fontSize: wp(5), color: '#1b7af7', marginBottom: wp(2) }}>ปิด</Text>
      </TouchableOpacity>

    </View>

  );

  renderEditProfile() {
    this.setState({ visibleModal: false })
    this.props.route.navigate('editProfilePage')
  }

  renderSetting() {
    this.setState({ visibleModal: false })
    this.props.route.navigate('settingPage')
  }


  render() {
    return (
      <View style={{
        width: '95%', height: wp(50), backgroundColor: '#FFF', borderRadius: 10, justifyContent: 'center', alignSelf: 'center', flexDirection: 'column'
        , shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, marginTop: wp(2)
      }}>

        <View style={styles.iconstyle2}>

          <TouchableOpacity onPress={() => this.props.route.navigate('checkbillPage')} >
            <Image style={{ width: wp(15), height: wp(20.5), }} source={require('../../ICON_Png/กรอบเท่ากัน/HomeNewTOTeasylifeAW128.png')} />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.props.route.navigate('qrcodePage')} >
            <Image style={{ width: wp(16), height: wp(20.5), marginLeft: wp(6), }} source={require('../../ICON_Png/กรอบเท่ากัน/HomeNewTOTeasylifeAW130.png')} />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.props.route.navigate('noticePage')} >
            <Image style={{ width: wp(15), height: wp(20.5), marginLeft: wp(6), }} source={require('../../ICON_Png/กรอบเท่ากัน/HomeNewTOTeasylifeAW131.png')} />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.props.route.navigate('reportPage')} >
            <Image style={{ width: wp(15), height: wp(20.5), marginLeft: wp(6), }} source={require('../../ICON_Png/กรอบเท่ากัน/HomeNewTOTeasylifeAW132.png')} />
          </TouchableOpacity>

        </View>


        <View style={styles.iconstyle3}>

          <TouchableOpacity onPress={() => this.props.route.navigate('phonebook')} >
            <Image style={{ width: wp(19), height: wp(20.5), }} source={require('../../ICON_Png/กรอบเท่ากัน/HomeNewTOTeasylifeAW133.png')} />
          </TouchableOpacity>



          <TouchableOpacity onPress={() => this.props.route.navigate('serviceCenter')} >
            <Image style={{ width: wp(14.5), height: wp(20.5), marginLeft: wp(5), }} source={require('../../ICON_Png/กรอบเท่ากัน/HomeNewTOTeasylifeAW134.png')} />
          </TouchableOpacity>


          <TouchableOpacity onPress={() => this.props.route.navigate('ebillPage')} >
            <Image style={{ width: wp(15), height: wp(20.5), marginLeft: wp(6), }} source={require('../../ICON_Png/กรอบเท่ากัน/HomeNewTOTeasylifeAW135.png')} />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.setState({ visibleModal: 'bottom' })}>
            <Image style={{ width: wp(18.7), height: wp(20.7), marginLeft: wp(4), }} source={require('../../alliconESL/HomeIcons/HomeIconsAW1_09.png')} />
          </TouchableOpacity>

        </View>

        <Modal
          isVisible={this.state.visibleModal === 'bottom'}
          // onSwipeComplete={() => this.setState({ visibleModal: false })}
          // swipeDirection={['down']}
          style={styles.bottomModal}
        >
          {this.renderModalContent()}
        </Modal>



      </View>
    )
  }
}



const styles = StyleSheet.create({
  iconstyle2: {
    justifyContent: 'center',
    alignContent: 'space-between',
    margin: wp(1.5),
    flexDirection: 'row'
  },
  iconstyle3: {
    justifyContent: 'center',
    alignContent: 'space-between',
    margin: wp(1.5),
    flexDirection: 'row'
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
    flex: 1,

  },
  content: {
    backgroundColor: 'white',
    height: wp(35),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },

  contentTitle1: {
    //marginTop:wp(2),
    fontSize: 16,
    marginBottom: wp(2),
    fontFamily: 'Prompt-Light'
  },
  contentTitle2: {
    marginTop: wp(2),
    fontSize: 16,
    marginBottom: wp(2),
    fontFamily: 'Prompt-Light'
  },
})
