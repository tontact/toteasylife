import React, { Component } from 'react';
import { Animated, Platform, FlatList, View, Text, Button, Alert, StyleSheet, Image, TouchableOpacity, Dimensions, ActivityIndicator, Modal, Linking } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';
import {
    widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';

import { SearchBar } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
// import Swiper from 'react-native-swiper';
import SwiperFlatList from 'react-native-swiper-flatlist';
import LinearGradient from 'react-native-linear-gradient';
import Modal2 from 'react-native-modal';
import { FluidNavigator, Transition } from 'react-navigation-fluid-transitions';
import { Header } from 'react-navigation';
import axios from 'axios';
import Carousel from 'react-native-snap-carousel';
import ImageViewer from 'react-native-image-zoom-viewer';
import { Icon } from 'react-native-elements';


const HEADER_MAX_HEIGHT = wp(66);
const HEADER_MIN_HEIGHT = wp(20);
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;


const IoniconsHeaderButton = passMeFurther => (
    <HeaderButton {...passMeFurther} IconComponent={Ionicons} iconSize={28} color="white" />
);



let screenWidth = Dimensions.get('window').width;

// const {
//   height: SCREEN_HEIGHT,
// } = Dimensions.get('window');

// const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
// const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
// const NAV_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 64;

// const SCROLL_EVENT_THROTTLE = 16;
// const DEFAULT_HEADER_MAX_HEIGHT = 170;
// const DEFAULT_HEADER_MIN_HEIGHT = NAV_BAR_HEIGHT;
// const DEFAULT_EXTRA_SCROLL_HEIGHT = 30;
// const DEFAULT_BACKGROUND_IMAGE_SCALE = 1.5;




export default class promotion3 extends Component {
    constructor(props) {
        super(props);
        this.index = 0;
        this.index1 = 0;
        this.index2 = 0;
    }

    state = {
        zoomImage: false,
        ListPromotion: [],
        scrollY: new Animated.Value(0),
        pickerDisplayed: false,
        test: null,

        data4: [
            {
                name: 'TOT fiber 2U ',
                detail: 'อินเทอร์เน็ตไฟเบอร์ออพติกแท้ 100% โปรโมชั่น Net Revo(ทุกระดับความเร็ว 50,100,150 และ 200 Mbps) ฟรี! ค่าติดตั้ง ค่าธรรมเนียมแรกเข้าหรือเปลี่ยนแพ็คเกจ และค่าบำรุงรักษารายเดือน ทั้งยังได้รับสิทธิ์ซื้ออุปกรณ์เสริมราคาพิเศษ *เงื่อนไขเป็นไปตามที่บริษัทกำหนด',
                path: 'https://www.tot.co.th/images/default-source/default-album/promotion/2018/05/tot-fiber2u_net-revo/aw_net-revo_final_website-01.png?sfvrsn=9f53f72d_2'
            },


            {
                name: 'Voice Topping',
                detail: 'โปรโมชั่น Voice Topping พูดไม่มียั้ง สำหรับผู้ใช้บริการโทรศัพท์เคลื่อนที่ TOT Mobile ระบบเติมเงิน สามารถสมัครรายการส่งเสริมการขายนี้ได้ผ่าน บริการด่วนพิเศษ กด USSD บริการผ่านเว็บไซต์ และบริการผ่าน Application TOT Mobile Easy Mobile',
                path: 'https://www.tot.co.th/images/default-source/default-album/promotion/2019/extra-top-speed/extra-top-speed_treser-mobile_promotion_01.png?sfvrsn=f80d4ca8_2'
            },
            {
                name: 'TOT 008',
                detail: 'วันนี้ลูกค้าสามารถโทรศัพท์ทางไกลระหว่างประเทศ ประหยัดได้มากกว่า เมื่อกด 008 (คมชัดประหยัดจริง) พิเศษ เริ่มต้นที่ 2 บาท/นาที ผ่านเครือข่ายโทรศัพท์ ทีโอที ได้ตั้งเเต่วันที่ 1 กันยายน 2561 – 31 สิงหาคม 2562',
                path: 'https://www.tot.co.th/images/default-source/default-album/tot-fiber-2u-campaign/top-speed/teaser-mobile_1080x865.png?sfvrsn=c930d281_2'
            },

            {
                name: 'TOT Mobile',
                detail: 'วันนี้ลูกค้าสามารถโทรศัพท์ทางไกลระหว่างประเทศ ประหยัดได้มากกว่า เมื่อกด 008 (คมชัดประหยัดจริง) พิเศษ เริ่มต้นที่ 2 บาท/นาที ผ่านเครือข่ายโทรศัพท์ ทีโอที ได้ตั้งเเต่วันที่ 1 กันยายน 2561 – 31 สิงหาคม 2562',
                path: 'https://www.tot.co.th/images/default-source/default-album/promotion/2018/05/tot3g_plearnplearn/mobile.jpg?sfvrsn=1394bd53_2'
            },
            {
                name: 'SME SUPERB SPEED',
                detail: 'วันนี้ลูกค้าสามารถโทรศัพท์ทางไกลระหว่างประเทศ ประหยัดได้มากกว่า เมื่อกด 008 (คมชัดประหยัดจริง) พิเศษ เริ่มต้นที่ 2 บาท/นาที ผ่านเครือข่ายโทรศัพท์ ทีโอที ได้ตั้งเเต่วันที่ 1 กันยายน 2561 – 31 สิงหาคม 2562',
                path: 'https://www.tot.co.th/images/default-source/default-album/promotion/2019/01/sme-superb-speed/mobile.png?sfvrsn=716c0d00_4'
            },

            {
                name: 'Y-tel 1234',
                detail: 'วันนี้ลูกค้าสามารถโทรศัพท์ทางไกลระหว่างประเทศ ประหยัดได้มากกว่า เมื่อกด 008 (คมชัดประหยัดจริง) พิเศษ เริ่มต้นที่ 2 บาท/นาที ผ่านเครือข่ายโทรศัพท์ ทีโอที ได้ตั้งเเต่วันที่ 1 กันยายน 2561 – 31 สิงหาคม 2562',
                path: 'https://www.tot.co.th/images/default-source/default-album/promotion/2018/05/tot-y-tel-1234/mobile.jpg?sfvrsn=5ef55721_4'
            },
        ],



    };




    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        // const params = navigation.getParam('checklogin')
        // this.state.user
        // alert(params)


        return {

            headerTransitionPreset: 'fade-in-place',
            // title: 'ชำระค่าบริการ',
            headerTitle: (<Text style={{ fontFamily: "Prompt-SemiBold", color: "#000", textAlign: "center", flex: 1, fontSize: 20 }}>โปรโมชั่น</Text>),

            headerRight: (
                <TouchableOpacity style={{ marginRight: wp(2.5) }} >
                    {/* <Icon name='ios-menu' size={30} color='#000' /> */}
                </TouchableOpacity>
            ),
            headerLeft: (
                <TouchableOpacity style={{ marginLeft: wp(3), }} >
                    <Ionicons name='ios-arrow-down' size={30} onPress={() => navigation.goBack()} />
                </TouchableOpacity>
            ),
            // //  headerTitle: <Logo/>,

            headerStyle: {
                backgroundColor: '#F9F9F9'
            },
            headerTintColor: '#000',
            headerTitleStyle: {
                fontFamily: "Prompt-Light",
                textAlign: 'center',
                flex: 1,
                fontSize: 20,
            },
        }
    };
    //   test(){
    //       Alert.alert('in')
    //   }



    // renderItem(item) {
    //   return (

    //   );
    // }

    componentDidMount() {
        this.getPromotion();
    }

    getPromotion() {
        const url = "http://203.113.11.167/api/promotion";
        console.log(url);
        axios.get(url)
            .then(result => {
                this.setState({ ListPromotion: result.data })
                let data = []

                this.state.ListPromotion.forEach(item => {
                    if (item.type_id == 4) {
                        data.push(item)
                    }
                })
                this.setState({ ListPromotion: data })
                // alert(JSON.stringify(data))

                // alert(JSON.stringify(result.data))
            })

            .catch(err => {
                alert(JSON.stringify(err));
            })

    }

    isLegitIndex1(index, length) {
        if (index < 0 || index >= length) return false;
        return true;
    }

    pagination2 = (velocity) => {
        let nextIndex;
        if (Platform.OS == "ios")
            nextIndex = velocity > 0 ? this.index2 + 1 : this.index2 - 1;
        else
            nextIndex = velocity < 0 ? this.index2 + 1 : this.index2 - 1;
        if (this.isLegitIndex1(nextIndex, this.state.ListPromotion.length)) {
            this.index2 = nextIndex;
        }
        this.flatlist2.scrollToIndex({ index: this.index2, animated: true });
    }

    renderItem(item) {

        // const { } = style;
        return (
            <View style={{ flex: 1, }}>
                <TouchableOpacity
                    // underlayColor={false}
                    onPress={() => {

                        this.setState({
                            test: { name: item.name, shortdetail: item.shortdetail, fulldetail: item.fulldetail, image: item.image, image_details: item.image_details, regispromotion: item.regispromotion },
                            pickerDisplayed: true
                        })
                    }}>
                    <View style={{ alignSelf: 'center', marginBottom: wp(4), }} >

                        <View style={{
                            width: wp(45), height: wp(50), backgroundColor: '#FFF', borderRadius: 10, alignItems: 'center', flexDirection: 'column'
                            , shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14,
                        }}>

                            <View style={{ width: wp(45), alignSelf: 'center', borderTopRightRadius: 10, borderTopLeftRadius: 10, overflow: 'hidden', }}>
                                <Image source={{ uri: item.image }} style={{ width: wp(45), height: wp(36), }} />
                            </View>

                            <View style={{ flexDirection: 'row' }}>
                                {/* <Image source={require('../img/2548.png')} style={{ width: 40, height: 40, borderRadius: (40 / 2), margin: 5 }} /> */}

                                <View style={{ flex: 1, flexDirection: 'column', marginLeft: wp(3), marginTop: 8 }}>
                                    <Text numberOfLines={1} style={{ color: "#000000EE", width: wp('35'), fontFamily: 'Prompt-SemiBold', fontSize: 13, }}>{item.name}</Text>
                                    <View style={{ flexDirection: 'column', marginTop: 1 }}>
                                        <Text numberOfLines={1} style={{ color: "#000000AA", width: wp('35'), fontSize: 10, fontFamily: 'Prompt-Light' }}>{item.shortdetail}</Text>
                                    </View>
                                </View>
                            </View>

                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }

    modaldetail = () => {
        const imageOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });
        const imageTranslate = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -50],
            extrapolate: 'clamp',
        });
        //let data = [1, 2, 3]
        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
            extrapolate: 'clamp',
        });
        return (
            <LinearGradient colors={['#FFF', '#FFF']} style={styles.linearGradient}>
                <View style={{ flex: 1, flexDirection: 'column', }}>
                    <ScrollView style={{ flex: 1, }}
                        showsVerticalScrollIndicator={false}
                        style={styles.fill}
                        scrollEventThrottle={16}
                        onScroll={Animated.event(
                            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }]
                        )}>


                        <Text style={{ marginTop: hp(40), marginLeft: wp(3), marginRight: wp(3), width: wp(95), fontFamily: 'Prompt-SemiBold', fontSize: 18, color: 'black' }}>
                            {this.state.test.name}
                        </Text>

                        <View style={{ alignItems: 'center' }}>
                            <Text style={{ marginTop: wp(5), marginLeft: wp(3), marginRight: wp(3), width: wp(85), textAlign: 'justify', fontFamily: 'Prompt-Light', color: 'black' }}>
                                {this.state.test.shortdetail}
                            </Text>


                            <View style={styles.cardNewsContainer}>
                                <Carousel
                                    ref={(c) => { this._carousel = c; }}
                                    data={this.state.test.image_details}
                                    sliderWidth={wp('100%')}
                                    itemWidth={wp(70)}
                                    enableSnap={true}
                                    loop={true}
                                    lockScrollWhileSnapping={true}
                                    renderItem={({ item }) => {
                                        return (
                                            <View style={{ shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, }}>
                                                <TouchableOpacity onPress={() => { this.setState({ zoomImage: true }) }}>
                                                    <Image style={{ alignSelf: 'center', recideMode: 'contian', width: wp(70), height: wp(50), borderRadius: 10, marginTop: wp(5) }} source={{ uri: item.imagedetail }}
                                                        PlaceholderContent={<ActivityIndicator />} />
                                                </TouchableOpacity>

                                                <Modal visible={this.state.zoomImage} transparent={true} swipeDirection='down' swipeThreshold={50} onSwipeComplete={() => this.setState({ zoomImage: false })} >
                                                    <ImageViewer imageUrls={this.state.test.image_details.map((item, index) => { return { url: item.imagedetail } })} />
                                                    <Icon
                                                        underlayColor={false}
                                                        // raised
                                                        name='md-close'
                                                        type='ionicon'
                                                        color='#FFF'
                                                        size={wp(7)}
                                                        containerStyle={{ position: 'absolute', right: wp(10), top: wp(13), alignSelf: 'flex-end', marginRight: wp(-2) }}
                                                        onPress={() => this.setState({
                                                            zoomImage: false
                                                        })}
                                                    />
                                                </Modal>
                                            </View>
                                        );
                                    }}
                                />
                            </View>

                            <Text style={{ marginTop: wp(5), marginLeft: wp(3), marginRight: wp(3), width: wp(85), textAlign: 'justify', fontFamily: 'Prompt-Light', color: 'black' }}>

                                {this.state.test.fulldetail}
                            </Text>

                        </View>
                        {this.state.test.regispromotion != "" ?
                            <TouchableOpacity
                                style={{ alignItems: 'center', }} onPress={() => Linking.openURL(this.state.test.regispromotion.toString())}>
                                {/* <Image  source={require('./img/icon-design-09.png')} style={{height:wp(12), width: wp(12)}} /> */}
                                <LinearGradient style={styles.login} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }} colors={['#0062b1', '#68ccca']}>
                                    <Text style={{ fontSize: wp(4), color: '#fff', fontFamily: 'Prompt-Light' }}> รายละเอียดเพิ่มเติม </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            : null}
                        <View style={styles.lineStylew1}></View>

                        <View style={{ height: wp(70), width: wp(100), flex: 1, backgroundColor: 'rgb(247, 247, 247)' }} >
                            <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(4), fontWeight: '500', marginLeft: wp(7), marginTop: wp(5), }}>โปรโมชั่นที่เกี่ยวข้อง</Text>
                            <FlatList

                                style={{ alignSelf: 'center', }}
                                showsHorizontalScrollIndicator={false}
                                horizontal
                                data={this.state.ListPromotion}
                                ref={ref => (this.flatlist2 = ref)}
                                onScrollEndDrag={e => {
                                    this.pagination2(e.nativeEvent.velocity.x);
                                }}
                                renderItem={({ item }) => {
                                    return (

                                        <TouchableOpacity

                                            // underlayColor={false}
                                            onPress={() => {
                                                this.setState({
                                                    test: { name: item.name, shortdetail: item.shortdetail, fulldetail: item.fulldetail, image: item.image, image_details: item.image_details, regispromotion: item.regispromotion },
                                                    pickerDisplayed: true
                                                })
                                            }}
                                            style={styles.BlockPromo1}>

                                            <View>
                                                <View style={{ width: wp(42), alignSelf: 'center', borderTopRightRadius: 10, borderTopLeftRadius: 10, overflow: 'hidden', }}>
                                                    <Image source={{ uri: item.image }} style={{ width: wp(42), height: hp(16), }} />
                                                </View>
                                                <View style={{ flexDirection: 'row' }}>
                                                    {/* <Image source={require('../img/2548.png')} style={{ width: 40, height: 40, borderRadius: (40 / 2), margin: 5 }} /> */}

                                                    <View style={{ flex: 1, flexDirection: 'column', marginLeft: wp(3), marginTop: 8, }}>
                                                        <Text numberOfLines={1} style={{ color: "#000000EE", width: wp('35'), fontFamily: 'Prompt-SemiBold', fontSize: 13 }}>{item.name}</Text>
                                                        <View style={{ flexDirection: 'column', marginTop: 1, marginBottom: wp(2) }}>
                                                            <Text numberOfLines={1} style={{ color: "#000000AA", width: wp('35'), fontSize: 10, fontFamily: 'Prompt-Light' }}>{item.shortdetail}</Text>
                                                        </View>
                                                        {/* <View style={styles.lineStylew4} />
    
                <Text style={{ justifyContent: 'center', alignSelf: 'center', fontWeight: 'bold', color: '#1E90FF' }}>View detail</Text> */}
                                                    </View>
                                                </View>

                                            </View>
                                        </TouchableOpacity>
                                    )
                                }}
                            />
                        </View>



                    </ScrollView>


                    <Animated.View style={[styles.header, { height: headerHeight }]}>
                        <Animated.View>
                            <View style={styles.bar}>
                                <Text style={styles.title}  >TOT Easy Life</Text>
                            </View>
                        </Animated.View>

                        <Animated.Image
                            style={[styles.backgroundImage, { opacity: imageOpacity, transform: [{ translateY: imageTranslate }] },]}
                            source={{ uri: this.state.test.image }} />

                        {/* <Text style={{ justifyContent: 'flex-start', alignItems: 'baseline', marginLeft: wp(3), fontWeight: '900', fontSize: 20 }} onPress={() => { this.setState({ pickerDisplayed: false, test: null }) }} >X</Text> */}

                        <View style={{ marginTop: wp(-8), marginLeft: wp(4) }}>
                            <Ionicons
                                containerStyle={{ justifyContent: 'flex-start', alignSelf: 'flex-start' }}
                                name='ios-close'

                                size={45}

                                onPress={() => { this.setState({ pickerDisplayed: false, test: null }) }} />
                        </View>
                    </Animated.View>



                </View>
            </LinearGradient>
        )
    }

    render() {
        const sizeIcon = wp('5%');
        return (
            // <LinearGradient colors={['#FFFAFA', '#FFFAF0']} style={styles.linearGradient}>
            <ScrollView style={{ flex: 1, }}>

                <View style={{ flex: 1, marginBottom: wp(20) }}>
                    <LinearGradient start={{ x: 1.5, y: 1 }} end={{ x: 0, y: 2 }} colors={['#FE8327', '#EB2028',]} style={styles.linearGradient}>

                        <View style={styles.detailTopBottomSubContainer}>
                            <View style={{ flexDirection: 'column', alignItems: 'center' }}>
                                <Text style={{ color: 'white', fontSize: scaleToDimension(35), fontFamily: 'Prompt-SemiBold', }}>TOT IPTV</Text>
                                <Text style={{ color: 'white', fontSize: scaleToDimension(18), fontFamily: 'Prompt-Light', marginTop: wp(-2), }}>กล่องดูทีวี</Text>
                            </View>
                            <View style={{ marginTop: wp(-23), width: wp(50), height: wp(50), alignSelf: 'center' }}>
                                <Image resizeMode="contain" style={{ width: wp(72), height: wp(72), alignSelf: 'center' }}
                                    source={require('../alliconESL/NewsIcons/PromotionTOTeasylifeAW1copy16.png')}
                                />
                            </View>
                        </View>


                    </LinearGradient>

                    <View style={{ marginTop: wp(5) }}>
                        <FlatList
                            showsHorizontalScrollIndicator={false}
                            numColumns={2}
                            data={this.state.ListPromotion}
                            keyExtractor={(item, index) => item.id}
                            ref={ref => (this.flatlist2 = ref)}
                            renderItem={({ item }) => this.renderItem(item)}>
                        </FlatList>

                    </View>

                    {this.state.test != null ?
                        <Modal2

                            animationType={"slide"}
                            isVisible={this.state.pickerDisplayed}
                            onSwipeComplete={() => this.setState({ pickerDisplayed: false })}
                            // swipeDirection={['down']}
                            style={styles.bottomModal}
                            onBackdropPress={() => this.setState({ pickerDisplayed: false })}

                        >

                            {
                                this.modaldetail()
                            }

                        </Modal2> : null
                    }

                </View>




            </ScrollView>
            // </LinearGradient>
        );
    }
}

const scaleToDimension = (size) => {
    return screenWidth * size / 375
};

const styles = StyleSheet.create({


    linearGradient: {
        flexDirection: 'column',
        flex: 1,
        alignSelf: 'center',

        height: wp(60),
        width: wp('100')
    },

    detailTopContainer: {
        height: scaleToDimension(250),
        width: screenWidth,
        backgroundColor: 'transparent',

    },

    detailTopBottomSubContainer: {
        width: screenWidth - 30,
        backgroundColor: 'transparent',
        position: 'absolute',
        // bottom: wp(6),
        // left: wp(7),
        // right: wp(5),
        flexDirection: "column",
        marginTop: wp(10),
        alignSelf: 'center',
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
        flex: 1,

    },
    cardNewsContainer: {
        alignSelf: 'center',
        marginTop: wp(5),
        shadowColor: "#000",
        backgroundColor: 'rgb(238, 238, 238)',
        width: wp(100), height: wp(60),
    },
    lineStylew1: {
        borderWidth: 0.25,
        borderColor: '#DCDCDC',
        alignSelf: 'center',
        width: wp(100),
        height: wp(3),
        marginTop: wp(5),
        backgroundColor: 'rgb(239, 239, 239)'
    },
    BlockPromo1: {
        flex: 1,
        backgroundColor: '#FFF',
        margin: wp(1.5),
        borderRadius: 10,
        shadowColor: "#000",
        marginBottom: wp(3), marginTop: wp(3),
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.23, shadowRadius: 2.62, elevation: 4,
        alignSelf: 'center'
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: '#FFF',
        overflow: 'hidden',
    },
    bar: {
        marginTop: hp(5),
        height: 10,
        alignItems: 'center',
        justifyContent: 'center',

    },
    title: {
        backgroundColor: 'transparent',
        color: 'black',
        fontSize: 18,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: wp(100),
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
    },
    login: {
        alignItems: 'center',
        // backgroundColor: '#03a9f4',
        //padding: 10,
        alignSelf: 'center',
        // backgroundColor: '#8A23FC',
        height: wp(12),
        width: wp(65),
        marginTop: wp(5),
        marginBottom: 10,
        //marginLeft:14,
        justifyContent: 'center',
        flexDirection: 'column',
        // borderWidth: 1,
        borderRadius: 30,
        // borderColor: '#8A23FC'
    },
})










