import React, { Component } from 'react';
import { Animated, Platform, FlatList, View, Text, Button, Alert, TouchableHighlight, StyleSheet, TextInput, ActivityIndicator, Image, ImageBackground, TouchableOpacity, WebView, Dimensions, Modal } from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import TabBar from "react-native-underline-tabbar";
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
import Faded from './Faded';
import { Icon } from 'react-native-elements'
import Share from 'react-native-share'
import Modal2 from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';
import Carousel from 'react-native-snap-carousel';
import ImageViewer from 'react-native-image-zoom-viewer';


const HEADER_MAX_HEIGHT = wp(66);
const HEADER_MIN_HEIGHT = wp(20);
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;


const HEIGHT = 230;


const items = [
  { name: 'TOT ได้รับรางวัลการประกวด Website 2562', code: '#000', },
  { name: 'TOT ได้รับรางวัลการประกวด Website 2562', code: '#000' },
  { name: 'TOT ได้รับรางวัลการประกวด Website 2562', code: '#000' },
  { name: 'TOT ได้รับรางวัลการประกวด Website 2562', code: '#000' },

];


// badge: 3
export default class example extends Component {

  constructor(props) {
    super(props);
    this.index = 0;

  }

  state = {
    test: null,
    zoomImage: false,
    NewFirst: [],
    ListNew: [],
    dataSource: {},
    scrollY: new Animated.Value(0),
    pickerDisplayed: false,

    data_like: {
      status: true
    },
    data_share: {
      status: true
    },

    data2: [
      {
        name: 'TOT ได้รับรางวัลการประกวด Website',
        detail: 'รางวัลการออกแบบและพัฒนาเว็บไซต์ www.tot.co.th จากงานประกาศผลรางวัลชั้นนำประจำปี 2561',
        path: 'https://www.tot.co.th/images/default-source/default-album/news/general/2019/tot-award-2019/news-1.jpg?sfvrsn=16a19bae_10'
      },
      {
        name: 'ทีโอที ภูมิใจ “เน็ตประชารัฐ” รับรางวัล the winner โครงสร้างพื้นฐานดีที่สุดของโลก ',
        detail: 'นายมนต์ชัย หนูสง กรรมการผู้จัดการใหญ่ บมจ. ทีโอที เปิดเผย ตามที่กระทรวงดิจิทัลเพื่อเศรษฐกิจและสังคมได้เสนอโครงการ “เน็ตประชารัฐ” ที่ ทีโอที รับผิดชอบดูแล เข้าชิงรางวัลในเวทีระดับโลก WSIS Project Prizes 2019 ซึ่งประกอบด้วย 18 ประเภท จัดโดยสหภาพโทรคมนาคมระหว่างประเทศ (International Telecommunication Union หรือ ITU) เพื่อส่งเสริมการดำเนินการตามผลการประชุมสุดยอดระดับโลกว่าด้วยสังคมสารสนเทศ และสนับสนุนการบรรลุเป้าหมายการพัฒนาที่ยั่งยืน ปี ค.ศ. 2030 ล่าสุด โครงการ “เน็ตประชารัฐ” ได้รับรางวัล the winner ในงาน World Summit on the Information Society (WSIS) Prized 2019 โดยเน็ตประชารัฐเป็นโครงการที่ได้รับคะแนนโหวตออนไลน์สูงสุด 5 โครงการจากทั้ง 18 ประเภท รวม 90 โครงการ',
        path: 'https://www.thairath.co.th/media/CiHZjUdJ5HPNXJ92GWzHVZOwmOz6FLzbhb.jpg'
      },
      {
        name: 'ทีโอที พร้อมหากได้รับเกียรติใช้เป็นสถานที่จัดการประชุมรัฐสภาชั่วคราว',
        detail: 'เตรียมห้องประชุมใหญ่/ห้องประชุมย่อย-ระบบไอที-สาธารณูปโภค-สิ่งอำนวยความสะดวกรองรับครบครัน',
        path: 'https://www.thairath.co.th/media/CiHZjUdJ5HPNXJ92GWzZr3bDox4wNVTSaQ.jpg'
      }
    ],


  }


  static navigationOptions = ({ navigation }) => {
    // const { params = {} } = navigation.state;

    // const params = navigation.getParam('checklogin')
    // this.state.user
    // alert(params)
    return {
      headerTitle: (<Text style={{ fontFamily: "Prompt-SemiBold", color: "#000", textAlign: "center", flex: 1, fontSize: 20 }}>ข่าวสารประชาสัมพันธ์</Text>),

      headerRight: (
        <TouchableOpacity style={{ marginRight: wp(2.5) }} >
          {/* <Ionicons name='ios-menu' size={30} color='#000' /> */}
        </TouchableOpacity>
      ),
      // //  headerTitle: <Logo/>,

      headerLeft: (
        <TouchableOpacity style={{ marginLeft: wp(3), }} >
          <Ionicons name='ios-arrow-back' size={30} onPress={() => navigation.goBack()} />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: '#FFF'
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        fontFamily: "Prompt-Light",
        textAlign: 'center',
        flex: 1,

      },
    }
  };

  isLegitIndex(index, length) {
    if (index < 0 || index >= length) return false;
    return true;
  }


  pagination = (velocity) => {
    let nextIndex;
    if (Platform.OS == "ios")
      nextIndex = velocity > 0 ? this.index + 1 : this.index - 1;
    else
      nextIndex = velocity < 0 ? this.index + 1 : this.index - 1;
    if (this.isLegitIndex(nextIndex, this.state.data2.length)) {
      this.index = nextIndex;
    }
    this.flatlist.scrollToIndex({ index: this.index, animated: true });
  }

  share() {
    let shareOptions = {

      title: 'เครือข่ายเน็ตอาสาประชารัฐ',
      message: 'Test',
      url: 'https://npcr.netpracharat.com/News/NewsCm/Detail.aspx?id=6980',
      subject: "Share Link" //  for email
    };
    Share.open(shareOptions)
    // this.point_1('share')
  }

  componentDidMount() {
    this.getNew();
    var that = this;
    let items = Array.apply(null, Array(4)).map((v, i) => {
      return { id: i, src: 'http://placehold.it/200x200?text=' + (i + 1) };
    });
    that.setState({
      dataSource: items,
    });
  }
  getNew() {
    const url = "http://203.113.11.167/api/news";
    console.log(url);
    axios.get(url)
      .then(result => {

        var data = result.data.slice(1)
        this.setState({
          NewFirst: result.data[0],
          ListNew: data,
        })
      })
      .catch(err => {
        alert(JSON.stringify(err));
      })
  }

  renderItem(item) {
    // const { } = style;
    return (
      <View style={{ flex: 1, marginTop: wp(5) }}>
        <TouchableOpacity
          // underlayColor={false}
          onPress={() => {
            this.setState({
              test: { name: item.name_news, detail: item.detail, image: item.image, image_details: item.image_details },
              pickerDisplayed: true
            })
          }}>
          <View style={{ alignSelf: 'center', marginBottom: wp(4), }} >

            <View style={{
              width: wp(45), height: wp(50), backgroundColor: '#FFF', borderRadius: 10, alignItems: 'center', flexDirection: 'column'
              , shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14,
            }}>

              <View style={{ width: wp(45), alignSelf: 'center', borderTopRightRadius: 10, borderTopLeftRadius: 10, overflow: 'hidden', }}>
                <Image source={{ uri: item.image }} style={{ width: wp(45), height: hp(16), }} />
              </View>

              <View style={{ flexDirection: 'row' }}>
                {/* <Image source={require('../img/2548.png')} style={{ width: 40, height: 40, borderRadius: (40 / 2), margin: 5 }} /> */}

                <View style={{ flex: 1, flexDirection: 'column', marginLeft: wp(3), marginTop: 8 }}>
                  <Text numberOfLines={1} style={{ color: "#000000EE", width: wp('35'), fontFamily: 'Prompt-SemiBold', fontSize: 13, }}>{item.name_news}</Text>
                  <View style={{ flexDirection: 'column', marginTop: 1 }}>
                    <Text numberOfLines={1} style={{ color: "#000000AA", width: wp('35'), fontSize: 10, fontFamily: 'Prompt-Light' }}>{item.detail}</Text>
                  </View>
                </View>
              </View>

            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  modaldetail = () => {
    const imageOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: 'clamp',
    });
    const imageTranslate = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -50],
      extrapolate: 'clamp',
    });
    //let data = [1, 2, 3]
    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: 'clamp',
    });
    return (
      <LinearGradient colors={['#FFF', '#FFF']} style={styles.linearGradient}>
        <View style={{ flex: 1, flexDirection: 'column', }}>
          <ScrollView style={{ flex: 1, }}
            showsVerticalScrollIndicator={false}
            style={styles.fill}
            scrollEventThrottle={16}
            onScroll={Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }]
            )}>


            <Text style={{ marginTop: hp(40), marginLeft: wp(3), marginRight: wp(3), width: wp(95), fontFamily: 'Prompt-SemiBold', fontSize: 18, color: 'black' }}>
              {this.state.test.name}
            </Text>

            <View style={{ alignItems: 'center' }}>
              <Text style={{ marginTop: wp(5), marginLeft: wp(3), marginRight: wp(3), width: wp(85), textAlign: 'justify', fontFamily: 'Prompt-Light', color: 'black' }}>
                {this.state.test.detail}
              </Text>


              <View style={styles.cardNewsContainer}>
                <Carousel
                  ref={(c) => { this._carousel = c; }}
                  data={this.state.test.image_details}
                  sliderWidth={wp('100%')}
                  itemWidth={wp(70)}
                  enableSnap={true}
                  loop={true}
                  lockScrollWhileSnapping={true}
                  renderItem={({ item }) => {
                    return (
                      <View style={{ shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, }}>
                        <TouchableOpacity onPress={() => { this.setState({ zoomImage: true }) }}>
                          <Image style={{ alignSelf: 'center', recideMode: 'contian', width: wp(70), height: wp(50), borderRadius: 10, marginTop: wp(5) }} source={{ uri: item.imagedetail }}
                            PlaceholderContent={<ActivityIndicator />} />
                        </TouchableOpacity>

                        <Modal visible={this.state.zoomImage} transparent={true} swipeDirection='down' swipeThreshold={50} onSwipeComplete={() => this.setState({ zoomImage: false })} >
                          <ImageViewer imageUrls={this.state.test.image_details.map((item, index) => { return { url: item.imagedetail } })} />
                          <Icon
                            underlayColor={false}
                            // raised
                            name='md-close'
                            type='ionicon'
                            color='#FFF'
                            size={wp(7)}
                            containerStyle={{ position: 'absolute', right: wp(10), top: wp(13), alignSelf: 'flex-end', marginRight: wp(-2) }}
                            onPress={() => this.setState({ zoomImage: false })}
                          />
                        </Modal>
                      </View>
                    );
                  }}
                />
              </View>
            </View>
          </ScrollView>


          <Animated.View style={[styles.header, { height: headerHeight }]}>
            <Animated.View>
              <View style={styles.bar}>
                <Text style={styles.title}  >TOT Easy Life</Text>
              </View>
            </Animated.View>

            <Animated.Image
              style={[styles.backgroundImage, { opacity: imageOpacity, transform: [{ translateY: imageTranslate }] },]}
              source={{ uri: this.state.test.image }} />


            <View style={{ marginTop: wp(-8), marginLeft: wp(4) }}>
              <Ionicons
                containerStyle={{ justifyContent: 'flex-start', alignSelf: 'flex-start' }}
                name='ios-close'

                size={45}

                onPress={() => { this.setState({ pickerDisplayed: false, test: null }) }} />
            </View>
          </Animated.View>



        </View>
      </LinearGradient>
    )
  }


  render() {

    const PageAll = ({ label }) => (


      <ScrollView style={{ flexDirection: 'column' }}>
        <View>
          <View style={styles.container}>
            <TouchableOpacity
              onPress={() => {
                this.setState({
                  test: { name: this.state.NewFirst.name_news, detail: this.state.NewFirst.detail, image: this.state.NewFirst.image, image_details: this.state.NewFirst.image_details },
                  pickerDisplayed: true
                })
              }}
              style={{ shadowColor: "#000", shadowOffset: { width: 5, height: 7, }, shadowOpacity: 0.25, shadowRadius: 3.84, }}
            >
              <Image resizeMode="cover" style={styles.logo}
                source={{ uri: this.state.NewFirst.image }}
              />
              <View style={styles.coverOverlayContainer}>

                <Faded height={HEIGHT / 3} color="#000" >

                  <View style={{
                    marginLeft: wp(5), flexDirection: 'column', width: wp(92), height: wp(15), borderRadius: 10,
                  }}>
                    <Text style={styles.text}>{this.state.NewFirst.name_news}</Text>
                    <Text numberOfLines={1} style={{ color: "#FFF", width: wp(80), fontSize: 14, fontFamily: 'Prompt-Light' }}>{this.state.NewFirst.detail}</Text>
                  </View>
                </Faded>
              </View>

            </TouchableOpacity>
          </View>

          <View style={styles.MainContainer}>
            <FlatList
              showsHorizontalScrollIndicator={false}
              numColumns={2}
              data={this.state.ListNew}
              ref={ref => (this.flatlist2 = ref)}
              renderItem={({ item }) => this.renderItem(item)}>



            </FlatList>

          </View>
        </View>
      </ScrollView>
    );
    const Page1 = ({ label }) => (


      <ScrollView style={{ flexDirection: 'column' }}>
        <View>
          <View style={styles.container}>
            <View style={{
              width: wp(95), marginTop: hp(2), marginBottom: hp(3), flexDirection: 'column', justifyContent: 'center', alignItems: 'center', flex: 1,
              shadowColor: "#000",
              shadowOffset: { width: 2, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5,
            }}>


              <FlatList
                showsHorizontalScrollIndicator={false}
                data={this.state.ListNew}
                ref={ref => (this.flatlist = ref)}
                onScrollEndDrag={e => {
                  this.pagination(e.nativeEvent.velocity.x);
                }}
                renderItem={({ item }) => {
                  return (
                    item.type_id == 1 ?
                      <TouchableOpacity
                        // underlayColor={false}
                        onPress={() => {
                          this.setState({
                            test: { name: item.name_news, detail: item.detail, image: item.image, image_details: item.image_details },
                            pickerDisplayed: true
                          })
                        }}
                        style={styles.BlockNews}>
                        <View style={{ borderRadius: 10, overflow: 'hidden', }}>
                          <Image source={{ uri: item.image }} style={{ width: wp(45), height: hp(20), }} />
                        </View>

                        <View style={{ flexDirection: 'row', width: wp(93) }}>
                          <View style={{ flex: 1, flexDirection: 'column', marginLeft: wp(3), marginTop: 13 }}>
                            <Text numberOfLines={1} style={{ color: "#000000EE", width: wp('42'), fontFamily: 'Prompt-SemiBold', fontSize: 11, }}>{item.name_news}</Text>
                            <View style={{ flexDirection: 'column', marginTop: 5 }}>
                              <Text numberOfLines={3} style={{ color: "#000000AA", width: wp('42'), fontSize: 9, fontFamily: 'Prompt-Light', fontWeight: '400' }}>{item.detail}</Text>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: wp(4) }}>

                              <Icon
                                // name={this.state.heartIcon}
                                onPress={() => { this.share() }}
                                name='ios-share-alt'
                                type='ionicon'
                                size={18}
                                containerStyle={{ justifyContent: 'center', marginTop: wp(1), marginLeft: wp(18) }}
                                color='black'
                              />
                              <Text style={{ marginLeft: wp(2), fontFamily: 'Prompt-Light', fontSize: 12, marginTop: wp(1) }} >แชร์</Text>


                            </View>

                          </View>

                        </View>

                      </TouchableOpacity>
                      : null
                  )
                }}
              />
            </View>
          </View>
        </View>
      </ScrollView>
    );

    const Page2 = ({ label }) => (

      <ScrollView style={{ flexDirection: 'column' }}>
        <View>
          <View style={styles.container}>
            <View style={{
              width: wp(95), marginTop: hp(2), marginBottom: hp(3), flexDirection: 'column', justifyContent: 'center', alignItems: 'center', flex: 1,
              shadowColor: "#000",
              shadowOffset: { width: 2, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5,
            }}>


              <FlatList
                showsHorizontalScrollIndicator={false}
                data={this.state.ListNew}
                ref={ref => (this.flatlist = ref)}
                onScrollEndDrag={e => {
                  this.pagination(e.nativeEvent.velocity.x);
                }}
                renderItem={({ item }) => {
                  return (
                    item.type_id == 2 ?
                      <TouchableOpacity
                        // underlayColor={false}
                        onPress={() => {
                          this.setState({
                            test: { name: item.name_news, detail: item.detail, image: item.image, image_details: item.image_details },
                            pickerDisplayed: true
                          })
                        }}
                        style={styles.BlockNews}>
                        <View style={{ borderRadius: 10, overflow: 'hidden', }}>
                          <Image source={{ uri: item.image }} style={{ width: wp(45), height: hp(20), }} />
                        </View>

                        <View style={{ flexDirection: 'row', width: wp(93) }}>
                          <View style={{ flex: 1, flexDirection: 'column', marginLeft: wp(3), marginTop: 13 }}>
                            <Text numberOfLines={1} style={{ color: "#000000EE", width: wp('42'), fontFamily: 'Prompt-SemiBold', fontSize: 11, }}>{item.name_news}</Text>
                            <View style={{ flexDirection: 'column', marginTop: 5 }}>
                              <Text numberOfLines={3} style={{ color: "#000000AA", width: wp('42'), fontSize: 9, fontFamily: 'Prompt-Light', fontWeight: '400' }}>{item.detail}</Text>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: wp(4) }}>

                              <Icon
                                // name={this.state.heartIcon}
                                onPress={() => { this.share() }}
                                name='ios-share-alt'
                                type='ionicon'
                                size={18}
                                containerStyle={{ justifyContent: 'center', marginTop: wp(1), marginLeft: wp(18) }}
                                color='black'
                              />
                              <Text style={{ marginLeft: wp(2), fontFamily: 'Prompt-Light', fontSize: 12, marginTop: wp(1) }} >แชร์</Text>


                            </View>

                          </View>

                        </View>

                      </TouchableOpacity>
                      : null
                  )
                }}
              />
            </View>
          </View>
        </View>
      </ScrollView>
    );

    const Page3 = ({ label }) => (

      <ScrollView style={{ flexDirection: 'column' }}>
        <View>
          <View style={styles.container}>
            <View style={{
              width: wp(95), marginTop: hp(2), marginBottom: hp(3), flexDirection: 'column', justifyContent: 'center', alignItems: 'center', flex: 1,
              shadowColor: "#000",
              shadowOffset: { width: 2, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5,
            }}>


              <FlatList
                showsHorizontalScrollIndicator={false}
                data={this.state.ListNew}
                ref={ref => (this.flatlist = ref)}
                onScrollEndDrag={e => {
                  this.pagination(e.nativeEvent.velocity.x);
                }}
                renderItem={({ item }) => {
                  return (
                    item.type_id == 3 ?
                      <TouchableOpacity
                        // underlayColor={false}
                        onPress={() => {
                          this.setState({
                            test: { name: item.name_news, detail: item.detail, image: item.image, image_details: item.image_details },
                            pickerDisplayed: true
                          })
                        }}
                        style={styles.BlockNews}>
                        <View style={{ borderRadius: 10, overflow: 'hidden', }}>
                          <Image source={{ uri: item.image }} style={{ width: wp(45), height: hp(20), }} />
                        </View>

                        <View style={{ flexDirection: 'row', width: wp(93) }}>
                          <View style={{ flex: 1, flexDirection: 'column', marginLeft: wp(3), marginTop: 13 }}>
                            <Text numberOfLines={1} style={{ color: "#000000EE", width: wp('42'), fontFamily: 'Prompt-SemiBold', fontSize: 11, }}>{item.name_news}</Text>
                            <View style={{ flexDirection: 'column', marginTop: 5 }}>
                              <Text numberOfLines={3} style={{ color: "#000000AA", width: wp('42'), fontSize: 9, fontFamily: 'Prompt-Light', fontWeight: '400' }}>{item.detail}</Text>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginTop: wp(4) }}>

                              <Icon
                                // name={this.state.heartIcon}
                                onPress={() => { this.share() }}
                                name='ios-share-alt'
                                type='ionicon'
                                size={18}
                                containerStyle={{ justifyContent: 'center', marginTop: wp(1), marginLeft: wp(18) }}
                                color='black'
                              />
                              <Text style={{ marginLeft: wp(2), fontFamily: 'Prompt-Light', fontSize: 12, marginTop: wp(1) }} >แชร์</Text>


                            </View>

                          </View>

                        </View>

                      </TouchableOpacity>
                      : null
                  )
                }}
              />
            </View>

          </View>
        </View>
      </ScrollView>
    );

    return (
      <View style={[styles.container, { paddingTop: wp(1) }]}>
        <ScrollableTabView
          style={fontFamily = "Prompt-Light"}
          tabBarActiveTextColor="#1C86EE"

          renderTabBar={() => <TabBar underlineColor="#1874CD" tabBarTextStyle={{ fontFamily: "Prompt-Light" }} />}>
          <PageAll tabLabel={{ label: "ข่าวทั้งหมด" }} abel="Page #1" />
          <Page1 tabLabel={{ label: "ข่าวทั่วไป" }} label="Page #2 " />
          <Page2 tabLabel={{ label: "ข่าวประชาสัมพันธ์องกรณ์", }} label="Page #3" />
          <Page3 tabLabel={{ label: "ข่าวธุรกิจ" }} label="Page #4 " />

        </ScrollableTabView>
        {this.state.test != null ?
          <Modal2

            animationType={"slide"}
            isVisible={this.state.pickerDisplayed}
            onSwipeComplete={() => this.setState({ pickerDisplayed: false })}
            // swipeDirection={['down']}
            style={styles.bottomModal}
            onBackdropPress={() => this.setState({ pickerDisplayed: false })}

          >

            {
              this.modaldetail()
            }

          </Modal2> : null
        }
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFF',

  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    fontFamily: 'Prompt-Light'
  },
  instructions: {
    textAlign: 'center',
    color: '#1E90FF',
    marginBottom: 5,
    fontSize: 28,
    fontFamily: 'Prompt-Light'
  },
  coverOverlayContainer: {
    backgroundColor: 'transparent',
    // backgroundColor: 'red',
    position: "absolute",
    top: wp(52),
    left: wp(0),
    right: 0,
    bottom: 0,
    width: wp(92),
    height: wp(15),
    justifyContent: 'space-between',
    // borderRadius: 10,
    resizeMode: "cover"
  },
  text: {
    color: '#FFF',
    fontFamily: 'Prompt-SemiBold',
    fontSize: 16,
  },
  text1: {
    color: '#FFF',
    fontFamily: 'Prompt-Light',
    fontSize: 10,
    width: wp(80)
  },

  logo: {
    backgroundColor: "white",
    height: HEIGHT,
    width: wp(92),
    marginTop: wp(5),
    // borderRadius: 10,

  },
  gridView: {
    marginTop: 20,
    flex: 1,
    marginLeft: wp(1),
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.3, shadowRadius: 3.84, elevation: 5,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    // borderRadius: 10,
    padding: 10,
    height: 140,
    width: wp(45)
  },

  itemName: {
    fontSize: 10,
    color: '#fff',
    fontWeight: '600',
    fontFamily: 'Prompt-Light',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 8,
    color: '#fff',
    fontFamily: 'Prompt-Light',
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
    flex: 1,

  },
  BlockNews: {
    flex: 1,
    backgroundColor: '#FFF',
    width: wp(93),
    borderRadius: 10,
    marginBottom: wp(3),
    marginTop: wp(2),
    height: wp(32.5),
    alignSelf: 'center',
    flexDirection: 'row',
  },
  linearGradient: {
    flexDirection: 'column',
    flex: 1,
    alignSelf: 'center',
    borderRadius: 5,
    height: 220,
    width: wp('100')
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#FFF',
    overflow: 'hidden',
  },

  fill: {
    flex: 1,
  },
  bar: {
    marginTop: hp(5),
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    backgroundColor: 'transparent',
    color: 'black',
    fontSize: 18,

  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover',
  },
  lineStylew6: {
    borderWidth: 0.5,
    borderColor: '#828282',
    alignSelf: 'center',
    width: wp(93),
  },
  MainContainer: {
    margin: wp(3),
    marginBottom: wp(5),
    flex: 1,
    marginLeft: wp(1.8),
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.3, shadowRadius: 3.84, elevation: 5,
  },
  imageThumbnail: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 140,
    width: wp(44.5),
    margin: wp(2)
  },
  cardNewsContainer: {
    alignSelf: 'center',
    marginTop: wp(5),
    shadowColor: "#000",
    backgroundColor: 'rgb(238, 238, 238)',
    width: wp(100), height: wp(60),

  },
});

