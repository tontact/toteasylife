import React, { Component } from 'react';
import { Animated, Platform, FlatList, View, Text, Button, Alert, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HeaderButtons, { HeaderButton, Item } from 'react-navigation-header-buttons';
import {
  widthPercentageToDP as wp, heightPercentageToDP as hp, listenOrientationChange as loc, removeOrientationListener as rol
} from 'react-native-responsive-screen';

import { ThemeConsumer } from 'react-native-elements'

import { SearchBar } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
// import Swiper from 'react-native-swiper';
import SwiperFlatList from 'react-native-swiper-flatlist';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import { FluidNavigator, Transition } from 'react-navigation-fluid-transitions';
import { Header } from 'react-navigation';
import Icon from "react-native-vector-icons/Ionicons";

const IoniconsHeaderButton = passMeFurther => (
  <HeaderButton {...passMeFurther} IconComponent={Ionicons} iconSize={28} color="white" />
);


const HEADER_MAX_HEIGHT = 300;
const HEADER_MIN_HEIGHT = wp(25);
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;


let screenWidth = Dimensions.get('window').width;

// const {
//   height: SCREEN_HEIGHT,
// } = Dimensions.get('window');

// const IS_IPHONE_X = SCREEN_HEIGHT === 812 || SCREEN_HEIGHT === 896;
// const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 44 : 20) : 0;
// const NAV_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 64;

// const SCROLL_EVENT_THROTTLE = 16;
// const DEFAULT_HEADER_MAX_HEIGHT = 170;
// const DEFAULT_HEADER_MIN_HEIGHT = NAV_BAR_HEIGHT;
// const DEFAULT_EXTRA_SCROLL_HEIGHT = 30;
// const DEFAULT_BACKGROUND_IMAGE_SCALE = 1.5;




export default class allpromotion extends Component {
  constructor(props) {
    super(props);

  }




  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    // const params = navigation.getParam('checklogin')
    // this.state.user
    // alert(params)


    return {

      headerTransitionPreset: 'fade-in-place',
      // title: 'ชำระค่าบริการ',
      headerTitle: (<Text style={{ fontFamily: "Prompt-SemiBold", color: "#000", textAlign: "center", flex: 1, fontSize: 20 }}>โปรโมชั่น</Text>),
      headerRight: (
        <TouchableOpacity style={{ marginRight: wp(2.5) }} >
          {/* <Icon name='ios-menu' size={30} color='#000' /> */}
        </TouchableOpacity>
      ),
      headerLeft: (
        <TouchableOpacity style={{ marginLeft: wp(3), }} >
          <Ionicons name='ios-arrow-back' size={30} onPress={() => navigation.goBack()} />
        </TouchableOpacity>
      ),
      // //  headerTitle: <Logo/>,

      headerStyle: {
        backgroundColor: '#F9F9F9'
      },
      headerTintColor: '#000',
      headerTitleStyle: {
        fontFamily: "Prompt-Light",
        textAlign: 'center',
        flex: 1,
        fontSize: 20,
      },
    }
  };
  //   test(){
  //       Alert.alert('in')
  //   }



  // renderItem(item) {
  //   return (

  //   );
  // }


  render() {



    const sizeIcon = wp('5%');
    return (
      // <LinearGradient colors={['#FFFAFA', '#FFFAF0']} style={styles.linearGradient}>
      <ScrollView style={{ flex: 1, }}>

        <View style={{ flex: 1, marginBottom: wp(20) }}>





          {/* <View style={styles.lineStylew3} /> */}

          <LinearGradient start={{ x: 0, y: 1.2 }} end={{ x: 1, y: 0 }} colors={['#FF18A7', '#FFC659',]} style={styles.linearGradient}>



            <View style={styles.detailTopBottomSubContainer}>
              {/* <Text style={{
                color: 'white',
                fontWeight: '500',
                fontFamily: 'Prompt-Light',
                fontSize: scaleToDimension(35)
              }}>ALL - {"\n"}PROMOTION</Text> */}

              <Text style={{ color: 'white', fontSize: scaleToDimension(35), fontFamily: 'Prompt-SemiBold', }}>ALL -</Text>
              <Text style={{ color: 'white', fontSize: scaleToDimension(35), fontFamily: 'Prompt-SemiBold', marginTop: wp(-5) }}>PROMOTION</Text>
              <Text style={{ color: 'white', fontSize: scaleToDimension(15), fontFamily: 'Prompt-Light', marginTop: wp(-3) }}>Get Great Deals !</Text>
              <Text style={{ color: 'white', fontSize: scaleToDimension(18), fontFamily: 'Prompt-SemiBold', marginTop: wp(0) }}>โปรโมชั่นทั้งหมด</Text>
            </View>



          </LinearGradient>



          <TouchableOpacity onPress={() => this.props.navigation.navigate('promotion1')} >
            <View style={{ flexDirection: 'column', alignItems: 'center', justifyContent: 'center', marginTop: wp(-15) }}>
              <View style={{ width: wp(87), height: wp(40), shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5, marginTop: wp(3), }} >
                <LinearGradient start={{ x: 1.2, y: 1 }} end={{ x: 0, y: 2 }} colors={['#00FFFF', '#0089D7',]} style={styles.linearGradient1}>
                  <View style={styles.detailTopBottomSubContainer1}>
                    <View style={{ flexDirection: 'column', marginTop: wp(4), }}>
                      <Text style={{ color: 'white', fontSize: scaleToDimension(35), fontFamily: 'Prompt-SemiBold', }}>Internet</Text>
                      <Text style={{ color: 'white', fontSize: scaleToDimension(35), fontFamily: 'Prompt-SemiBold', marginTop: wp(-5), }}>& Fiber</Text>

                      <Text style={{ color: 'white', fontSize: scaleToDimension(18), fontFamily: 'Prompt-Light', marginTop: wp(0), }}>อินเตอร์เน็ต / ไฟเบอร์</Text>
                    </View>
                    <View style={{ marginTop: wp(-24), width: wp(57), height: wp(57), marginLeft: wp(-18), }}>
                      <Image resizeMode="contain" style={{ width: wp(57), height: wp(57), }}
                        source={require('../alliconESL/NewsIcons/PromotionTOTeasylifeAW1copy_18.png')}
                      />
                    </View>
                  </View>
                </LinearGradient>

              </View>

            </View>
          </TouchableOpacity>


          <TouchableOpacity onPress={() => this.props.navigation.navigate('promotion2')} >


            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>

              <View style={{ width: wp(87), height: wp(40), shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5, marginTop: wp(3), }} >

                <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 1, y: 0 }} colors={['#A4DBF8', '#FFF',]} style={styles.linearGradient1}>
                  <View style={styles.detailTopBottomSubContainer2}>
                    <View style={{ marginTop: wp(-18), width: wp(50), height: wp(50) }}>
                      <Image resizeMode="contain" style={{ width: wp(60), height: wp(60), }}
                        source={require('../alliconESL/NewsIcons/PromotionTOTeasylifeAW1copy17.png')}
                      />
                    </View>
                    <View style={{ flexDirection: 'column', marginLeft: wp(-18), }}>
                      <Text style={{ color: 'white', fontSize: scaleToDimension(35), fontFamily: 'Prompt-SemiBold', marginLeft: wp(28) }}>Fixed</Text>
                      <Text style={{ color: 'white', fontSize: scaleToDimension(35), fontFamily: 'Prompt-SemiBold', marginTop: wp(-5), marginLeft: wp(34) }}>Line</Text>

                      <Text style={{ color: 'white', fontSize: scaleToDimension(18), fontFamily: 'Prompt-Light', marginTop: wp(0), }}>โทรศัพท์บ้าน / โทรทางไกล</Text>
                    </View>

                  </View>



                </LinearGradient>

              </View>



            </View>

          </TouchableOpacity>


          <TouchableOpacity onPress={() => this.props.navigation.navigate('promotion3')} >


            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>

              <View style={{ width: wp(87), height: wp(40), shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5, marginTop: wp(3), }} >

                <LinearGradient start={{ x: 1.5, y: 1 }} end={{ x: 0, y: 2 }} colors={['#FE8327', '#EB2028',]} style={styles.linearGradient1}>
                  <View style={styles.detailTopBottomSubContainer3}>
                    <View style={{ flexDirection: 'column' }}>
                      <Text style={{ color: 'white', fontSize: scaleToDimension(35), fontFamily: 'Prompt-SemiBold', }}>TOT IPTV</Text>
                      <Text style={{ color: 'white', fontSize: scaleToDimension(18), fontFamily: 'Prompt-Light', marginTop: wp(-2), }}>กล่องดูทีวี</Text>
                      <View style={{ marginTop: wp(-33.5), width: wp(50), height: wp(50), alignSelf: 'center' }}>
                        <Image resizeMode="contain" style={{ width: wp(72), height: wp(72), alignSelf: 'center' }}
                          source={require('../alliconESL/NewsIcons/PromotionTOTeasylifeAW1copy16.png')}
                        />
                      </View>
                    </View>
                  </View>



                </LinearGradient>

              </View>



            </View>



          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.props.navigation.navigate('promotion4')} >


            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>


              <View style={{ width: wp(87), height: wp(40), shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5, marginTop: wp(5), }} >

                <LinearGradient start={{ x: 1, y: 0 }} end={{ x: 1, y: 0 }} colors={['#FF9C1B', '#FFF',]} style={styles.linearGradient1}>
                  <View style={styles.detailTopBottomSubContainer1}>
                    <View style={{ flexDirection: 'column' }}>
                      <Text style={{ color: 'white', fontSize: scaleToDimension(35), fontFamily: 'Prompt-SemiBold' }}>Mobile</Text>
                      {/* <Text style={{ color: 'white', fontSize: scaleToDimension(35), fontFamily: 'Prompt-SemiBold', marginTop: wp(-5), }}>& Fiber</Text> */}

                      <Text style={{ color: 'white', fontSize: scaleToDimension(18),fontFamily: 'Prompt-Light', marginTop: wp(0), }}>มือถือ</Text>
                    </View>
                    <View style={{ marginTop: wp(-24), width: wp(57), height: wp(57), marginLeft: wp(-5) }}>
                      <Image resizeMode="contain" style={{ width: wp(73), height: wp(73), }}
                        source={require('../alliconESL/NewsIcons/PromotionTOTeasylifeAW1copy15.png')}
                      />
                    </View>
                  </View>



                </LinearGradient>

              </View>
            </View>


          </TouchableOpacity>






        </View>




      </ScrollView>
      // </LinearGradient>
    );
  }
}

const scaleToDimension = (size) => {
  return screenWidth * size / 375
};

const styles = StyleSheet.create({


  linearGradient: {
    flexDirection: 'column',
    flex: 1,
    alignSelf: 'center',

    height: wp(53),
    width: wp('100')
  },

  linearGradient1: {
    flexDirection: 'column',
    flex: 1,
    alignSelf: 'center',
    borderRadius: 10,
    width: wp(87), height: wp(40),


  },


  detailTopContainer: {
    height: scaleToDimension(250),
    width: screenWidth,
    backgroundColor: 'transparent',

  },

  detailTopBottomSubContainer: {
    width: screenWidth - 30,
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: wp(14),
    left: wp(7),
    right: wp(5),
  },

  detailTopBottomSubContainer1: {

    top: wp(8),
    left: wp(4),
    flexDirection: 'row'


  },
  detailTopBottomSubContainer2: {

    top: wp(8),

    flexDirection: 'row'


  },
  detailTopBottomSubContainer3: {

    top: wp(2),
    alignSelf: 'center',
    flexDirection: 'row'


  },

})









