import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, AsyncStorage, StyleSheet, Button, Alert, SafeAreaView } from 'react-native'
import ImageSelecter from 'react-native-image-picker';
import CardView from 'react-native-cardview'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen';
import axios from 'axios';
import Modal from 'react-native-modal';
import base64 from 'react-native-base64';
import DeviceInfo from 'react-native-device-info';
import RNRestart from "react-native-restart";
import Icon3 from "react-native-vector-icons/Ionicons";

import PinView from 'react-native-pin-view';
import { ScrollView } from 'react-native-gesture-handler';



export default class index extends Component {

  state = {
    data: [],
    data1: [],
    databa: [],
    userid: 0,
    checkbill: [],
    imageSource: null,
    m1: [],
    m2: [],
    bano: [],
    svno: [],
    sum: 0,
    Ipaddress: '',
    mbrid: '',
    usrtoken: '',
    email: '',
    baid: '',
    visibleModalId: false,
    pincode: null,
    pincodeOnOff: false,
    Email: '',
    Mobileno: '',
    id: '',
    UserImage: null,

    userImage: null,
  }



  componentDidMount = async () => {
    DeviceInfo.getIpAddress().then(ip => {
      console.log("IpAddress:" + ip);
      this.setState({ Ipaddress: ip })
    });
    var mbrid = await AsyncStorage.getItem('mbrid');
    var usrtoken = await AsyncStorage.getItem('usrtoken');
    var email = await AsyncStorage.getItem('email');
    var id = await AsyncStorage.getItem('userid');
    // console.log(email);
    // console.log(mbrid);
    console.log(id);

    this.setState({ id: id })
    // this.setState({ mbrid: mbrid })
    // this.setState({ usrtoken: usrtoken })
    // this.setState({ email: email })
    this.getProfile();
    // this.getBa();
    var pincode = await AsyncStorage.getItem('pincode');
    var pincodeOnOff = await AsyncStorage.getItem('switchOn3');
    this.setState({
      pincode: pincode,
      pincodeOnOff: pincodeOnOff

    })
  }


  onComplete = (inputtedPin, clear) => {
    if (inputtedPin == this.state.pincode) {
      this.setState({ visibleModalId: null })
    } else {
      Alert.alert(
        "รหัสผ่านไม่ถูกต้อง",
        "กรุณาใส่รหัสผ่านใหม่")
      clear();
    }
  }

  onPress(inputtedPin, clear, pressed) {
    console.log("Pressed: " + pressed);
    console.log("inputtedPin: " + inputtedPin)
  }

  getProfile() {
    const url = "http://203.113.11.167/api/myuser/" + this.state.id;
    console.log(url);
    // const headers = {
    //   'Content-Type': 'application/json',
    //   'Authorization': 'JWT fefege...'
    // }
    axios.get(url)
      .then(result => {
        // var userprofile = JSON.stringify(result);
        // console.log(userprofile);
        console.log(result);
        console.log(JSON.stringify(result.data.data.id));
        this.setState({
          data1: result.data.data,
          UserImage: result.data.data.userImage
        })
        if (this.state.pincodeOnOff == "true") {
          if (this.state.pincode != null) {
            this.setState({ visibleModalId: 'bottom' })
          }

        }

      })
      .catch(err => {
        alert(JSON.stringify(error));
      })
  }



  renderModalContentPincode = () => (
    <View style={{ flex: 1, backgroundColor: 'white', flexDirection: "column", alignItems: "center", }}>
      <SafeAreaView style={{ flex: 1, alignItems: "center", flexDirection: "column", marginTop: wp('20%') }}>

        <Text style={{ color: "#008DFF", fontSize: 30, fontFamily: "Prompt-Light", fontWeight: '500' }}>Enter the Code</Text>
        <Text style={{ color: "#008DFF", fontSize: 18, fontFamily: "Prompt-Light", }}>กรุณาใส่รหัสผ่าน</Text>

        <PinView
          onPress={this.onPress}
          onComplete={this.onComplete}
          pinLength={6}
          buttonTextColor="#008DFF"
          buttonBgColor="white"
          inputActiveBgColor="#008DFF"
          inputBgOpacity={0.3}
          keyboardViewStyle={{ borderWidth: 1, borderColor: '#008DFF', }}
          inputViewStyle={{ width: wp('3%'), height: wp('3%'), marginTop: wp('2%') }}
          keyboardViewTextStyle={{ fontSize: 30, fontWeight: '300' }}
          buttonDeletePosition="right"
          deleteText={<Icon3 name="ios-backspace" size={32} color="black" />}
          buttonDeleteStyle={{ borderWidth: 0 }}
        />
      </SafeAreaView>
    </View>
  );


  renderModalContent = () => (

    <View style={styles.content}>



      <Text onPress={() => this.renderSetting()} style={styles.contentTitle1}>ตั้งค่า</Text>
      {/* <View style={styles.lineStylew6} /> */}
      <Text onPress={() => this.renderEditProfile()} style={styles.contentTitle2}>แก้ไขโปรไฟล์</Text>
      {/* <View style={styles.lineStylew6} /> */}
       <TouchableOpacity
          onPress={() => this.setState({  visibleModal: null})}

        >
        <Text style={{fontWeight:'500', fontFamily: 'prompt-light', fontSize:wp(5),color: '#1b7af7', marginBottom:wp(2) }}>ปิด</Text>
        </TouchableOpacity>

    </View>

  );

  renderEditProfile() {
    this.setState({ visibleModal: false })
    this.props.route.navigate('editProfilePage')
  }

  renderSetting() {
    this.setState({ visibleModal: false })
    this.props.route.navigate('settingPage')
  }



  SelectCameraRoll = () => {
    //   ImagePicker.openPicker({
    //     width: 300,
    //     height: 300,
    //     cropping: true,
    //     includeBase64: true,
    //     includeExif: true,
    // }).then(image => {
    //     console.log('received base64 image');
    //     this.setState({
    //         imageSource: {uri: 'data:image/jpeg;base64,'+image.data}
    //     });
    // })
    const options = {
      // mediaType:'photo',
      quality: 0.5,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };
    ImageSelecter.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        // alert('response image ....')
        // const source = { uri: data:${response.data.mime};base64, + response.data };
        // let source = { uri: response.uri };
        this.setState({
          UserImage: {
            uri: 'data:image/jpeg;base64,' + response.data,
          }
        });
        alert(this.state.UserImage.uri);
        //this.uploadPhoto(this.state.citizen);
      }
    });
  };



  render() {
    return (
      <View >

        <View style={{
          width: wp(95), height: wp(32), backgroundColor: '#FFF', borderRadius: 10, justifyContent: 'center', alignSelf: 'center', flexDirection: 'column'
          , shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5, marginTop: wp(3)
        }}>



          <View>

            {/* <View style={styles.lineStylew3} /> */}

          </View>
          {/* {
              this.state.UserImage == null ?

          : <Image
                  source={{ uri: 'http://203.113.11.167/uploads/imagesaveuser/' + this.state.UserImage }}
                  style={{ width: wp('25'), height: wp('25') ,borderRadius: wp('25') / 2,marginTop: wp(0.9), marginLeft:wp(0.8) }}
                />
            } */}


          <View style={{ marginLeft: wp(6), marginTop: wp(0), flexDirection: 'row', }}>

          <View style={{ width: wp('28'), height: wp('28'), borderRadius: wp('28') / 2, borderColor: '#008cff', borderWidth: wp(0.7), }}>
          {
              this.state.UserImage == null ?

                <Image
                  source={require('../../alliconESL/HomeIcons/HomeTOTeasylifeAW111.png')}
                  style={{ width: wp('27'), height: wp('27') ,borderRadius: wp('27') / 2, marginTop: wp(0), marginLeft:wp(-0.3) }}
                />   : <Image
                source={{ uri: 'http://203.113.11.167/uploads/imagesaveuser/' + this.state.UserImage }}
                style={{ width: wp('25'), height: wp('25') ,borderRadius: wp('25') / 2,marginTop: wp(0.9), marginLeft:wp(0.8) }}
              />
          }
            </View>

            <View style={{ flexDirection: 'column', marginLeft: wp(3),marginTop:wp(5) }}>



{/* 
              <TouchableOpacity

                onPress={() => this.setState({ visibleModal: 'bottom' })}>
                <Image style={{ marginLeft: wp('50%'), width: wp(4.5), height: wp(4.5), marginTop: wp('1%') }} source={require('../../ICON_Png/กรอบเท่ากัน/icondesign14.png')} />
              </TouchableOpacity> */}

              <Text style={{ fontFamily: 'Prompt-Light', fontWeight: '500', fontSize: wp('4%'), width: wp(57), color: '#3d90ff', marginTop: wp('0%'), }}>{this.state.data1.name} {this.state.data1.lastname}</Text>

              {/* 
              {this.state.baid == "" ?

                <View style={{ flexDirection: 'row', marginTop: wp(1), marginLeft: wp(2), }}>
                  <Image
                    source={require('../../ICON_Png/กรอบเท่ากัน/icondesign-13.png')}
                    style={{ width: wp('7%'), height: wp('7%'), borderRadius: wp('7%') / 2, }}
                  />
                  <Text style={{ fontFamily: 'Prompt-Light', color: '#3d90ff', fontSize: wp('3%'), marginTop: wp(1), fontWeight: '400' }}>รหัสลูกค้า: </Text>
                  <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp('3%'), marginTop: wp(1) }}>ไม่มีข้อมูล</Text>

                </View>
                :
                <View style={{ flexDirection: 'row', marginTop: wp(1), marginLeft: wp(2) }}>
                  <Image
                    source={require('../../ICON_Png/กรอบเท่ากัน/icondesign-13.png')}
                    style={{ width: wp('7%'), height: wp('7%'), borderRadius: wp('7%') / 2, }}
                  />
                  <Text style={{ fontFamily: 'Prompt-Light', color: '#3d90ff', fontSize: wp('3%'), marginTop: wp(1), fontWeight: '400' }}>รหัสลูกค้า: </Text>
                  <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp('3%'), marginTop: wp(1) }}>{this.state.baid}</Text>
                </View>} */}


              <View style={{ flexDirection: 'row', marginHorizontal: wp(0) }}>
                <Image
                  source={require('../../ICON_Png/กรอบเท่ากัน/icondesign-12.png')}
                  style={{ width: wp('7%'), height: wp('7%'), borderRadius: wp('7%') / 2, }}
                />
                <Text style={{ fontFamily: 'Prompt-Light', color: '#3d90ff', fontSize: wp('3%'), marginTop: wp(1), fontWeight: '400' }}>เบอร์มือถือ:</Text>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp('3%'), marginTop: wp(1) }}> {this.state.data1.tel}</Text>

              </View>

              <View style={{ flexDirection: 'row', marginHorizontal: wp(1) }}>
                <Image
                  source={require('../../alliconESL/HomeIcons/HomeTOTeasylifeAW112.png')}
                  style={{ width: wp('5%'), height: wp('5%'), borderRadius: wp('5%') / 2, marginTop: wp(0.7) }}
                />
                <Text style={{ fontFamily: 'Prompt-Light', color: '#3d90ff', fontSize: wp('3%'), marginTop: wp(1), fontWeight: '400', marginLeft: wp(1) }}>อีเมล:</Text>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp('3%'), marginTop: wp(1) }}> {this.state.data1.email}</Text>

              </View>


              <Modal
                isVisible={this.state.visibleModal === 'bottom'}
                // onSwipeComplete={() => this.setState({ visibleModal: false })}
                // swipeDirection={['down']}
                style={styles.bottomModal}
              >
                {this.renderModalContent()}
              </Modal>

              <Modal
                isVisible={this.state.visibleModalId === 'bottom'}
                onSwipeComplete={() => this.setState({ visibleModal: null })}
                // swipeDirection={['up', 'left', 'right', 'down']}
                style={styles.bottomModal}
              >
                {this.renderModalContentPincode()}
              </Modal>




            </View>

          </View>

        </View>

        {/* <View style={{
          width: wp(95), height: wp(12), backgroundColor: '#f9f9f9', borderBottomLeftRadius: 10, borderBottomRightRadius: 10, justifyContent: 'center', alignSelf: 'center', flexDirection: 'column'
          , shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 14, borderTopColor: '#DCDCDC', borderTopWidth: 1,
        }}>
          <View style={{ flexDirection: 'row', marginTop: wp(1), }}>
            {this.state.baid == null ?
              <View>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(3), color: 'black', marginTop: wp(4), marginLeft: wp(31) }}> กรุณาเพิ่มรหัสลูกค้า เพื่อแสดงยอดค้างชำระ </Text>
              </View>
              :
              <View style={{ flexDirection: 'row', marginTop: wp(-1) }}>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(3), marginLeft: wp(40), marginTop: wp(5) , fontWeight:'500', color:'#3d90ff'}}>ยอดค้างชำระ</Text>
                <Text style={{ fontFamily: 'Prompt-Light', fontSize: wp(5), marginTop: wp(2.8), marginHorizontal: wp(4),fontWeight:'400' }}>2200.25 บาท</Text>
              </View>}
          </View>

        </View> */}




        {/* 
        <TouchableOpacity

          onPress={() => this.SelectCameraRoll()}>
          <Image
            source={require('../../ICON_Png/กรอบเท่ากัน/icondesign-11.png')}
            style={{ width: 25, height: 25, borderRadius: 25 / 2, marginTop: wp(-7), marginLeft: wp(30) }}
          />
        </TouchableOpacity> */}



      </View>
    )
  }
}



const styles = StyleSheet.create({
  headerHome: {
    borderBottomColor: '#FFDEAD',
    borderBottomWidth: wp('1.5'),
    borderColor: '#FFDEAD',
    justifyContent: 'center',
    marginLeft: wp('2'),
    alignItems: 'center',
    borderWidth: 2,
    borderRadius: 100,
    width: wp('8.6'),
    height: wp('8.6'),
    backgroundColor: '#0099FF'
  },
  content: {
    backgroundColor: 'white',
    height: wp(35),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  contentTitle1: {
    //marginTop:wp(2),
    fontSize: 16,
    marginBottom: wp(2),
    fontFamily: 'Prompt-Light'
  },
  contentTitle2: {
    marginTop: wp(2),
    fontSize: 16,
    marginBottom: wp(2),
    fontFamily: 'Prompt-Light'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  iconstyle4: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconstyle1: {
    justifyContent: 'center', alignSelf: 'center', margin: 1,
  },
  iconstyle2: {
    justifyContent: 'center', alignSelf: 'center', margin: 5, flexDirection: 'row'
  },
  lineStylew3: {
    borderWidth: 0.5,
    borderColor: '#DCDCDC',
    backgroundColor: '#9B9B9B',
    marginTop: wp(28.5),
    alignSelf: 'center',
    width: wp('95'),
  },
  lineStylew4: {
    borderWidth: 0.2,
    borderColor: '#DCDCDC',
    alignSelf: 'center',
    width: wp(100),
  },
  lineStylew5: {
    borderWidth: 0.5,
    borderColor: '#DCDCDC',
    backgroundColor: '#9B9B9B',
    marginTop: 10,
    alignSelf: 'center',
    width: wp('68'),
    marginRight: wp(3),

  },
  lineStylew6: {
    borderWidth: 0.25,
    borderColor: '#DCDCDC',
    alignSelf: 'center',
    width: wp(100),
  },

  iconView: {
    flexDirection: 'column',
    flex: 1,
    // backgroundColor: '#87CEFA', 
    height: hp('220'), width: wp('95'),
    borderRadius: 5,
    alignSelf: 'center',
    paddingTop: 15
  },

  linearGradient: {
    flexDirection: 'column',
    flex: 1,
    alignSelf: 'center',
    borderRadius: 5,
    height: 220,
    width: wp('100')
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },

  buttonSeeall: {
    alignItems: 'flex-end',
    // marginTop: 150,
    backgroundColor: '#00BFFF',
    color: '#fff',
    padding: 5,
    marginTop: 20,
    marginRight: 10,
    borderRadius: 5,
    justifyContent: 'flex-end',
    flexDirection: 'row'

  },

  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
    flex: 1,

  },
  fill: {
    flex: 1,
  },
  bar: {
    marginTop: hp(5),
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    backgroundColor: 'transparent',
    color: 'black',
    fontSize: 18,

  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#FFF',
    overflow: 'hidden',
  },
  bar1: {
    marginTop: wp(21),
    height: 18,
    alignItems: 'center',
    justifyContent: 'center',

  },

  BlockPromo: {
    flex: 1,
    backgroundColor: '#FFFAFA',
    marginLeft: wp(3),
    borderRadius: 10,
    height: 180,
    shadowColor: "#000",
    marginBottom: wp(3), marginTop: wp(3),
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.23, shadowRadius: 2.62, elevation: 4,
  },
  BlockNews: {
    flex: 1,
    backgroundColor: '#FFFAFA',
    marginLeft: wp(3),
    borderRadius: 10,
    marginBottom: wp(3),
    marginTop: wp(3),
    height: wp(68),
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.3, shadowRadius: 4.65, elevation: 15,
  }


})



