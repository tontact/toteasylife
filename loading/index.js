import React, { Component } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as loc,
    removeOrientationListener as rol
} from 'react-native-responsive-screen';
export default class Loding extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={{  backgroundColor:'rgba(80,80,80,0.1)', flex:1, justifyContent:'center' , alignItems:'center'}}>
        <ActivityIndicator color= '#117ffe' size='large' style={{borderColor: 'rgba(80,80,80,0.5)' , height:wp(30) , width:wp(30)}} />
      </View>
    );
  }
}
